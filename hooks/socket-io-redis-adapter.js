/**
 * socket.io-redis hook
 * TODO:- make this it's own module
 */
const sredis = require('socket.io-redis');

module.exports = {
    redisAdapter: {
        event: 'start',
        priority: 27,
        fn(next) {
            let config = this.config.redis || {};

            if (!this.sio) {
                this.logger.warn('socket-io instance not found');
                return next();
            }

            if (!config.host) {
                this.logger.warn('redis configuration not found');
                return next();
            }

            this.sio.adapter(sredis({
                host: config.host,
                port: config.port
            }));

            this.logger.info('redis socket-io adapter initialized');

            next();
        }
    },
    redisAdapterClose: {
        event: 'stop',
        priority: 1,
        fn(next) {
            this.sio._adapter.subClient.quit();
            this.sio._adapter.pubClient.quit();
            this.config.session.store.client.quit(); // TODO:- figure out why client isn't closing

            next();
        }
    }
};
