const { toSnakeCase } = require("../libs/utils");

module.exports = {
    toSnakeCaseHook: {
        event: "start",
        priority: "MIDDLEWARE",
        fn(next) {
            const app = this.app;
            const _json = app.response.json;
            app.response.json = function(obj) {
                let snakeCaseObj = toSnakeCase(obj);
                return _json.call(this, snakeCaseObj);
            };
            next();
        }
    }
};
