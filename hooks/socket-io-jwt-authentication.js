/**
 * drg authentication bridge
 * TODO:- make this it's own module
 */
const jwtAuth = require('socketio-jwt-auth');

module.exports = {
    jwtAuthentication: {
        event: 'start',
        priority: 30,
        fn(next) {
            let sio = this.sio;

            if (!sio) {
                this.logger.warn('socket-io instance not found');
                return next();
            }

            if (!this.config.jwt) {
                this.logger.warn('JWT token not defined');
                return next();
            }

            sio.use(jwtAuth.authenticate(this.config.jwt, (payload, done) => {
                // TODO:- check if user with id exists?
                done(null, payload);
            }));

            next();
        }
    }
};
