const multer = require("multer");
const { resolve } = require("path");

const getFileFormatByFilename = filename => {
    let filenameList = filename.split(".");
    return "." + filenameList[filenameList.length - 1];
};

const storage = dirName =>
    multer.diskStorage({
        destination: (req, file, callback) => {
            callback(null, resolve(__dirname, `../public/${dirName}`));
        },
        filename: (req, file, callback) => {
            callback(
                null,
                `${file.fieldname}-${Date.now()}${getFileFormatByFilename(
                    file.originalname
                )}`
            );
        }
    });

module.exports = {
    upload: (name, dirName, maxCount, fileFilter) => (req, res, next) => {
        let multerOptions = { storage: storage(dirName) };

        if (fileFilter) {
            multerOptions.fileFilter = fileFilter;
        }

        let upload = multer(multerOptions).fields([{ name, maxCount }]);
        upload(req, res, err => {
            if (err) {
                return next(err);
            }
            return next();
        });
    },
    getFileFormatByFilename: getFileFormatByFilename,
    imageGridFilter(req, file, cb) {
        const { Component, ComponentFile } = req.models;
        const { componentUUID } = req.params;

        return Component.findOne({ where: { uuid: componentUUID } }).then(
            component => {
                if (component.typeId === Component.TYPE.IMAGE_GRID) {
                    return ComponentFile.isImage(file.path).then(isImage => {
                        req.fileValidationError =
                            "Invalid file type. Please upload an image.";
                        cb(null, false, req.fileValidationError);
                    });
                }
                cb(null, true, req);
            }
        );
    }
};
