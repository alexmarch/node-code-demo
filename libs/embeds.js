const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const url = require('url');

module.exports = {
    parseDoc: url => {
        return JSDOM.fromURL(url, {})
            .then(dom => {
                const document = dom.window.document;
                let data = {
                    title: null,
                    image: null,
                    description: null
                };
                const openGraphTitle = document.querySelectorAll('meta[property="og:title"]');
                const twitterTitle = document.querySelectorAll('meta[property^="twitter:title"], meta[name^="twitter:title"]');
                const schemaOrgTitle = document.querySelectorAll('meta[itemprop^="name"]');

                const openGraphImage = document.querySelectorAll('meta[property="og:image"]');
                const twitterImage = document.querySelectorAll('meta[property^="twitter:image"], meta[name^="twitter:image"]');
                const schemaOrgImage = document.querySelectorAll('meta[itemprop^="image"]');

                const openGraphDescription = document.querySelectorAll('meta[property="og:description"]');
                const twitterDescription = document.querySelectorAll('meta[property^="twitter:description"], meta[name^="twitter:description"]');
                const schemaOrgDescription = document.querySelectorAll('meta[itemprop^="description"]');

                if (openGraphTitle && openGraphTitle.length) {
                    data.title = openGraphTitle[0].getAttribute('content');
                }

                if (twitterTitle && twitterTitle.length) {
                    data.title = twitterTitle[0].getAttribute('content');
                }

                if (schemaOrgTitle && schemaOrgTitle.length) {
                    data.title = twitterTitle[0].getAttribute('content');
                }

                if (openGraphImage && openGraphImage.length) {
                    data.image = openGraphImage[0].getAttribute('content');
                }

                if (twitterImage && twitterImage.length) {
                    data.image = twitterImage[0].getAttribute('content');
                }

                if (schemaOrgImage && schemaOrgImage.length) {
                    data.image = schemaOrgImage[0].getAttribute('content');
                }

                if (openGraphDescription && openGraphDescription.length) {
                    data.description = openGraphDescription[0].getAttribute('content');
                }

                if (twitterDescription && twitterDescription.length) {
                    data.description = twitterDescription[0].getAttribute('content');
                }

                if (schemaOrgDescription && schemaOrgDescription.length) {
                    data.description = schemaOrgDescription[0].getAttribute('content');
                }

                return Promise.resolve(data);

            }).catch(err => Promise.reject({ err, message: `Parse URL error ${url}` }));
    },

    parseVideo: url => {
        return JSDOM.fromURL(url, {})
            .then(doc => {
            let host = url.parse(url);
            let description = document.querySelectorAll('#watch-description-text');
            let vinemoDescription = document.querySelectorAll('noscript > .description-wrapper');
            let data = {
                url,
                embedUrl: '',
                description: ''
            };

            if (host.hostname === 'www.youtube.com') {
                if (description && description.length) {
                    data.description = description[0].innerText;
                }
                const videoId = host.searchParams.get('v');

                if (videoId) {
                    data.embedUrl = `https://www.youtube.com/embed/${videoId}`
                }

            }

            if (host.hostname === 'vimeo.com') {
                if (vinemoDescription && vinemoDescription.length) {
                    data.description = vinemoDescription[0].innerText;
                }
                data.embedUrl =  `https://player.vimeo.com/video${host.path}`
            }

            return Promise.resolve(data);
        });
    }
}
