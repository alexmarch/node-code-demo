const bcrypt = require('bcrypt');
const { createHash } = require('crypto');

module.exports = {
  hashPassword: (password) => bcrypt.hash(password, bcrypt.genSaltSync(10)),
  verifyPassword: (password, passwordHash) => bcrypt.compareSync(password, passwordHash),
  genRndToken: () => {
    const sha1 = createHash('sha1');
    return sha1.update(String(Math.random())).digest('hex');
  },
  encodeObject: obj => encodeURIComponent(JSON.stringify(obj))
}
