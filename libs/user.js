const _ = require('lodash');
const { genRndToken } = require('../libs/crypto');
const fs = require('fs');
const Mustache = require('mustache');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const config = require('clout-js').config;
const mail = nodemailer.createTransport(smtpTransport(config.transport));
const path = require('path');


// Get admin context for email template
const getAdminCtx = (adminUser) => ({
    admin_first_name: adminUser.firstName,
    admin_last_name: adminUser.lastName,
    admin_title: adminProfile.get('title'),
    admin_email: adminUser.email,
    first_name: profile.user.get('firstName'),
    company_name: config.companyName,
    platform_title: config.platformTitle,
    set_password_link: `${config.frontEndUrl}/new-password/${profile.get('token')}`
});

module.exports = {
    userManagementPermissionsForPageIds: (user, ids, req) => {
        const { UserGuard, AuthUser, Page } = req.models;
        const readAction = { action: "read", internal: UserGuard.Permissions.Page.view };
        const writeAction = { action: "write", internal: UserGuard.Permissions.Page.change };
        const moderateAction = { action: "moderate", internal: UserGuard.Permissions.Page.moderate };

        return new Promise((resolve, reject) => {
            if (user.isSuperUser) { resolve([ readAction, writeAction, moderateAction ]); }
            Page.all()
                .then(pages => UserGuard.getGuardObjectsForUser(user, [UserGuard.Permissions.Page.moderate], UserGuard.Types.Page, pages))
                .then(guards => {
                    let idsList = _.indexOf(ids.map(id => !_.find(objects, { objectId: id })), false);
                    if (idsList >= 0) {
                        return reject("Operation not permitted.");
                    }
                    resolve([readAction, writeAction]);
                })
                .catch(err => reject(err));
        });
    },
    editUserPermissionsCheck: (editedUserId, user, req) => {
        const { AccountProfile, AuthUser } = req.models;

        return new Promise((resolve, reject) => {
            AccountProfile.findOne({ where: { userId: editedUserId }, include: [{ model: AuthUser, as: 'user' }] })
            .then(profile => {
                if (profile.user.get('isSuperUser') || profile.user.get('id') === user.id) { reject('Edited admin account user.') }
                AccountProfile.findOne({ where: { userId: user.id }, include: [{ model: AuthUser, as: 'user' }] })
                .then(reqUserProfile => {
                    if (reqUserProfile.get('role') != AccountProfile.Roles.moderator) { return resolve(profile); }
                    reqUserProfile.getModerators()
                        .then(moderators => {
                            let moderatorsList = moderators.filter(moderator => moderator.get('id') === user.id);
                            if (!moderatorsList.length) {
                                return reject('This is not moderator user.')
                            }
                            return resolve(profile);
                        });
                });
            });
        });
    },
    approveUser: (adminUser, profile, req) => {
        const { AccountProfile } = req.models;

        return new Promise((resolve, reject) => profile.update({ isApproved: true, token: genRndToken(), tokenCreatedAt: Date.now() })
            .then(() => {
                if (!mail) return resolve(profile);
                AccountProfile.findOne({ where: { userId: adminUser.id }})
                    .then(adminProfile => {
                        const ctx = getAdminCtx(adminProfile);
                        let mailOptions = {
                            from: config.defaultFromEmail,
                            to: profile.user.get('email'),
                            subject: `${ config.platformTitle } - New user request approved`,
                            html: Mustache.render(fs.readFileSync(
                                path.resolve(__dirname, '../view/email/access-invited.hbs'),
                                { encoding: 'utf8' }),
                                ctx)
                        };
                        mail.sendMail(mailOptions, (err, info) => {
                            if (!err) return resolve(profile);
                            reject(err);
                        });
                    });
            }));
    },
    rejectUser: (adminUser, profile, req) => {
        const { AccountProfile } = req.models;

        return new Promise((resolve, reject) => profile.update({ isApproved: false, token: null, tokenCreatedAt: null })
            .then(() => profile.user.update({ isActive: false }).then(() => {
                AccountProfile.findOne({ where: { userId: adminUser.id }})
                    .then(adminProfile => {
                        const ctx = getAdminCtx(adminProfile);
                        let mailOptions = {
                            from: config.defaultFromEmail,
                            to: profile.user.get('email'),
                            subject: `${ config.platformTitle } - New user request denied`,
                            html: Mustache.render(fs.readFileSync(
                                path.resolve(__dirname, '.../view/email/access-denied.hbs'),
                                { encoding: 'utf8' }),
                                ctx)
                        };
                        mail.sendMail(mailOptions, (err, info) => {
                            if (!err) return resolve(profile);
                            reject(err);
                        });
                    });
            })));
    },
    inviteUser: (req) => {
        const { AccountProfile, AuthUser } = req.models;
        const body = req.body || {};
        body.token = genRndToken();
        body.tokenCreatedAt = Date.now();
        body.isApproved = true;
        body.isActive = true;

        return new Promise((resolve, reject) => AuthUser.create(body)
        .then(user => {
            body.userId = user.get('id');
            AccountProfile.create(body)
                .then(profile => {
                    AccountProfile.findOne({ where: { userId: adminUser.id }})
                    .then(adminProfile => {
                        const ctx = getAdminCtx(adminProfile);
                        let mailOptions = {
                            from: config.defaultFromEmail,
                            to: profile.user.get('email'),
                            subject: `${ config.platformTitle } - New user invitation`,
                            html: Mustache.render(fs.readFileSync(
                                path.resolve(__dirname, '../conf/templates/users/email/access-invited.txt'),
                                { encoding: 'utf8' }),
                                ctx)
                        };
                        mail.sendMail(mailOptions, (err, info) => {
                            if (!err) return resolve(user.get({ plain: true }));;
                            reject(err);
                        });
                    });
                });
        }));
    },
    // Apply permissions for profile user
    applyProfilePermissions: (permissions, pages, profile, editedPages) => {
        return Promise.all(permissions.map(perm => UserGuard.removeUserPermission(profile.user, perm['internal'], UserGuard.Types.Page, pages)
        .then(() => {
            if (editedPages[perm['action']].length > 0) {
                return Page.findAll({ where: { id: { $in: editedPages[perm['action']] }}})
                    .then(resultPages => UserGuard.assignUserPermission(profile.user.get('id'), perm['internal'], UserGuard.Types.Page, resultPages));
            }
        })));
    }
}
