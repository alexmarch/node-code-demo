const SphinxClient = require("sphinxapi");
const { API_SPHINX_HOST, API_SPHINX_PORT } = require("clout-js").config;

module.exports = {
    connect: function() {
        return new Promise((resolve, reject) => {
            this.sphinxClient = new SphinxClient();
            this.sphinxClient.SetServer(
                API_SPHINX_HOST,
                Number(API_SPHINX_PORT)
            );
            this.sphinxClient.SetLimits(offset, limits);
            this.sphinxClient.Status((err, result) => {
                if (err) {
                    reject(
                        `Sphinx connection error (host: ${API_SPHINX_HOST}, port: ${API_SPHINX_PORT}) "${err}"`
                    );
                }
                resolve(this);
            });
        });
    },
    query: function(q, offset=0, limits=500, permissionIds, models) {
        let pageIds = [], componentIds = [], componentFileIds = [];
        let { Page, Component, ComponentFile } = models;

        this.sphinxClient.SetLimits(offset, limit);

        return Promise((resolve, reject) => {
            this.sphinxClient.Query(
                q,
                "pages,components,componentfiles",
                (err, result) => {
                    if (err) {
                        reject(err);
                    }

                    if (!result.matches && !result.matches.length) {
                        return resolve([]);
                    }

                    result.matches.forEach(obj => {
                        switch(obj.attrs.index_type) {
                            case 'page':
                                if (permissionIds.indexOf(obj.attrs.id)) {
                                    pageIds.push(obj.attrs.id);
                                }
                                break;
                            case 'component':
                                componentIds.push(obj.attrs.id);
                                break;
                            case 'componentfile':
                                componentFileIds.push(obj.attrs.id);
                                break;
                        };
                    });

                    return Promise.all([
                            Page.findAll({ where: { id: { $in: pageIds } }, include: [{ model: PageVersion, as: 'pageVersion' }]}),
                            Component.findAll({
                                        where: { id: { $in: componentIds } },
                                        include: [{ model: PageVersion, as: 'page_version', where: { pageId: { $in: permissionIds }} }]}),
                            ComponentFile.findAll({
                                        where: { id: { $in: componentFileIds } },
                                        include: [{
                                            model: Component, as: 'component',
                                            include: [{ model: PageVersion, as: 'page_version', where: { pageId: { $in: permissionIds } } }]
                                        }]})
                            ])
                            .then(results => {
                                let result = results
                                    .reduce((prev, next) => prev.concat(next))
                                    .map(itemModel => itemModel.serializer());

                                return resolve(result);
                            })
                            .catch(err => reject(err));

                }
            );
        });
    }
};
