const Mustache = require('mustache');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const { transport } = require('../conf/nodemailer-transport');
const fs = require('fs');
const mail = nodemailer.createTransport(smtpTransport(transport));
const path = require('path');

module.exports = {
    sendAdminEmail: (profile, subject, template, req) => {
        const { AccountProfile, AuthUser, Organisation } = req.models;

        return new Promise((resolve, reject) => {
            AuthUser.findAll({ where: { isSuperUser: true } })
                .then(admins => {
                    let mails = [];
                    let profileContext = {
                        first_name: profile.user.firstName,
                        last_name: profile.user.lastName,
                        title: profile.user.title,
                        email: profile.user.email,
                        organisation: profile.organisation.name,
                        message: profile.message,
                        backend_url: `${process.env.FRONTEND_URL}/app/users/detail/${profile.user.id}`
                    }

                    admins.forEach(admin => {
                        AccountProfile.findOne({ where: { userId: admin.get('id') } })
                            .then(adminProfile => {
                                if (!adminProfile) return reject('Admin profile not found.')
                                let ctx = {
                                    company_name: process.env.COMPANY_NAME,
                                    platform_title: process.env.PLATFORM_TITLE,
                                    admin_first_name: admin.get('firstName'),
                                    admin_last_name: admin.get('lastName'),
                                    admin_title: adminProfile.get('title'),
                                    admin_email: admin.get('email'),
                                }

                                mails.push(new Promise((resolve, reject) => {
                                    let mailOptions = {
                                        from: process.env.DEFAULT_FROM_EMAIL,
                                        to: ctx.admin_email,
                                        subject: `${process.env.PLATFORM_TITLE} - ${subject}`,
                                        html: Mustache.render(fs.readFileSync(
                                            path.resolve(__dirname, `../view/email/${template}.hbs`),
                                            { encoding: 'utf8' }),
                                            Object.assign({}, ctx, profileContext))
                                    };
                                    mail.sendMail(mailOptions, (err, info) => {
                                        if (!err) return resolve(profile);
                                        reject(err);
                                    });
                                }));
                            });
                    });
                    Promise.all(mails)
                        .then(() => resolve())
                        .catch(err => reject(err));
                });
        });
    },
    sendSetPasswordEmail: (profile, req) => {
        return new Promise((resolve, reject) => {
            let ctx = {
                'first_name': profile.user.firstName,
                'company_name': process.env.COMPANY_NAME,
                'platform_title': process.env.PLATFORM_TITLE,
                'set_password_link': `${process.env.FRONTEND_URL}/new-password/${profile.token}`
            };
            let mailOptions = {
                from: process.env.DEFAULT_FROM_EMAIL,
                to: profile.user.email,
                subject: `${process.env.PLATFORM_TITLE} - Reset password`,
                html: Mustache.render(fs.readFileSync(
                    path.resolve(__dirname, `../view/email/reset-password.hbs`),
                    { encoding: 'utf8' }),
                    ctx)
            };

            mail.sendMail(mailOptions, (err, info) => {
                if (!err) return resolve(profile);
                reject(err);
            });
        });
    },
    canUpdateProfile: () => (req, res, next) => {
        const userId = req.params.id;
        const user = req.user;
        const body = req.body;

        if ((user.id != req.params.id) && (!user.isSuperUser || (user.role && user.role === 'admin'))) {
            return res.status(403).json({ userId: userId, message: 'You can\'t update other user profile' });
        }
        if ((!user.isSuperUser || (user.role && user.role !== 'admin')) && (body.role === 'admin' || body.role === 'moderator')) {
            return res.status(403).json({ message: 'You can\'t update role to admin / moderator' });
        }
        next();
    },
    canGetAccount: () => (req, res, next) => {
        const accountId = req.params.id;
        const user = req.user;

        if (accountId != user.id && (!user.isSuperUser || (user.role && user.role !== 'admin')) ) return res.badRequest('Wrong user id.');
        next();
    }
}
