const jwt = require('jsonwebtoken');
const uid = require('rand-token').uid;
const { jwt: { secret } } = require('clout-js').config;
const { JWT_EXPIRES_IN } = process.env;

module.exports = {
    signPayload: payload => jwt.sign(payload, secret, {
        expiresIn: JWT_EXPIRES_IN
    }),
    genRefreshToken: () => uid(40),
    verifyToken: token => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, secret, (err, result) => {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            })
        })
    }
}
