const http = require("http");
const _ = require("lodash");
const thumb = require("node-thumbnail").thumb;

module.exports = {
    isValidUrl: url =>
        /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(
            url
        ),
    toCamelCase: data => {
        let dataCopy = Object.assign({}, data);
        _.each(dataCopy, (v, k) => {
            dataCopy[_.camelCase(k)] = v;
        });
        return dataCopy;
    },
    downloadData: function(url) {
        return Promise((resolve, reject) => {
            if (!this.isValidUrl(url)) {
                return reject(new Error(`Url {url} not valid.`));
            }
            http.get(url, res => {
                const statusCode = res.statusCode;
                let rowData = "";

                if (statusCode !== 200)
                    return reject(new Error(`Error code ${statusCode}.`));

                res.on("data", chunk => (rowData += chunk));
                res
                    .on("end", () => resolve(rowData))
                    .on("error", e => reject(e));
            });
        });
    },
    toSnakeCase: function(obj) {
        let origin = Object.assign({}, obj);

        if (!obj) throw new Error("Object not defined.");

        const _toSnakeCase = obj => {
            _.each(obj, (value, key) => {
                if (_.isObject(value)) {
                    _toSnakeCase(value);
                }
                obj[_.snakeCase(key)] = value;
            });
            return obj;
        };

        return _toSnakeCase(origin);
    },
    makeThumbnail: (path, dest) => {
        return thumb({
            prefix: "",
            suffix: "",
            width: 800,
            source: path,
            destination: dest
        });
    }
};
