const path = require('path');
const config = require('clout-js').config;
const { encodeObject } = require('./crypto');

/**
 * @description SSO APIs
 */
const loadSettings = (req) => {
    const settings = require(path.resolve(__dirname, `../${config.samlFolder}/settings.json`));
    const advSettings = require(path.resolve(__dirname, `../${config.samlFolder}/advanced_settings.json`));
    const basicSettings = Object.assign({}, settings, advSettings);
    const host = req.hostname;

    let acsUrl = basicSettings.sp.assertionConsumerService.url;
    acsUrl = acsUrl.replace('ENV_DJANGO_HOST', host);
    basicSettings.sp.assertionConsumerService.url = acsUrl;

    let entityId = basicSettings.sp.entityId;
    entityId = entityId.replace('ENV_DJANGO_HOST', host);
    basicSettings.sp.entityId = entityId;

    let slsUrl = basicSettings.sp.singleLogoutService.url;
    slsUrl = slsUrl.replace('ENV_DJANGO_HOST', host);
    basicSettings.sp.singleLogoutService.url = slsUrl;

    return basicSettings;
};
/**
 *
 * @param {Object} profile
 * @param {Object} res
 */
const genRedirectUrl = (profile, res) => {
    const host = config.frontUrl;
    const attrs = encodeObject({ token: profile.token });
    let url;

    if (profile.ssoLoginCompleted) {
        url = `${host}/login/sso?${attrs}`;
    } else {
        url = `${host}/sso/additional-details?${attrs}`;
    }
    return res.redirect(url);
};

const getSamlNameId = (req, postData, getData) => {
    let samlNameId;
    if (postData && postData.samlNameId) {
        samlNameId = postData.samlNameId;
    } else if (getData && getData.samlNameId) {
        samlNameId = getData.samlNameId;
    } else if(req.session && req.session.samlNameId) {
        samlNameId = req.session.samlNameId;
    }
    return samlNameId;
};

const getSessionIndex = req => (
    req.session['samlSessionIndex'] || null
);

const getUserData = req => {
    if (req.session['samlUserdata']) {
        return req.session['samlUserdata'];
    } else if(req.query.samlUserdata) {
        return JSON.parse(req.query.samlUserdata)
    }
    return {};
};

const getQueryData = req => {
    const { query } = req;
    return query ? Object.keys(query).length > 0 ? req.query : null : null;
};

const getBodyData = req => {
    const { body } = req;
    return body ? Object.keys(body).length > 0 ? req.body : null : null;
};

module.exports = {
    loadSettings,
    genRedirectUrl,
    getSamlNameId,
    getSessionIndex,
    getUserData,
    getQueryData,
    getBodyData
};
