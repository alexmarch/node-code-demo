const passport = require('passport');
const { verifyToken } = require('./jwt');

module.exports = {
    jwtAuthenticate: () => passport.authenticate('jwt', { session: false }),
    jwtUserSerialize: () => (req, res, next) => {
        if (req.headers.authorization) {
           const token = req.headers.authorization.split(' ')[1];
           verifyToken(token).then(user => {
               req.user = user;
               next();
           }).catch(err => { next(err) });
        } else {
            next();
        }
    },
    IsAdminOrModerator: () => (req, res, next) => {
        const { AccountProfile } = req.models;
        AccountProfile.findOne({ where: { userId: req.user.id } })
            .then(profile => {
                const isAdminOrModerator = () => {
                    let isRoleAdminOrModerator = [ AccountProfile.Roles.admin, AccountProfile.Roles.moderator ].indexOf(profile.get('role')) >= 0;
                    return  isRoleAdminOrModerator || req.user.isSuperUser;
                };

                if (isAdminOrModerator()) {
                    return next();
                };

                res.unauthorized({
                    message: 'Wrong user type.'
                });
            })
            .catch(err => res.unauthorized({ err, message: 'User not authorized.' }))
    }
}
