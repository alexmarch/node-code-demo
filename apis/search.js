const { jwtAuthenticate } = require('../libs/auth');
const sphinx = require('../libs/sphinx');

/**
 * Search APIs
 */
module.exports = {
    search: {
        path: '/v1/search',
        method: 'get',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const user = req.user;
            const { q, offset, limit } = req.query;
            const { AuthUser, Page, UserGuard } = req.models;

            const getPageIds = pages => {
                // @TODO: tree_pk_list later need improve this
                return pages.map(page => (page.id || page.get('id')))
            };

            const searchInPages = pageIds => {
                return sphinx.connect()
                    .then(client => client.query(q, offset, limit, pageIds))
                    .then(results => res.json({ results }))
                    .catch(err => res.badRequest(err));
            };

            if (AuthUser.isAdmin(user)) {
                return Page.all()
                    .then(pages => Promise.resolve(getPageIds(pages)))
                    .then(ids => searchInPages(ids));
            }

            AuthUser.getControlledPagesForUser(user, [
                UserGuard.Permissions.Page.view,
                UserGuard.Permissions.Page.change
            ])
            .then(pages => Promise.resolve(getPageIds(pages)))
            .then(ids => searchInPages(ids));
        }
    }
};
