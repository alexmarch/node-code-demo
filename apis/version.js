const { jwtAuthenticate } = require('../libs/auth');

module.exports = {
    retrive: {
        retrieve: {
            path: '/v1/version/:pageId',
            method: 'get',
            hooks: [jwtAuthenticate()],
            fn: (req, res) => {
                const userId = req.user.id;
                const pageId = req.params.pageId;
                const { PageVersion, Page } = req.models;

                PageVersion.findAll({
                    where: { pageId, ownerId: userId },
                    order: [['version', 'DESC']],
                    include: [{ model: Page, as: 'page' }]
                })
                    .then(versions =>
                        res.json(versions.map(ver => ver.serializer()))
                    )
                    .catch(err =>
                        res.notFound({
                            err,
                            message: 'Page version not found.'
                        })
                    );
            }
        }
    }
};
