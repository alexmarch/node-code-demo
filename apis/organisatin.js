// Organisation APIs
module.exports = {
    viewAll: {
        path: '/v1/organisations',
        method: 'get',
        fn: (req, res) => {
            const { Organisation } = req.models;

            Organisation.all()
                .then(organisations =>
                    res.json({
                        organisations: organisations.map(organisation =>
                            organisation.serializer()
                        )
                    })
                )
                .catch(err => res.badRequest({ err }));
        }
    },
    ViewById: {
        path: '/v1/organisations/:id',
        method: 'get',
        fn: (req, res) => {
            const { Organisation } = req.models;
            const id = req.params.id;

            Organisation.findById(id)
                .then(organisation => res.json(organisation.serializer()))
                .catch(err => res.badRequest({ err }));
        }
    }
};
