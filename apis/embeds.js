const { jwtAuthenticate } = require('../libs/auth');
const { parseDoc } = require('../libs/embeds');

// Embeds API
module.exports = {
    extract: {
        path: '/v1/embeds/extract',
        method: 'get',
        hooks: [ jwtAuthenticate() ],
        fn: (req, res) => {
            const { url } = req.query;
            if (!url) {
                return res.badRequest('Unable to generate preview for the specified URL.');
            }
            parseDoc(url)
                .then(data => res.json({ title: data.title, image: data.image, description: data.description }))
                .catch(err => res.badRequest(err));
        }
    },
    video: {
        path: '/v1/embeds/video',
        method: 'get',
        hooks: [ jwtAuthenticate() ],
        fn: (req, res) => {
            const { url } = req.query;
            if (!url) {
                return res.badRequest('Unable to generate preview for the specified URL.');
            }
            parseImage(url)
                .then(data => res.json({ title: data.title, image: data.image, description: data.description }))
                .catch(err => res.badRequest(err));
        }
    }
}
