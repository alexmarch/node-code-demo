const { jwtAuthenticate } = require('../../libs/auth');
const { downloadData } = require('../../libs/utils');
const { upload } = require('../../libs/upload');
const { componentFileLinksImagePath } = require('clout-js').config;
const fs = require('fs');

/**
 * Links APIs
 */
module.exports = {
    list: {
        path: '/v1/pages/:pageId/components/:componentUUID/links',
        method: 'get',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const user = req.user;
            const { pageId, componentUUID } = req.params;
            const { Page, PageVersion, Component, ComponentFile } = req.models;

            Page.findOne({
                where: { id: pageId },
                include: [{ model: PageVersion, as: 'pageVersion' }]
            })
                .then(page => {
                    if (!page.pageVersion) {
                        return res.notFound(`Page version not found.`);
                    }
                    return Component.findOne({
                        where: {
                            uuid: componentUUID
                        }
                    }).then(component =>
                        ComponentFile.findAll({
                            where: { componentId: component.get('id') },
                            order: [['position']]
                        })
                    );
                })
                .then(files =>
                    res.json({ results: files.map(file => file.serializer()) })
                )
                .catch(err => res.notFound(`Page ${pageId} not found.`));
        }
    },
    create: {
        path: '/v1/pages/:pageId/components/:componentUUID/links',
        method: 'post',
        hooks: [
            jwtAuthenticate(),
            upload('data', componentFileLinksImagePath, 1)
        ],
        fn: (req, res) => {
            const user = req.user;
            const { pageId, componentUUID } = req.params;
            const { Page, Component, ComponentFile, PageVersion } = req.models;

            let data = req.body;

            const createComponentFile = (page, component) => {
                return ComponentFile.createComponentFile(data, component).then(
                    fileComponent => {
                        return page
                            .lockFor(user)
                            .then(() => Promise.resolve(fileComponent));
                    }
                );
            };

            Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageVersion,
                        as: 'pageVersion'
                    }
                ]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    return page
                        .shadowVersion(null, user)
                        .then(pageVersion => {
                            return page.lockFor(user).then(() =>
                                Component.findOne({
                                    where: {
                                        uuid: componentUUID
                                    },
                                    order: [['position']]
                                })
                            );
                        })
                        .then(component => {
                            if (data.data && isValidUrl(data.data)) {
                                return downloadData(data.data).then(rowData => {
                                    data.data = rowData;
                                    return createComponentFile(page, component);
                                });
                            } else if (req.files.data && req.files.data[0]) {
                                const file = req.files.data[0];

                                data.data = file.path;
                                data.originalFileName = file.originalname;
                            }

                            return createComponentFile(page, component);
                        });
                })
                .then(fileComponent => res.json(fileComponent.serializer()))
                .catch(err =>
                    res.notFound({
                        err,
                        message: `Page ${pageId} not found.`
                    })
                );
        }
    },
    update: {
        path: '/v1/pages/:pageId/components/:componentUUID/links/:linkUUID',
        method: 'patch',
        hooks: [
            jwtAuthenticate(),
            upload('data', componentFileLinksImagePath, 1)
        ],
        fn: (req, res) => {
            const user = req.user;
            const { pageId, componentUUID, linkUUID } = req.params;
            const { Page, Component, ComponentFile, PageVersion } = req.models;

            let data = req.body;

            const updateComponentFile = component => {
                return ComponentFile.findOne({
                    where: {
                        componentId: component.get('id'),
                        uuid: linkUUID
                    }
                }).then(fileComponent => {
                    return ComponentFile.updateComponentFile(
                        data,
                        component,
                        linkUUID
                    );
                });
            };

            Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageVersion,
                        as: 'pageVersion'
                    }
                ]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    return page
                        .shadowVersion(null, user)
                        .then(pageVersion => {
                            return page.lockFor(user).then(() =>
                                Component.findOne({
                                    where: {
                                        uuid: componentUUID
                                    },
                                    order: [['position']]
                                })
                            );
                        })
                        .then(component => {
                            if (data.data && isValidUrl(data.data)) {
                                return downloadData(data.data).then(rowData => {
                                    data.data = rowData;
                                    return updateComponentFile(component);
                                });
                            } else if (req.files.data && req.files.data[0]) {
                                let file = req.files.data[0];

                                data.data = file.path;
                                data.originalFileName = file.originalname;
                            }

                            return updateComponentFile(component);
                        });
                })
                .then(fileComponent => res.json(fileComponent.serializer()))
                .catch(err =>
                    res.notFound({
                        err,
                        message: `Page ${pageId} not found.`
                    })
                );
        }
    },
    partialUpdate: {
        path: '/v1/pages/:pageId/components/:componentUUID/links/:linkUUID',
        method: 'put',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {}
    },
    retrieve: {
        path: '/v1/pages/:pageId/components/:componentUUID/links/:linkUUID',
        method: 'get',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const { pageId, componentUUID, linkUUID } = req.params;
            const { version } = req.query;
            const { Page, Component, ComponentFile, PageVersion } = req.models;

            const getComponentLinkFile = pageVersion => {
                return Component.findOne({
                    where: {
                        uuid: componentUUID,
                        pageVersion: pageVersion.get('id')
                    }
                }).then(component =>
                    ComponentFile.findOne({
                        where: {
                            componentId: component.get('id'),
                            uuid: linkUUID
                        }
                    })
                );
            };

            Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageVersion,
                        as: 'pageVersion'
                    }
                ]
            })
                .then(page => {
                    if (!page.pageVersion) {
                        return res.notFound(`Page version not found.`);
                    }

                    if (version) {
                        return PageVersion.findOne({
                            where: { pageId, version }
                        }).then(pageVersion =>
                            getComponentLinkFile(pageVersion)
                        );
                    }

                    return getComponentLinkFile(page.pageVersion);
                })
                .then(file => res.json(file.serializer()))
                .catch(err => res.notFound(`Page ${pageId} not found.`));
        }
    },
    destroy: {
        path: '/v1/pages/:pageId/components/:componentUUID/links/:linkUUID',
        method: 'delete',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const user = req.user;
            const { pageId, componentUUID, linkUUID } = req.params;
            const { Page, Component, ComponentFile, PageVersion } = req.models;

            Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageVersion,
                        as: 'pageVersion'
                    }
                ]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    return page.shadowVersion(null, user).then(pageVersion => {
                        return page.lockFor(user).then(() =>
                            Component.findOne({
                                where: {
                                    uuid: componentUUID
                                },
                                order: [['position']]
                            })
                        );
                    });
                })
                .then(component =>
                    ComponentFile.findOne({
                        where: {
                            componentId: component.get('id'),
                            uuid: linkUUID
                        }
                    })
                )
                .then(fileComponent => fileComponent.destroy())
                .then(() => res.ok())
                .catch(err =>
                    res.notFound({
                        err,
                        message: `Page ${pageId} not found.`
                    })
                );
        }
    }
};
