const { jwtAuthenticate } = require('../../libs/auth');
const uuidv4 = require('uuid/v4');
const { toCamelCase } = require('../../libs/utils');

/**
 * Components APIs
 */
module.exports = {
    list: {
        path: '/v1/pages/:pageId/components',
        method: 'get',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const pageId = req.params.pageId;
            let { limit, offset, page } = req.query;
            const { Page, PageVersion, Component } = req.models;
            let numberOfPages = 0;
            let count = 0;
            let version = req.query.version;

            const findNextVersion = (page, version) => {
                return PageVersion.findOne({
                    where: { pageId: page.get('id'), version: version + 1 }
                }).then(ver => {
                    if (ver !== null) {
                        return Promise.resolve({ page, version: version + 1 });
                    }
                    return Promise.resolve({ page, version });
                });
            };

            const getComponents = result => {
                return PageVersion.findOne({
                    where: {
                        pageId: result.page.get('id'),
                        version: result.version
                    }
                }).then(ver => {
                    return Component.findAndCountAll({
                        where: { pageVersion: ver.get('id') }
                    }).then(componentCount => {
                        count = componentCount.count;

                        if (limit === undefined) limit = 20;

                        numberOfPages = Math.ceil(count / limit);

                        if (page) {
                            offset = limit * (page - 1);
                        }

                        return Component.findAll({
                            where: { pageVersion: ver.get('id') },
                            limit,
                            offset,
                            order: [['position', 'ASC']]
                        });
                    });
                });
            };

            Page.findOne({
                where: { id: pageId },
                include: [{ model: PageVersion, as: 'pageVersion' }]
            })
                .then(page => {
                    if (!version) {
                        version = page.pageVersion.get('version');
                        // if shadow version for current user exists - serve components from it
                        if (page.isLockedBy(req.user)) {
                            return findNextVersion(page, version);
                        }
                    }
                    return Promise.resolve({ page, version });
                })
                .then(result => getComponents(result))
                .then(components =>
                    res.json({
                        results: components.map(comp => comp.serializer()),
                        count: components.length,
                        numberOfPages,
                        limit
                    })
                )
                .catch(err =>
                    res.notFound({ err, message: `Page ${pageId} not found.` })
                );
        }
    },
    create: {
        path: '/v1/pages/:pageId/components',
        method: 'post',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const user = req.user;
            const pageId = req.params.pageId;
            const { Page, PageVersion, Component } = req.models;
            const data = toCamelCase(req.body);

            const createComponent = (version = null, page) => {
                const versionId = version ? version.get('id') : null;
                return page
                    .shadowVersion(versionId, user)
                    .then(pageVer => {
                        if (pageVer) {
                            data.pageVersion = pageVer.get('id');
                        }
                        return page.lockFor(user);
                    })
                    .then(() => Component.create(data))
                    .then(component => {
                        return PageVersion.findById(
                            component.get('pageVersion')
                        ).then(version =>
                            Promise.resolve({ component, version })
                        );
                    })
            };

            Page.findOne({
                where: { id: pageId },
                include: [{ model: PageVersion, as: 'pageVersion' }]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    /*
				        Here we must switch the original page version with the versioned we just cloned, otherwise
				        DRF instantiates objects from old page version
        			*/

                    if (!data['pageVersion']) {
                        return createComponent(null, page);
                    }

                    return PageVersion.findOne({
                        where: {
                            version: data.pageVersion,
                            pageId
                        },
                        include: [{ model: Page, as: 'page' }]
                    }).then(version => createComponent(version, page));
                })
                .then(({ component, version }) => {
                    component = component.serializer();
                    if (version) {
                        component.page_version = version.get({ plain: true });
                    }
                    return res.json(component);
                })
                .catch(err => {
                    if (err) {
                        return res.badRequest(err);
                    }
                    return res.notFound(`Page ${pageId} not found.`);
                });
        }
    },
    update: {
        path: '/v1/pages/:pageId/components/:componentUUID',
        method: 'patch',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const { pageId, componentUUID } = req.params;
            const { Page, PageVersion, Component } = req.models;
            const user = req.user;
            const data = req.body;

            Page.findOne({
                where: { id: pageId },
                order: [['position', 'ASC']],
                include: [{ model: PageVersion, as: 'pageVersion' }]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    return page
                        .shadowVersion(null, user)
                        .then(pageVer => {
                            return Component.findOne({
                                where: { uuid: componentUUID }
                            });
                        })
                        .then(component => {
                            if (data.partial) {
                                return component.updateAttributes(data);
                            }
                            return component.update(data);
                        })
                        .then(() => page.lockFor(user))
                        .then(() => res.json(data));
                })
                .catch(err =>
                    res.notFound({ err, message: `Page ${pageId} not found.` })
                );
        }
    },
    partialUpdate: {
        path: '/v1/pages/:pageId/components/:id',
        method: 'put',
        hooks: [jwtAuthenticate()],
        fn: function(req, res) {
            req.body.partial = true;
            this.update(req, res);
        }
    },
    retrieve: {
        path: '/v1/pages/:pageId/components/:componentUUID',
        method: 'get',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const { pageId, componentUUID } = req.params;
            const { Page, PageVersion, Component } = req.models;
            let version = req.query.version;
            let pageVersion = { model: PageVersion, as: 'pageVersion' };

            if (version) {
                pageVersion.where = { version };
            }

            Page.findOne({
                where: { id: pageId },
                include: [pageVersion]
            })
                .then(page =>
                    Component.findOne({
                        where: {
                            uuid: componentUUID,
                            pageVersion: page.pageVersion.get('id')
                        },
                        include: [{ model: PageVersion, as: 'page_version' }]
                    })
                )
                .then(comp => res.json(comp.serializer()))
                .catch(err => res.notFound(`Page ${pageId} not found.`));
        }
    },
    destroy: {
        path: '/v1/pages/:pageId/components/:componentUUID',
        method: 'delete',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const { pageId, componentUUID } = req.params;
            const user = req.user;
            const { Page, PageVersion, Component } = req.models;

            Page.findOne({
                where: { id: pageId },
                order: [['position', 'ASC']],
                include: [{ model: PageVersion, as: 'pageVersion' }]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    return page
                        .shadowVersion(null, user)
                        .then(pageVer =>
                            Component.findOne({
                                where: { uuid: componentUUID }
                            })
                        )
                        .then(component => component.destroy(req.body))
                        .then(() => page.lockFor(user))
                        .then(() =>
                            res.json({
                                message: `Component ${componentUUID} was deleted.`
                            })
                        );
                })
                .catch(err => res.notFound(`Page ${pageId} not found.`));
        }
    },
    duplicate: {
        path: '/v1/pages/:pageId/components/:componentUUID/duplicate',
        method: 'post',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const { pageId, componentUUID } = req.params;
            const { Page, PageVersion, Component } = req.models;
            const user = req.user;
            let newComponent;

            Page.findOne({
                where: { id: pageId },
                order: [['position', 'ASC']],
                include: [{ model: PageVersion, as: 'pageVersion' }]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    return page
                        .shadowVersion(null, user)
                        .then(pageVer =>
                            Component.find({
                                where: { uuid: componentUUID }
                            })
                        )
                        .then(component => {
                            newComponent = component.get({ plain: true });
                            newComponent.id = undefined;
                            newComponent.position = newComponent.position + 1;
                            newComponent.uuid = uuidv4();
                            // @TODO:  component_files = list(component.files.all())
                            return Component.create(newComponent);
                        })
                        .then(component => {
                            return page
                                .lockFor(user)
                                .then(() => res.json(component.serializer()));
                        });
                })
                .catch(err => res.notFound(`Page ${pageId} not found.`));
        }
    }
};
