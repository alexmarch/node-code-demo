const { jwtAuthenticate } = require('../../libs/auth');
const { isValidUrl, downloadData } = require('../..//libs/utils');
const { upload } = require('../../libs/upload');
const { componentFileImagePath } = require('clout-js').config;
const fs = require('fs');

/**
 * File APIs
 */
module.exports = {
    list: {
        path: '/v1/pages/:pageId/components/:componentUUID/files',
        method: 'get',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const user = req.user;
            const { pageId, componentUUID } = req.params;
            const { version } = req.query;
            const { Page, Component, ComponentFile, PageVersion } = req.models;

            const getComponentFile = pageVersion => {
                return Component.findOne({
                    where: {
                        uuid: componentUUID,
                        pageVersion: pageVersion.get('id')
                    }
                }).then(component =>
                    ComponentFile.findAll({
                        where: { componentId: component.get('id') },
                        order: [['position']]
                    })
                );
            };

            Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageVersion,
                        as: 'pageVersion'
                    }
                ]
            })
                .then(page => {
                    if (!page.pageVersion) {
                        return res.notFound(`Page version not found.`);
                    }

                    if (version) {
                        return PageVersion.findOne({
                            where: { pageId, version }
                        }).then(pageVersion => getComponentFile(pageVersion));
                    }

                    return getComponentFile(page.pageVersion);
                })
                .then(files =>
                    res.json({ results: files.map(file => file.serializer()) })
                )
                .catch(err => res.notFound(`Page ${pageId} not found.`));
        }
    },
    create: {
        path: '/v1/pages/:pageId/components/:componentUUID/files',
        method: 'post',
        hooks: [jwtAuthenticate(), upload('data', componentFileImagePath, 1)],
        fn: (req, res) => {
            const user = req.user;
            const pageId = req.params.pageId;
            const componentUUID = req.params.componentUUID;
            const { Page, Component, ComponentFile, PageVersion } = req.models;

            let data = req.body;

            const createComponentFile = (page, component) => {
                return ComponentFile.createComponentFile(data, component).then(
                    fileComponent => {
                        return page
                            .lockFor(user)
                            .then(() => {
                                if (fileComponent) {
                                    return Promise.resolve(fileComponent)
                                }
                                return Promise.reject('Bad file component.');
                            });
                    }
                );
            };

            // page must be locked to be able to make any changes
            Page.findOne({
                where: { id: pageId },
                include: [{ model: PageVersion, as: 'pageVersion', where: { pageId } }]
            })
                .then(page => {
                    return page.shadowVersion(null, user)
                        .then(pageVersion =>
                            Component.findOne({
                                where: { uuid: componentUUID }
                            })
                        )
                        .then(component => {
                            data.componentId = component.get('id');

                            if (data.data && isValidUrl(data.data)) {
                                // @TODO: Need later improve downloadData function
                                return downloadData(data.data).then(rowData => {
                                    data.data = rowData;
                                    return createComponentFile(page, component);
                                });
                            } else if (req.files.data && req.files.data[0]) {
                                let file = req.files.data[0];

                                data.data = file.path;
                                data.originalFileName = file.originalname;

                                if (
                                    component.get('typeId') ===
                                    Component.TYPE.IMAGE_GRID
                                ) {
                                    return ComponentFile.isImage(
                                        file.path
                                    ).then(isImage => {
                                        if (!isImage) {
                                            fs.unlinkSync(file.path);
                                            return Promise.reject(
                                                'Invalid file type. Please upload an image.'
                                            );
                                        } else {
                                            return createComponentFile(page, component);
                                        }
                                    });
                                }
                            }
                            return createComponentFile(page, component);
                        });
                })
                .then(fileComponent => res.json(fileComponent.get({ plain: true })))
                .catch(err => {
                    if (err) {
                        return res.badRequest(err);
                    }
                    res.notFound({ err, message: 'Page not found.' });
                });
        }
    },
    retrive: {
        path: '/v1/pages/:pageId/components/:componentUUID/files/:fileUUID',
        method: 'get',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const user = req.user;
            const { pageId, componentUUID, fileUUID } = req.params;
            const { version } = req.query;
            const { Page, Component, ComponentFile, PageVersion } = req.models;

            const getComponentFile = pageVersion => {
                return Component.findOne({
                    where: {
                        uuid: componentUUID,
                        pageVersion: pageVersion.get('id')
                    }
                }).then(component =>
                    ComponentFile.findOne({
                        where: {
                            componentId: component.get('id'),
                            uuid: fileUUID
                        }
                    })
                );
            };

            Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageVersion,
                        as: 'pageVersion'
                    }
                ]
            })
                .then(page => {
                    if (!page.pageVersion) {
                        return res.notFound(`Page version not found.`);
                    }

                    if (version) {
                        return PageVersion.findOne({
                            where: { pageId, version }
                        }).then(pageVersion => getComponentFile(pageVersion));
                    }

                    return getComponentFile(page.pageVersion);
                })
                .then(file => res.json(file.serializer()))
                .catch(err => res.notFound(`Page ${pageId} not found.`));
        }
    },
    update: {
        path: '/v1/pages/:pageId/components/:componentUUID/files/:fileUUID',
        method: 'patch',
        hooks: [jwtAuthenticate(), upload('data', componentFileImagePath, 1)],
        fn: (req, res) => {
            const user = req.user;
            const { pageId, componentUUID, fileUUID } = req.params;
            const { Page, Component, PageVersion, ComponentFile } = req.models;
            let data = req.body;

            const updateComponentFile = component => {
                return ComponentFile.findOne({
                    where: {
                        componentId: component.get('id'),
                        uuid: fileUUID
                    }
                }).then(fileComponent => {
                    return ComponentFile.updateComponentFile(
                        data,
                        component,
                        fileUUID
                    );
                });
            };

            Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageVersion,
                        as: 'pageVersion'
                    }
                ]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    return page.shadowVersion(null, user).then(pageVersion => {
                        return page.lockFor(user).then(() => {
                            return Component.findOne({
                                where: {
                                    uuid: componentUUID
                                },
                                order: [['position']]
                            });
                        });
                    });
                })
                .then(component => {
                    data.componentId = component.get('id');

                    if (data.data && isValidUrl(data.data)) {
                        return downloadData(data.data).then(rowData => {
                            data.data = rowData;
                            return updateComponentFile(component);
                        });
                    } else if (req.files.data && req.files.data[0]) {
                        let file = req.files.data[0];

                        data.data = file.path;
                        data.originalFileName = file.originalname;

                        if (
                            component.get('typeId') ===
                            Component.TYPE.IMAGE_GRID
                        ) {
                            return ComponentFile.isImage(file.path).then(
                                isImage => {
                                    if (!isImage) {
                                        fs.unlinkSync(file.path);
                                        return Promise.reject(
                                            'Invalid file type. Please upload an image.'
                                        );
                                    }
                                }
                            );
                        }
                    }

                    return updateComponentFile(component);
                })
                .then(fileComponent => res.json(fileComponent.serializer()))
                .catch(err => {
                    if (err) {
                        return res.badRequest(err);
                    }
                    res.notFound({ err, message: `Page ${pageId} not found.` });
                });
        }
    },
    partialUpdate: {
        path: '/v1/pages/:pageId/components/:componentUUID/files/:fileUUID',
        method: 'put',
        hooks: [jwtAuthenticate()],
        fn: function(req, res) {
            req.body.partial = true;
            this.update(req, res);
        }
    },
    destroy: {
        path: '/v1/pages/:pageId/components/:componentUUID/files/:fileUUID',
        method: 'delete',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const user = req.user;
            const { pageId, componentUUID, fileUUID } = req.params;
            const { Page, Component, PageVersion, ComponentFile } = req.models;

            const deleteComponentFile = component => {
                return ComponentFile.findOne({
                    where: {
                        componentId: component.get('id'),
                        uuid: fileUUID
                    }
                }).then(fileComponent => {
                    return fileComponent.destroy();
                });
            };

            Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageVersion,
                        as: 'pageVersion'
                    }
                ]
            })
                .then(page => page.assertPageIsLockedBy(user))
                .then(page => {
                    return page.shadowVersion(null, user).then(pageVersion => {
                        return page.lockFor(user).then(() => {
                            return Component.findOne({
                                where: {
                                    uuid: componentUUID
                                },
                                order: [['position']]
                            });
                        });
                    });
                })
                .then(component => {
                    return deleteComponentFile(component);
                })
                .then(fileComponent => res.json(fileComponent.serializer()))
                .catch(err =>
                    res.notFound({ err, message: `Page ${pageId} not found.` })
                );
        }
    }
};
