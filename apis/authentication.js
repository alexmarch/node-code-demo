const { verifyPassword } = require('../libs/crypto');
const { signPayload, verifyToken, genRefreshToken } = require('../libs/jwt');
const { jwtAuthenticate } = require('../libs/auth');

/**
 * @description Authentication APIs
 */
module.exports = {
    login: {
        path: '/v1/auth/login',
        method: 'post',
        fn: (req, res) => {
            const { email, password } = req.body;
            const { AuthUser } = req.models;
            AuthUser.findOne({
                where: {
                    email: email.toLowerCase()
                }
            })
                .then(user => {
                    user = user.serializer({
                        plain: true
                    });
                    if (!verifyPassword(password, user.password)) {
                        return Promise.reject('Invalid Password');
                    }
                    const token = signPayload(user);
                    const refreshToken = genRefreshToken();
                    res.json({
                        token,
                        refreshToken
                    });
                })
                .catch(err => {
                    res.badRequest({
                        err,
                        message: 'Unable to login with provided credentials.'
                    });
                });
        }
    },
    unlockAllPages: {
        path: '/v1/auth/unlock-all-pages',
        method: 'post',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {}
    },
    refreshToken: {
        path: '/v1/auth/refresh-token',
        mathod: 'post',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const { refreshToken } = req.body;
            // @TODO: Refresh token implement logic here
        }
    },
    verifyToken: {
        path: '/v1/auth/verify-token',
        mathod: 'post',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const { token } = req.body;
            verifyToken(token)
                .then(user => {
                    res.json(user);
                })
                .catch(err => {
                    res.unauthorized({
                        err,
                        message: 'The token is invalid.'
                    });
                });
        }
    }
};
