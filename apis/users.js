const { jwtAuthenticate, IsAdminOrModerator } = require('../libs/auth');
const { hashPassword } = require('../libs/crypto');
const { upload } = require('../libs/upload');
const {
    userManagementPermissionsForPageIds,
    editUserPermissionsCheck,
    inviteUser,
    managedUsers,
    approveUser,
    rejectUser,
    applyProfilePermissions
} = require('../libs/user');
const { avatarImagePath } = require('clout-js').config;
const thumb = require('node-thumbnail').thumb;
const _ = require('lodash');

/**
 * @description Users APIs
 */
module.exports = {
    bulk: {
        path: '/v1/users/bulk',
        methods: ['patch', 'put'],
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const { AccountProfile } = req.models;
            AccountProfile.bulkUpdate(req.body, req.method === 'PATCH')
                .then(data => res.json({ data }))
                .catch(err =>
                    res.badRequest({ err, message: 'Bulk update bad request.' })
                );
        }
    },
    usersList: {
        path: '/v1/users',
        method: 'get',
        hooks: [jwtAuthenticate(), IsAdminOrModerator()],
        fn: (req, res) => {
            const { AuthUser } = req.models;

            AuthUser.getUsersByQueryParams(req.user, req.query)
                .then(users =>
                    res.json({ results: users, count: users.length })
                )
                .catch(err =>
                    res.badRequest({
                        err,
                        message: 'Get all users bad request.'
                    })
                );
        }
    },
    update: {
        path: '/v1/users/:id',
        method: 'patch',
        hooks: [
            jwtAuthenticate(),
            IsAdminOrModerator(),
            upload('avatar', avatarImagePath, 1)
        ],
        fn: (req, res) => {
            const { AuthUser, AccountProfile } = req.models;
            const id = req.params.id;
            let body = req.body;

            AccountProfile.findOne({
                where: { userId: id },
                include: [{ model: AuthUser, as: 'user' }]
            })
                .then(profile => {
                    if (
                        req.files &&
                        req.files.avatar &&
                        req.files.avatar.length > 0
                    ) {
                        body.avatar =
                            '/avatars/' + req.files.avatar[0].filename;
                    } else {
                        return profile;
                    }

                    return thumb({
                        prefix: '',
                        suffix: '',
                        width: 800,
                        source: req.files.avatar[0].path,
                        destination:
                            req.files.avatar[0].destination + '_thumbnail/'
                    }).then(file => {
                        body.avatarThumbnail =
                            '/avatars_thumbnail/' +
                            req.files.avatar[0].filename;
                        return profile;
                    });
                })
                .then(profile =>
                    profile
                        .update(body)
                        .then(() => profile.user.update(body))
                        .then(() => res.json(profile.serializer()))
                        .catch(err =>
                            res.badRequest({
                                err,
                                message: 'Can\'t update user details'
                            })
                        )
                );
        }
    },
    userPermissions: {
        path: '/v1/users/:id/permissions',
        methods: ['get', 'put'],
        hooks: [jwtAuthenticate(), IsAdminOrModerator()],
        fn: (req, res) => {
            const user = req.user;
            const editedUserId = req.params.id;
            const { AccountProfile, UserGuard, AuthUser, Page } = req.models;
            const editedPages = {
                read: req.body.read || [],
                write: req.body.write || [],
                moderate: req.body.moderate || []
            };

            const applyPermission = (permissions, profile, pages) => {
                return Promise.resolve(
                    permissions.map(perm =>
                        UserGuard.removeUserPermission(
                            profile.user,
                            perm['internal'],
                            UserGuard.Types.Page,
                            pages
                        ).then(() => {
                            if (editedPages[perm['action']].length > 0) {
                                return Page.findAll({
                                    where: {
                                        id: { $in: editedPages[perm['action']] }
                                    }
                                }).then(editedPages =>
                                    UserGuard.assignUserPermission(
                                        profile.user.get('id'),
                                        perm['internal'],
                                        UserGuard.Types.Page,
                                        editedPages
                                    )
                                );
                            }
                        })
                    )
                );
            };

            if (user.id == editedUserId) {
                return res.status(403).json({ message: 'Forbidden' });
            }

            if (req.method === 'PUT') {
                return editUserPermissionsCheck(editedUserId, user, req).then(
                    profile =>
                        userManagementPermissionsForPageIds(
                            user,
                            [
                                ...editedPages.read,
                                ...editedPages.write,
                                editedPages.moderate
                            ],
                            req
                        )
                            .then(permissions =>
                                AuthUser.getControlledPagesForUser(user)
                                    .then(pages =>
                                        applyPermission(
                                            permissions,
                                            profile,
                                            pages
                                        )
                                    )
                                    .then(() =>
                                        res.json({
                                            read: editedPages.read,
                                            write: editedPages.write,
                                            moderate: editedPages.moderate,
                                            id: profile.user.get('id')
                                        })
                                    )
                            )
                            .catch(err =>
                                res.status(403).json({
                                    err,
                                    message: 'Forbidden.'
                                })
                            )
                );
            }

            AccountProfile.findOne({
                where: { userId: editedUserId },
                include: [{ model: AuthUser, as: 'user' }]
            }).then(profile =>
                AuthUser.getControlledPagesForUser(user)
                    .then(pages =>
                        Promise.all([
                            UserGuard.getGuardObjectsForUser(
                                profile.user,
                                [UserGuard.Permissions.Page.view],
                                UserGuard.Types.Page,
                                pages
                            ),
                            UserGuard.getGuardObjectsForUser(
                                profile.user,
                                [UserGuard.Permissions.Page.change],
                                UserGuard.Types.Page,
                                pages
                            ),
                            UserGuard.getGuardObjectsForUser(
                                profile.user,
                                [UserGuard.Permissions.Page.moderate],
                                UserGuard.Types.Page,
                                pages
                            )
                        ]).then(result => {
                            const [read, write, moderate] = result;
                            res.json({
                                id: profile.user.get('id'),
                                read: _.map(read, r => r.id),
                                write: _.map(write, w => w.id),
                                moderate: _.map(moderate, m => m.id)
                            });
                        })
                    )
                    .catch(err =>
                        res.badRequest({
                            err,
                            message: 'User permissions bad request.'
                        })
                    )
            );
        }
    },
    bulkPutPermissions: {
        path: '/v1/users/permissions',
        method: 'put',
        hooks: [jwtAuthenticate(), IsAdminOrModerator()],
        fn: (req, res) => {
            const permissions = request.body;
            const user = req.user;
            let promises = [];
            permissions.forEach(permActions => {
                if (user.id === permActions.id) {
                    return res.status(403).json({ message: 'Forbidden.' });
                }
                promises.push(
                    editUserPermissionsCheck(permActions.id, user, req)
                        .then(profile =>
                            userManagementPermissionsForPageIds(
                                user,
                                [
                                    ...permActions.read,
                                    ...permActions.write,
                                    permActions.moderate
                                ],
                                req
                            )
                        )
                        .then(permissions =>
                            AuthUser.getControllerdPagesForUser(user)
                        )
                        .then(pages =>
                            applyProfilePermissions(
                                permissions,
                                pages,
                                profile,
                                permActions
                            )
                        )
                        .then(() =>
                            Promise.resolve({
                                read: permActions.read,
                                write: permActions.write,
                                moderate: permActions.moderate,
                                id: profile.user.get('id')
                            })
                        )
                );
            });

            Promise.all(promises)
                .then(result => res.json(result))
                .catch(err =>
                    res.status(403).json({ err, message: 'Forbidden.' })
                );
        }
    },
    approve: {
        path: '/v1/users/:id/approve',
        method: 'post',
        hooks: [jwtAuthenticate(), IsAdminOrModerator()],
        fn: (req, res) => {
            const user = req.user;
            const id = req.params.id;
            const { AccountProfile, AuthUser } = req.models;

            AccountProfile.find({
                where: { userId: id },
                include: [{ model: AuthUser, as: 'user' }]
            })
                .then(profile => {
                    if (profile.get('isApproved')) {
                        return res.badRequest({
                            message:
                                'You tried to approve already approved user.'
                        });
                    }

                    return profile
                        .approve(user, req)
                        .then(() => res.json(profile.serializer()));
                })
                .catch(err =>
                    res.badRequest({
                        err,
                        message: 'Approve bad request.'
                    })
                );
        }
    },
    reject: {
        path: '/v1/users/:id/reject',
        method: 'post',
        hooks: [jwtAuthenticate(), IsAdminOrModerator()],
        fn: (req, res) => {
            const user = req.user;
            const id = req.params.id;
            const { AccountProfile, AuthUser } = req.models;

            AccountProfile.find({
                where: { userId: id },
                include: [{ model: AuthUser, as: 'user' }]
            })
                .then(profile => {
                    if (!profile.get('isApproved')) {
                        return res.badRequest({
                            message:
                                'You tried to reject user that was previously rejected.'
                        });
                    }

                    return profile
                        .reject(user, req)
                        .then(() => res.json(profile.serializer()));
                })
                .catch(err =>
                    res.badRequest({ err, message: 'Reject bad request.' })
                );
        }
    },
    setPassword: {
        path: '/v1/users/:id/set-password',
        method: 'post',
        hooks: [jwtAuthenticate(), IsAdminOrModerator()],
        fn: (req, res) => {
            const user = req.user;
            const id = req.params.id;
            const { password } = req.body;
            const { AuthUser } = req.models;

            AuthUser.setPassword(id, password)
                .then(user =>
                    user.save().then(() =>
                        res.json({
                            result: user.get('id'),
                            message: 'Password was set successfully.'
                        })
                    )
                )
                .catch(err =>
                    res.notFound({ err, message: 'User not found.' })
                );
        }
    },
    roles: {
        path: '/v1/users/roles',
        method: 'get',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            return res.json(req.models.AccountProfile.Roles);
        }
    },
    invite: {
        path: '/v1/users/invite',
        method: 'post',
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            // @TODO - Implement method inviteUser(req)
        }
    },
    userModerators: {
        path: '/v1/users/:id/moderators',
        methods: ['get', 'put'],
        hooks: [jwtAuthenticate(), IsAdminOrModerator()],
        fn: (req, res) => {
            const userId = req.params.id;
            const { moderators } = req.body || {};
            const { AccountProfile, AuthUser } = req.model;

            AccountProfile.find({
                where: { userId },
                include: [{ model: AuthUser, as: 'user' }]
            })
                .then(profile => {
                    if (profile.isAdmin() || profile.isModerator()) {
                        return res.status(403).json({
                            message: 'Only users can have moderators.'
                        });
                    }
                    if (req.method === 'PUT' && moderators) {
                        profile.setModerators(moderators);
                        return res.json({ moderators });
                    }
                    return res
                        .status(403)
                        .json({ message: 'Only users can have moderators.' });
                })
                .catch(err =>
                    res.notFound({ err, message: 'User not found.' })
                );
        }
    },
    bulkUserModerators: {
        path: '/v1/users/bulk_moderators',
        methods: ['patch', 'put'],
        hooks: [jwtAuthenticate(), IsAdminOrModerator()],
        fn: (req, res) => {
            let moderators = req.body;
            const { AccountProfile } = req.model;
            let promises = [];

            if (!Array.isArray(moderators)) {
                return res.badRequest({
                    message: 'Bulk user moderators bad parameters.'
                });
            }

            promises = moderators.map(moderator => {
                return AccountProfile.find({ where: { userId: moderator.id } })
                    .then(profile => {
                        if (!profile.isAdmin() && !profile.isModerator()) {
                            profile.setModerators(moderator.moderators);
                            return Promise.resolve({
                                moderators: moderator.moderators
                            });
                        }
                        return Promise.reject(
                            'Only users can have moderators.'
                        );
                    })
                    .catch(err =>
                        Promise.reject({ err, message: 'User not found.' })
                    );
            });
            Promise.all(promises)
                .then(results => res.json({ results }))
                .catch(err => res.badRequest({ err }));
        }
    },
    managedUsers: {
        path: '/v1/users/:id/managed_users',
        methods: ['get', 'put'],
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const userId = req.params.id;
            const managedUsers = req.body ? req.body['managed_users'] : null;
            const { AccountProfile, AuthUser } = req.model;

            managedUsers(req, userId, managedUsers)
                .then(result => res.json(result))
                .catch(err =>
                    res.status(403).json({
                        err,
                        message: 'Managed users only exist on moderators.'
                    })
                );
        }
    },
    bulkManagedUsers: {
        path: '/v1/users/managed_users',
        methods: ['put', 'patch'],
        hooks: [jwtAuthenticate()],
        fn: (req, res) => {
            const data = req.body;
            const promises = [];

            if (!Array.isArray(data)) {
                return res.badRequest('Bulk managed users bad request.');
            }
            promises = data.map(userData =>
                managedUsers(
                    req,
                    userData.id,
                    userData.managedUsers || userData['managed_users']
                )
            );
            Promise.all(promises)
                .then(() => res.json(data))
                .catch(err =>
                    res.status(403).json({
                        err,
                        message: 'Managed users only exist on moderators.'
                    })
                );
        }
    }
};
