const { ServiceProvider, IdentityProvider } = require('samlify');
const { jwtAuthenticate } = require('../libs/auth');
const { genRndToken } = require('../libs/crypto');
const { signPayload } = require('../libs/jwt');
const { logger } = require('debug')('SSO');
const {
    loadSettings,
    genRedirectUrl,
    getSamlNameId,
    getSessionIndex,
    getUserData,
    getQueryData,
    getBodyData
} = require('../libs/sso');

module.exports = {
    index: {
        path: '/v1/auth/sso',
        method: 'all',
        fn: (req, res) => {
            const post_data = getBodyData(req);
            const get_data = getQueryData(req);
            const settings = loadSettings(req);
            const sp = new ServiceProvider(settings.sp);
            const idp = new IdentityProvider(settings.idp);
            const email = getSamlNameId(req, post_data, get_data);
            const { AuthUser, AccountProfile } = req.models;

            switch (get_data) {
                case 'sso':
                    return res.redirect(sp.createLoginRequest(idp, 'redirect'));
                    break;
                case 'slo':
                    return sp.parseLogoutRequest(
                        idp,
                        'redirect',
                        req,
                        reqInfo =>
                            sp.sendLogoutResponse(
                                idp,
                                reqInfo,
                                'redirect',
                                null,
                                url => res.redirect(url)
                            )
                    );
                    break;
                case 'acs':
                    return sp
                        .parseLoginResponse(idp, 'post', req)
                        .then(parseResult => {
                            logger('parseLoginResponse', parseResult);
                            req.session['samlUserdata'] = parseResult;
                            if (post_data && post_data.RelayState) {
                                res.redirect(post_data.RelayState);
                            }
                        })
                        .catch(console.error);
                    break;
                case 'sls':
                    return req.session.destroy(err => {
                        if (err) {
                            throw Error(err);
                        }
                    });
                    break;
                default:
                    break;
            }

            if (!email) {
                return res.badRequest({
                    message: 'Unable to create user without provided nameId.'
                });
            }

            const userData = getUserData(req);

            AuthUser.findOne({ where: { email } })
                .then(user =>
                    AccountProfile.findOne({
                        where: { userId: user.get('id') },
                        include: [{ model: AuthUser, as: 'user' }]
                    }).then(profile => {
                        if (!profile) {
                            return AccountProfile.createProfile(
                                email,
                                userData,
                                req
                            ).then(userProfile =>
                                genRedirectUrl(userProfile, res)
                            );
                        }
                        return profile
                            .update({
                                token: genRndToken(),
                                tokenCreatedAt: Date.now()
                            })
                            .then(() => profile.user.update(userData))
                            .then(() => profile.serializer())
                            .then(userProfile =>
                                genRedirectUrl(userProfile, res)
                            );
                    })
                )
                .catch(err =>
                    AccountProfile.createProfile(email, userData, req).then(
                        userProfile => genRedirectUrl(userProfile, res)
                    )
                );
        }
    },
    metadata: {
        path: '/v1/auth/sso/metadata',
        method: 'get',
        fn: (req, res) => {
            const sp = new ServiceProvider(loadSettings(req).sp);
            res
                .header('Content-Type', 'application/xml')
                .send(sp.getMetadata());
        }
    },
    login: {
        path: '/v1/auth/sso/login',
        method: 'post',
        fn: (req, res) => {
            const { token } = req.body;
            const { AccountProfile, AuthUser } = req.models;

            AccountProfile.findOne({
                where: { token },
                include: [{ model: AuthUser, as: 'user' }]
            })
                .then(profile =>
                    profile.update({ token: null, tokenCreatedAt: null })
                )
                .then(() => {
                    if (
                        !profile.user.get('isActive') ||
                        !profile.get('isApproved')
                    ) {
                        return res.unauthorized({
                            message: 'User is not active or not approved.'
                        });
                    }
                    return res.json({
                        token: signPayload(profile.user.userSerializer())
                    });
                })
                .catch(err =>
                    res.badRequest({ err, message: 'Can\'t find user profile.' })
                );
        }
    },
    completeProfile: {
        path: '/v1/auth/sso/complete-profile',
        method: 'post',
        fn: (req, res) => {
            const data = req.body;
            const { AccountProfile, AuthUser } = req.models;

            AccountProfile.findOne({
                where: { token: data.token },
                include: [{ model: AuthUser, as: 'user' }]
            })
                .then(profile =>
                    profile.update(
                        Object.assign({}, data, {
                            ssoLoginCompleted: true,
                            isApproved: true,
                            token: null,
                            tokenCreatedAt: null
                        })
                    )
                )
                .then(() =>
                    profile.user.update(
                        Object.assign({}, data, { isActive: true })
                    )
                )
                .then(() =>
                    res.json({
                        token: signPayload(profile.user.userSerializer())
                    })
                )
                .catch(err =>
                    res.badRequest({ err, message: 'Can\'t find user profile.' })
                );
        }
    }
};
