const ExtractJwt = require('passport-jwt').ExtractJwt;
const config = require('clout-js').config;

module.exports = {
    passport: {
        directory: '/strategies',
        serializeUser: 'models.AuthUser.serializeUser',
        deserializeUser: 'models.AuthUser.deserializeUser',
        jwt: {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            // @TODO - get jwtsecret from one source.
            // @TODO config - undefined it heppened in conf/* files need check it
            secretOrKey: process.env.JWT_SECRET_KEY || config.jwt.secret
        },
        useDefault: true,
        useDefault: ['jwt'],
    }
};
