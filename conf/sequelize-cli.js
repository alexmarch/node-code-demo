const {
    MYSQL_DATABASE,
    MYSQL_USER,
    MYSQL_PASSWORD,
    MYSQL_HOST,
    LOGGING_SEQUELIZE
} = process.env;
const { resolve } = require('path');

module.exports = {
    development: {
        username: MYSQL_USER,
        password: MYSQL_PASSWORD,
        database: MYSQL_DATABASE,
        host: MYSQL_HOST || 'mysql',
        dialect: 'mysql'
    },
    test: {
        dialect: 'sqlite',
        storage: resolve(__dirname, '../data/test-db.sqlite3')
    },
    production: {
        username: MYSQL_USER,
        password: MYSQL_PASSWORD,
        database: MYSQL_DATABASE,
        host: MYSQL_HOST || 'mysql',
        dialect: 'mysql'
    }
};
