/**
 * Default configuration
 */

module.exports = {
    session: {
        secret: process.env.SESSION_SECRET
    },
    jwt: {
        secret: process.env.JWT_SECRET_KEY,
        algorithm: "HS256"
    },
    imagesPath: "pages/cover_images",
    imagesThumbPath: "pages/cover_images_thumbnail",
    avatarImagePath: "avatars",
    componentFileImagePath: "components/files",
    componentFileThumbPath: "components/file_thumbnail",
    componentFontFileImagePath: "components/fonts",
    componentFileLinksImagePath: "components/links",
    pageLimit: 50,
    pageOffset: 0,
    frontUrl: process.env.FRONTEND_URL,
    samlFolder: process.env.SAML_FOLDER,
    companyName: process.env.COMPANY_NAME,
    platformTitle: process.env.PLATFORM_TITLE,
    frontEndUrl: process.env.FRONTEND_URL,
    defaultFromEmail: process.env.DEFAULT_FROM_EMAIL,
    COMPONENTS_DATA_MAX_BYTES: 1 * 1024 * 1024, // 1 MiB
    COMPONENT_TYPES: ["links", "imagegrid"],
    API_SPHINX_HOST: process.env.API_SPHINX_HOST,
    API_SPHINX_PORT: process.env.API_SPHINX_PORT
};
