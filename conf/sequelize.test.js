const {
    MYSQL_DATABASE,
    MYSQL_USER,
    MYSQL_PASSWORD,
    MYSQL_HOST,
    LOGGING_SEQUELIZE,
    NODE_ENV
} = process.env;

module.exports = {
    sequelize: {
        database: MYSQL_DATABASE,
        username: MYSQL_USER,
        password: MYSQL_PASSWORD,
        connection: {
            dialect: 'sqlite',
            storage: require('path').resolve(__dirname, '../data/test-db.sqlite3'),
            logging: false
        },
        sync: false
    }
};
