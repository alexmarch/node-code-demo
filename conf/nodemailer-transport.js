let transport = {};

if (process.env.NODE_ENV === 'test') {
    transport = {
        service: 'gmail',
        host: 'smtp.gmail.com',
        auth: {
            user: process.env.GMAIL_USER,
            pass: process.env.GMAIL_PASS
        }
    };
}

module.exports = { transport }
