const {
    MYSQL_DATABASE,
    MYSQL_USER,
    MYSQL_PASSWORD,
    MYSQL_HOST,
    LOGGING_SEQUELIZE,
    NODE_ENV
} = process.env;

module.exports = {
    sequelize: {
        database: MYSQL_DATABASE,
        username: MYSQL_USER,
        password: MYSQL_PASSWORD,
        connection: {
            host : MYSQL_HOST,
            dialect: 'mysql',
            dialectOptions: {
                multipleStatements: true
            },
            logging: false
        },
        sync: false
    }
};
