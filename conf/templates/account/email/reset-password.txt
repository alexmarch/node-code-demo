Hi {{ first_name }},

We received a request to change your password on the {{ platform_title }}.

Click the link below to set a new password:
{{ set_password_link }}

If you don't want to change your password, ignore this email.

Regards,
{{ company_name }}
