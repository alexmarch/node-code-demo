const testLib = require('clout-js/test/lib');
const CURRENT_APPLICATION_PATH = require('path').join(__dirname, '../');

before(function (done) {
    this.timeout(0);
    process.env.PORT = 8420;
    process.env.NODE_ENV = 'test';
    let clout = testLib.createInstance(CURRENT_APPLICATION_PATH);
    clout.on('started', () => process.nextTick(() => done()));
});

after(function () {
    this.timeout(0);
    return testLib.cloutInstance.stop();
});
