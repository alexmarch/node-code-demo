const PNGImage = require("pngjs-image");
const os = require("os");
const path = require("path");
const fs = require("fs");

module.exports = {
    getTmpImage: fileName => {
        const width = 640;
        const height = 480;
        const image = PNGImage.createImage(width, height);
        const filePath = path.resolve(`${os.tmpdir()}`, fileName);

        image.setAt(width / 2, height / 2, {
            red: 255,
            green: 0,
            blue: 0,
            alpha: 100
        });

        return new Promise((resolve, reject) => {
            image.writeImage(filePath, err => {
                if (err) reject(err);
                resolve({
                    data: fs.readFileSync(filePath),
                    name: fileName,
                    filePath
                });
            });
        });
    },
    getTmpFont: fileName => {
        const filePath = path.resolve(`${os.tmpdir()}`, fileName);
        fs.writeFileSync(
            filePath,
            Buffer.from([0001, 0000, 0013, 0100, 0004, 0030, 4453, 4947])
        );
        return filePath;
    },
    removeFile: filePath => fs.unlinkSync(filePath),
    createPage: (clout, ownerId, data = { isPublished: false }) => {
        const { Page, PageVersion } = clout.models;

        return Page.create(data).then(page => {
            return PageVersion.create({
                pageId: page.get("id"),
                ownerId,
                version: data.version ? data.version : 1
            }).then(pageVer => {
                return page
                    .update({ activeVersionId: pageVer.get("id") })
                    .then(() => Promise.resolve(page));
            });
        });
    }
};
