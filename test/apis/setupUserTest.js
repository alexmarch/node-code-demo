const jwt = require('jsonwebtoken');
const { JWT_EXPIRES_IN } = process.env;

const ADMIN_EMAIL = 'john.doe@test.com';
const MODERATOR_EMAIL = 'jane.doe@test.com';
const USER_EMAIL = 'john.roe@test.com';
const OTHER_USER_EMAIL = 'bob.roe@test.com';
const USER_PASSWORD = 'test1234';

const setupUserTest = (clout) => {
    const { Organisation, AuthUser, AccountProfile, } = clout.models;
    const secret = clout.config.jwt.secret;

    return new Promise((resolve, reject) => {
            Organisation.create({ name: 'Test Company' })
                .then(organisation => { clout.organisation = organisation })
                .then(() => AuthUser.create({
                    username: 'john.doe',
                    email: ADMIN_EMAIL,
                    password: USER_PASSWORD,
                    isSuperUser: true,
                    dateJoined: Date.now(),
                    firstName: 'John',
                    lastName: 'Doe',
                    isStaff: false,
                    isActive: true,
                }))
                .then(adminUser => { clout.adminUser = adminUser })
                .then(() => AccountProfile.create({
                    userId: clout.adminUser.get('id'),
                    title: 'Developer',
                    organisationId: clout.organisation.get('id'),
                    role: AccountProfile.Roles.admin
                }))
                .then(adminProfile => { clout.adminProfile = adminProfile })
                .then(() => Organisation.create({ name: 'Super Company' })
                .then(superCompany => { clout.superCompany = superCompany }))
                .then(() => AuthUser.create({
                    username: 'jane.doe',
                    email: MODERATOR_EMAIL,
                    password: USER_PASSWORD,
                    isSuperUser: false,
                    dateJoined: Date.now(),
                    firstName: 'Jane',
                    lastName: 'Doe',
                    isStaff: false,
                    isActive: true
                })
                .then(moderatorUser => { clout.moderatorUser = moderatorUser }))
                .then(() => AccountProfile.create({
                    userId: clout.moderatorUser.get('id'),
                    title: 'Manager',
                    organisationId: clout.superCompany.get('id'),
                    role: AccountProfile.Roles.moderator
                })
                .then(moderatorProfile => { clout.moderatorProfile = moderatorProfile }))
                .then(() => AuthUser.create({
                    username: 'john.roe',
                    email: USER_EMAIL,
                    password: USER_PASSWORD,
                    isSuperUser: false,
                    dateJoined: Date.now(),
                    firstName: 'John',
                    lastName: 'Roe',
                    isStaff: false,
                    isActive: true
                })
                .then(user => { clout.user = user; }))
                .then(() => AccountProfile.create({
                     userId: clout.user.get('id'),
                     title: 'Developer',
                     organisationId: clout.superCompany.get('id'),
                     role: AccountProfile.Roles.user,
                     isApproved: false
                })
                .then(accountProfile2 => { clout.accountProfile2 = accountProfile2 }))
                .then(() => AuthUser.create({
                    username: 'bob.roe',
                    email: OTHER_USER_EMAIL,
                    password: USER_PASSWORD,
                    isSuperUser: false,
                    dateJoined: Date.now(),
                    firstName: 'Bob',
                    lastName: 'Roe',
                    isStaff: false,
                    isActive: true
                })
                .then(user => { clout.otherUser = user; }))
                .then(() => AccountProfile.create({
                    userId: clout.otherUser.get('id'),
                    title: 'Developer',
                    organisationId: clout.superCompany.get('id'),
                    role: AccountProfile.Roles.user,
                    isApproved: false
                }))
                .then(otherUserProfile => { clout.otherUserProfile = otherUserProfile })
                .then(() => {
                    clout.moderatorProfile.addModerator(clout.moderatorUser.get('id'));
                    clout.moderatorAuthToken = jwt.sign(clout.moderatorUser.get({ plain: true }), secret);
                    clout.adminAuthToken = jwt.sign(clout.adminUser.get({ plain: true }), secret);
                    clout.userAuthToken = jwt.sign(clout.user.get({ plain: true }), secret);
                    resolve(clout);
                });
    });
};

const destroyUserTest = (clout) => {
        return Promise.resolve()
        .then(() => clout.adminProfile.destroy())
        .then(() => clout.moderatorProfile.destroy())
        .then(() => clout.accountProfile2.destroy())
        .then(() => clout.otherUserProfile.destroy())
        .then(() => clout.organisation.destroy())
        .then(() => clout.superCompany.destroy())
        .then(() => clout.moderatorUser.destroy())
        .then(() => clout.adminUser.destroy())
        .then(() => clout.user.destroy())
        .then(() => clout.otherUser.destroy());
};

const destroyEachUsersGetTest = (clout) => {
    const { Page, UserGuard } = clout.models;
    return Promise.resolve()
        .then(() => Page.destroy({truncate: true}))
        .then(() => UserGuard.destroy({truncate: true}));
};

module.exports = {
    setupUserTest,
    destroyUserTest,
    destroyEachUsersGetTest
};
