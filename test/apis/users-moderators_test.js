const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const { setupUserTest, destroyUserTest } = require('./setupUserTest');

const should = chai.should();

chai.use(chaiHttp);

describe('User moderator API', function () {
    let clout;

    const request = () => chai.request(clout.server['http']);

    before(() => clout = testLib.cloutInstance);

    before(done => setupUserTest(clout).then(() => done()));

    after(done => destroyUserTest(clout).then(() => done()));

});
