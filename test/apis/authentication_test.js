const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();

chai.use(chaiHttp);

const user = {
    username: 'john.doe12345',
    email: 'john.doe12345@test.com',
    password: 'test12345',
    isSuperUser: false,
    dateJoined: Date.now(),
    firstName: 'John',
    lastName: 'Doe',
    isStaff: false,
    isActive: true
};

describe('Authentication API', function () {
    const request = () => chai.request(clout.server['http']);
    let clout;
    let userModel;

    before(() => clout = testLib.cloutInstance);

    before(function (done) {
        let AuthUser = clout.models.AuthUser;

        AuthUser.findOne({username: user.username})
            .then((userInstance) => {
                if (userInstance) {
                    return userInstance;
                }

                return AuthUser.create(user);
            })
            .then(_user => {
                userModel = _user;
                done();
            });
    });

    describe('POST /api/v1/auth/login', () => {
        it('it should response with status 200 OK', done => {
            request()
                .post('/api/v1/auth/login')
                .send({
                    email: 'john.doe12345@test.com',
                    password: 'test12345'
                })
                .end((err, res) => {
                    res.should.have.property('status', 200);
                    done();
                });
        });
        it('it should login with camelcased email address', done => {
            request()
                .post('/api/v1/auth/login')
                .send({
                    email: 'john.doe12345@test.com',
                    password: 'test12345'
                })
                .end((err, res) => {
                    res.should.have.property('status', 200);
                    done();
                });
        });
        it('it should response object with token and refreshToken', done => {
            request()
                .post('/api/v1/auth/login')
                .send({
                    email: 'john.doe12345@test.com',
                    password: 'test12345'
                })
                .end((err, res) => {
                    res.body.should.have.property('token');
                    res.body.should.have.property('refreshToken');
                    done();
                });
        });
        it('it should verify token', done => {
            request()
                .post('/api/v1/auth/login')
                .send({
                    email: 'john.doe12345@test.com',
                    password: 'test12345'
                })
                .end((err, res) => {
                    const token = res.body.token;
                    request()
                        .post('/api/v1/auth/verify-token')
                        .set('authorization', `Bearer ${token}`)
                        .send({
                            token: token
                        })
                        .end((err, res) => {
                            should.not.exist(err);
                            res.status.should.eql(200);
                            res.type.should.eql('application/json');
                            should.exist(res.body.id);
                            done();
                        });
                });
        });
        it('it should response 400 not exist user with email', done => {
            request()
                .post('/api/v1/auth/login')
                .send({
                    email: 'not_exist_user@test.com',
                    password: 'test12345'
                })
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(400);
                    res.type.should.eql('application/json');
                    done();
                });
        });
        it('it should response 400 wrong password', done => {
            request()
                .post('/api/v1/auth/login')
                .send({
                    email: 'john.doe12345@test.com',
                    password: 'wrong_password'
                })
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(400);
                    res.type.should.eql('application/json');
                    done();
                });
        });
    });

    describe('POST /api/v1/auth/unlock-all-pages', () => {
        it.skip('it should unlock all pages on logout', done => {
            // @TODO: Need add test for logout here
            done();
        });
    });


});
