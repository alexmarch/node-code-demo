const jwt = require("jsonwebtoken");
const chai = require("chai");
const chaiHttp = require("chai-http");
const testLib = require("clout-js/test/lib");
const should = chai.should();
const { createPage, getTmpImage } = require("../testLib");

chai.use(chaiHttp);

describe("Components API", function() {
    let clout,
        userUser,
        user2Model,
        profileModel,
        profile2Model,
        orgModel,
        token,
        tokenUser,
        pageVerModel;

    const request = () => chai.request(clout.server["http"]);

    before(() => (clout = testLib.cloutInstance));

    before(function(done) {
        const { Page, AuthUser, Organisation, AccountProfile } = clout.models;
        const secret = clout.config.jwt.secret;

        this.timeout(3000);

        AuthUser.create({
            username: "john.doe",
            email: "john.doe@test.com",
            password: "test1234",
            isSuperUser: false,
            dateJoined: Date.now(),
            firstName: "John",
            lastName: "Doe",
            isStaff: false,
            isActive: true
        }).then(user =>
            Organisation.create({ name: "Test organisation" }).then(
                organisation =>
                    Page.create({
                        userId: user.get("id"),
                        title: "Developer",
                        organisationId: organisation.get("id")
                    }).then(() =>
                        AuthUser.create({
                            username: "john.doe2",
                            email: "john.doe2@test.com",
                            password: "test12345",
                            isSuperUser: false,
                            dateJoined: Date.now(),
                            firstName: "John",
                            lastName: "Doe 2",
                            isStaff: false,
                            isActive: true
                        }).then(user2 =>
                            AccountProfile.create({
                                userId: user.get("id"),
                                title: "Developer",
                                organisationId: organisation.get("id"),
                                role: AccountProfile.Roles.admin
                            })
                                .then(profile => {
                                    profileModel = profile;

                                    return AccountProfile.create({
                                        userId: user2.get("id"),
                                        title: "Test user",
                                        organisationId: organisation.get("id"),
                                        role: AccountProfile.Roles.user
                                    });
                                })
                                .then(profile2 => {
                                    userModel = user;
                                    user2Model = user2;
                                    profile2Model = profile2;
                                    orgModel = organisation;
                                    token = jwt.sign(
                                        userModel.get({ plain: true }),
                                        secret
                                    );
                                    tokenUser = jwt.sign(
                                        user2Model.get({ plain: true }),
                                        secret
                                    );
                                    createPage(clout, userModel.get("id")).then(
                                        pageVer => {
                                            pageVerModel = pageVer;
                                            done();
                                        }
                                    );
                                })
                        )
                    )
            )
        );
    });

    after(done => {
        Promise.all([
            profileModel.destroy().then(() => userModel.destroy()),
            profile2Model
                .destroy()
                .then(() => user2Model.destroy())
                .then(() => orgModel.destroy())
        ]).then(() => done());
    });

    describe("GET /v1/pages/{pageId}/components", () => {
        it("should return bad request for none authorized user", function(done) {
            const { Component } = clout.models;

            this.timeout(2000);

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp => {
                request()
                    .get(`/api/v1/pages/${pageVerModel.get("id")}/components`)
                    .end((err, res) => {
                        should.exist(err);
                        res.status.should.eql(401);
                        done();
                    });
            });
        });

        it("should get components list for user", done => {
            const { Component } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp => {
                request()
                    .get(`/api/v1/pages/${pageVerModel.get("id")}/components`)
                    .set("Authorization", `Bearer ${token}`)
                    .end((err, res) => {
                        should.not.exist(err);
                        res.status.should.eql(200);
                        res.body.should.not.be.null;
                        res.body.should.have.property("results");

                        createPage(clout, userModel.get("id")).then(pageVer => {
                            Component.create({
                                pageVersion: pageVer.get("activeVersionId"),
                                typeId: "HEADER_H2"
                            }).then(comp => {
                                request()
                                    .get(
                                        `/api/v1/pages/${pageVer.get(
                                            "id"
                                        )}/components`
                                    )
                                    .set("Authorization", `Bearer ${token}`)
                                    .end((err, res) => {
                                        should.not.exist(err);
                                        res.status.should.eql(200);
                                        res.body.should.not.be.null;
                                        res.body.should.have.property(
                                            "results"
                                        );
                                        res.body.results.should.not.empty;
                                        res.body.should.have.property(
                                            "numberOfPages"
                                        );
                                        res.body.numberOfPages.should.not.be
                                            .null;
                                        done();
                                    });
                            });
                        });
                    });
            });
        });

        it("should retrieve single component", done => {
            const { Component } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp1 => {
                return createPage(clout, userModel.get("id"))
                    .then(pageVer1 =>
                        Component.create({
                            pageVersion: pageVer1.get("activeVersionId"),
                            typeId: "HEADER_H2"
                        })
                    )
                    .then(comp2 =>
                        createPage(clout, userModel.get("id"), {
                            isPublished: false,
                            version: 3
                        })
                    )
                    .then(pageVer3 => {
                        return Component.create({
                            pageVersion: pageVer3.get("activeVersionId"),
                            typeId: "HEADER_H3"
                        }).then(comp3 => {
                            request()
                                .get(
                                    `/api/v1/pages/${pageVer3.get(
                                        "id"
                                    )}/components/${comp3.get("uuid")}`
                                )
                                .set("Authorization", `Bearer ${token}`)
                                .end((err, res) => {
                                    should.not.exist(err);
                                    res.status.should.eql(200);
                                    res.body.should.not.be.null;
                                    res.body.should.have.property("typeId");
                                    res.body.typeId.should.eql("HEADER_H3");
                                    done();
                                });
                        });
                    });
            });
        });

        it("should retrieve single component with version = 3", done => {
            const { Component } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp1 => {
                return createPage(clout, userModel.get("id"))
                    .then(pageVer1 =>
                        Component.create({
                            pageVersion: pageVer1.get("activeVersionId"),
                            typeId: "HEADER_H2"
                        })
                    )
                    .then(comp2 =>
                        createPage(clout, userModel.get("id"), {
                            isPublished: false,
                            version: 3
                        })
                    )
                    .then(pageVer3 => {
                        return Component.create({
                            pageVersion: pageVer3.get("activeVersionId"),
                            typeId: "HEADER_H3"
                        }).then(comp3 => {
                            request()
                                .get(
                                    `/api/v1/pages/${pageVer3.get(
                                        "id"
                                    )}/components/${comp3.get(
                                        "uuid"
                                    )}/?version=3`
                                )
                                .set("Authorization", `Bearer ${token}`)
                                .end((err, res) => {
                                    should.not.exist(err);
                                    res.status.should.eql(200);
                                    res.body.should.not.be.null;
                                    res.body.should.have.property(
                                        "page_version"
                                    );
                                    res.body.typeId.should.eql("HEADER_H3");
                                    done();
                                });
                        });
                    });
            });
        });

        it("should requires lock when creating component", done => {
            const data = {
                page_version: null, // should be set to latest version by API
                type_id: "H1_HEADER",
                data: '{"rand":true}',
                position: null
            };

            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/components`)
                .set("Authorization", `Bearer ${token}`)
                .send(data)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(400);
                    done();
                });
        });

        it("should create component", done => {
            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                .set("Authorization", `Bearer ${token}`)
                .send()
                .end(() => {
                    const data = {
                        page_version: null, // should be set to latest version by API
                        type_id: "H1_HEADER",
                        data: '{"rand":true}',
                        position: null
                    };
                    request()
                        .post(
                            `/api/v1/pages/${pageVerModel.get("id")}/components`
                        )
                        .set("Authorization", `Bearer ${token}`)
                        .send(data)
                        .end((err, res) => {
                            should.not.exist(err);
                            res.status.should.eql(200);
                            res.body.should.not.be.null;
                            res.body.should.have.property("typeId");
                            res.body.typeId.should.eql("H1_HEADER");
                            res.body.should.have.property("position");
                            res.body.position.should.eql(0);
                            done();
                        });
                });
        });

        it("should create component with position", done => {
            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                .set("Authorization", `Bearer ${token}`)
                .send()
                .end(() => {
                    const data = {
                        page_version: null, // should be set to latest version by API
                        type_id: "H1_HEADER",
                        data: '{"rand":true}',
                        position: 2
                    };
                    request()
                        .post(
                            `/api/v1/pages/${pageVerModel.get("id")}/components`
                        )
                        .set("Authorization", `Bearer ${token}`)
                        .send(data)
                        .end((err, res) => {
                            should.not.exist(err);
                            res.status.should.eql(200);
                            res.body.should.not.be.null;
                            res.body.should.have.property("typeId");
                            res.body.typeId.should.eql("H1_HEADER");
                            res.body.should.have.property("position");
                            res.body.position.should.eql(2);
                            done();
                        });
                });
        });

        it("should create component with specific page version", done => {
            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                .set("Authorization", `Bearer ${token}`)
                .send()
                .end(() => {
                    const data = {
                        page_version: 1, // should be set to latest version by API
                        type_id: "H1_HEADER",
                        data: '{"rand":true}',
                        position: 2
                    };
                    request()
                        .post(
                            `/api/v1/pages/${pageVerModel.get("id")}/components`
                        )
                        .set("Authorization", `Bearer ${token}`)
                        .send(data)
                        .end((err, res) => {
                            should.not.exist(err);
                            res.status.should.eql(200);
                            res.body.should.not.be.null;
                            res.body.should.have.property("typeId");
                            res.body.typeId.should.eql("H1_HEADER");
                            res.body.should.have.property("page_version");
                            res.body.position.should.eql(2);
                            done();
                        });
                });
        });

        it("should update component page version", done => {
            const { Component } = clout.models;

            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                .set("Authorization", `Bearer ${token}`)
                .send()
                .end(() => {
                    const data = {
                        page_version: 5, // should be set to latest version by API
                        type_id: "H1_HEADER",
                        data: '{"rand":true}',
                        position: 2
                    };
                    return Component.create({
                        pageVersion: pageVerModel.get("activeVersionId"),
                        typeId: "HEADER_H3"
                    }).then(comp => {
                        request()
                            .patch(
                                `/api/v1/pages/${pageVerModel.get(
                                    "id"
                                )}/components/${comp.get("uuid")}`
                            )
                            .set("Authorization", `Bearer ${token}`)
                            .send(data)
                            .end((err, res) => {
                                should.not.exist(err);
                                res.status.should.eql(200);
                                done();
                            });
                    });
                });
        });

        it("should delete component", done => {
            const { Component } = clout.models;

            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                .set("Authorization", `Bearer ${token}`)
                .send()
                .end(() => {
                    return Component.create({
                        pageVersion: pageVerModel.get("activeVersionId"),
                        typeId: "HEADER_H3"
                    }).then(comp => {
                        request()
                            .delete(
                                `/api/v1/pages/${pageVerModel.get(
                                    "id"
                                )}/components/${comp.get("uuid")}`
                            )
                            .set("Authorization", `Bearer ${token}`)
                            .end((err, res) => {
                                should.not.exist(err);
                                res.status.should.eql(200);
                                done();
                            });
                    });
                });
        });

        it("should duplicate component", done => {
            const { Component } = clout.models;

            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                .set("Authorization", `Bearer ${token}`)
                .send()
                .end(() => {
                    return Component.create({
                        pageVersion: pageVerModel.get("activeVersionId"),
                        typeId: "HEADER_H3"
                    }).then(comp => {
                        request()
                            .post(
                                `/api/v1/pages/${pageVerModel.get(
                                    "id"
                                )}/components/${comp.get("uuid")}/duplicate`
                            )
                            .set("Authorization", `Bearer ${token}`)
                            .end((err, res) => {
                                should.not.exist(err);
                                res.status.should.eql(200);
                                done();
                            });
                    });
                });
        });

        it("should set meta data with valid json", done => {
            const { Component } = clout.models;

            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                .set("Authorization", `Bearer ${token}`)
                .send()
                .end(() => {
                    const data = {
                        data: '{"rand": "nope"}'
                    };
                    return Component.create({
                        pageVersion: pageVerModel.get("activeVersionId"),
                        typeId: "HEADER_H3"
                    }).then(comp => {
                        request()
                            .patch(
                                `/api/v1/pages/${pageVerModel.get(
                                    "id"
                                )}/components/${comp.get("uuid")}`
                            )
                            .set("Authorization", `Bearer ${token}`)
                            .send(data)
                            .end((err, res) => {
                                should.not.exist(err);
                                res.status.should.eql(200);
                                done();
                            });
                    });
                });
        });

        it("should set image caption", done => {
            const { Component, ComponentFile } = clout.models;

            request()
                .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                .set("Authorization", `Bearer ${token}`)
                .send()
                .end(() => {
                    const data = {
                        description: "No description"
                    };
                    return Component.create({
                        pageVersion: pageVerModel.get("activeVersionId"),
                        typeId: "HEADER_H3"
                    }).then(comp => {
                        getTmpImage("image_caption.png").then(
                            ({ filePath }) => {
                                ComponentFile.create({
                                    componentId: comp.get("id"),
                                    data: filePath,
                                    title: "Different Image"
                                }).then(compFile => {
                                    request()
                                        .patch(
                                            `/api/v1/pages/${pageVerModel.get(
                                                "id"
                                            )}/components/${comp.get(
                                                "uuid"
                                            )}/files/${compFile.get("uuid")}`
                                        )
                                        .set("Authorization", `Bearer ${token}`)
                                        .field({
                                            description: data.description
                                        })
                                        .end((err, res) => {
                                            should.not.exist(err);
                                            res.status.should.eql(200);
                                            done();
                                        });
                                });
                            }
                        );
                    });
                });
        });
    });
});
