const jwt = require("jsonwebtoken");
const chai = require("chai");
const chaiHttp = require("chai-http");
const testLib = require("clout-js/test/lib");
const should = chai.should();
const { createPage, getTmpImage } = require("../testLib");
const fs = require("fs");

chai.use(chaiHttp);

describe("Components File API", function() {
    let clout,
        userUser,
        user2Model,
        profileModel,
        profile2Model,
        orgModel,
        token,
        tokenUser,
        pageVerModel;

    const request = () => chai.request(clout.server["http"]);

    before(() => (clout = testLib.cloutInstance));

    before(done => {
        const { Page, AuthUser, Organisation, AccountProfile } = clout.models;
        const secret = clout.config.jwt.secret;

        AuthUser.create({
            username: "john.doe",
            email: "john.doe@test.com",
            password: "test1234",
            isSuperUser: false,
            dateJoined: Date.now(),
            firstName: "John",
            lastName: "Doe",
            isStaff: false,
            isActive: true
        }).then(user =>
            Organisation.create({ name: "Test organisation" }).then(
                organisation =>
                    Page.create({
                        userId: user.get("id"),
                        title: "Developer",
                        organisationId: organisation.get("id")
                    }).then(() =>
                        AuthUser.create({
                            username: "john.doe2",
                            email: "john.doe2@test.com",
                            password: "test12345",
                            isSuperUser: false,
                            dateJoined: Date.now(),
                            firstName: "John",
                            lastName: "Doe 2",
                            isStaff: false,
                            isActive: true
                        }).then(user2 =>
                            AccountProfile.create({
                                userId: user.get("id"),
                                title: "Developer",
                                organisationId: organisation.get("id"),
                                role: AccountProfile.Roles.admin
                            })
                                .then(profile => {
                                    profileModel = profile;

                                    return AccountProfile.create({
                                        userId: user2.get("id"),
                                        title: "Test user",
                                        organisationId: organisation.get("id"),
                                        role: AccountProfile.Roles.user
                                    });
                                })
                                .then(profile2 => {
                                    userModel = user;
                                    user2Model = user2;
                                    profile2Model = profile2;
                                    orgModel = organisation;
                                    token = jwt.sign(
                                        userModel.get({ plain: true }),
                                        secret
                                    );
                                    tokenUser = jwt.sign(
                                        user2Model.get({ plain: true }),
                                        secret
                                    );
                                    createPage(clout, userModel.get("id")).then(
                                        pageVer => {
                                            pageVerModel = pageVer;
                                            done();
                                        }
                                    );
                                })
                        )
                    )
            )
        );
    });

    after(done => {
        Promise.all([
            profileModel.destroy().then(() => userModel.destroy()),
            profile2Model
                .destroy()
                .then(() => user2Model.destroy())
                .then(() => orgModel.destroy())
        ]).then(() => done());
    });

    describe("GET /v1/pages/{pageId}/components/{componentUUID}/files", () => {
        it("should not retrieve component files for unauthenticated user", function(done) {
            const { Component } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp => {
                request()
                    .get(
                        `/api/v1/pages/${pageVerModel.get(
                            "id"
                        )}/components/${comp.get("uuid")}/files`
                    )
                    .end((err, res) => {
                        should.exist(err);
                        res.status.should.eql(401);
                        done();
                    });
            });
        });

        it("should retrieve all component files", function(done) {
            const { Component, ComponentFile } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp => {
                getTmpImage("component_file.png").then(({ filePath }) => {
                    ComponentFile.create({
                        componentId: comp.get("id"),
                        data: filePath,
                        title: "Component file"
                    }).then(cf => {
                        request()
                            .get(
                                `/api/v1/pages/${pageVerModel.get(
                                    "id"
                                )}/components/${comp.get("uuid")}/files`
                            )
                            .set("Authorization", `Bearer ${token}`)
                            .end((err, res) => {
                                should.not.exist(err);
                                res.status.should.eql(200);
                                res.body.should.have.property("results");
                                res.body.results.should.not.have.lengthOf(0);
                                done();
                            });
                    });
                });
            });
        });
    });

    describe("GET /v1/pages/{pageId}/components/{componentUUID}/files/{fileUUID}", () => {
        it("should retrieve ingle component file", function(done) {
            const { Component, ComponentFile } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp => {
                getTmpImage("component_file.png").then(({ filePath }) => {
                    ComponentFile.create({
                        componentId: comp.get("id"),
                        data: filePath,
                        title: "Component file"
                    }).then(cf => {
                        request()
                            .get(
                                `/api/v1/pages/${pageVerModel.get(
                                    "id"
                                )}/components/${comp.get(
                                    "uuid"
                                )}/files/${cf.get("uuid")}`
                            )
                            .set("Authorization", `Bearer ${token}`)
                            .end((err, res) => {
                                should.not.exist(err);
                                res.status.should.eql(200);
                                res.body.should.have.property("title");
                                res.body.title.should.eql("Component file");
                                done();
                            });
                    });
                });
            });
        });

        it("should retrieve single component file with page version = 3", function(done) {
            const { Component, ComponentFile, PageVersion } = clout.models;

            PageVersion.create({
                pageId: pageVerModel.get("id"),
                version: 3,
                ownerId: userModel.get("id")
            }).then(pageVersion => {
                Component.create({
                    pageVersion: pageVersion.get("id"),
                    typeId: "HEADER_H2"
                }).then(comp => {
                    getTmpImage("component_file.png").then(({ filePath }) => {
                        ComponentFile.create({
                            componentId: comp.get("id"),
                            data: filePath,
                            title: "Component file2"
                        }).then(cf => {
                            request()
                                .get(
                                    `/api/v1/pages/${pageVerModel.get(
                                        "id"
                                    )}/components/${comp.get(
                                        "uuid"
                                    )}/files/${cf.get("uuid")}/?version=3`
                                )
                                .set("Authorization", `Bearer ${token}`)
                                .end((err, res) => {
                                    should.not.exist(err);
                                    res.status.should.eql(200);
                                    res.body.should.have.property("title");
                                    res.body.title.should.eql(
                                        "Component file2"
                                    );
                                    done();
                                });
                        });
                    });
                });
            });
        });
    });

    describe("POST /v1/pages/{pageId}/components/{componentUUID}/files", () => {
        it("should create component file", function(done) {
            const { Component, ComponentFile, PageVersion } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp => {
                getTmpImage("component_file.png").then(({ filePath }) => {
                    let data = {
                        data: fs.readFileSync(filePath),
                        title: "Component file"
                    };

                    request()
                        .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                        .set("Authorization", `Bearer ${token}`)
                        .send()
                        .end(() => {
                            request()
                                .post(
                                    `/api/v1/pages/${pageVerModel.get(
                                        "id"
                                    )}/components/${comp.get("uuid")}/files`
                                )
                                .set("Authorization", `Bearer ${token}`)
                                .field("title", data.title)
                                .attach("data", data.data, "component_file.png")
                                .end((err, res) => {
                                    should.not.exist(err);
                                    res.status.should.eql(200);
                                    res.body.should.have.property("title");
                                    res.body.title.should.eql("Component file");
                                    fs.unlinkSync(res.body.data);
                                    done();
                                });
                        });
                });
            });
        });

        it("should upload only image for imagegrid component", function(done) {
            const { Component, ComponentFile, PageVersion } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: Component.TYPE.IMAGE_GRID
            }).then(comp => {
                getTmpImage("component_file.txt").then(({ filePath }) => {
                    let data = {
                        data: fs.readFileSync(filePath),
                        title: "Component file"
                    };

                    request()
                        .post(`/api/v1/pages/${pageVerModel.get("id")}/lock`)
                        .set("Authorization", `Bearer ${token}`)
                        .send()
                        .end(() => {
                            request()
                                .post(
                                    `/api/v1/pages/${pageVerModel.get(
                                        "id"
                                    )}/components/${comp.get("uuid")}/files`
                                )
                                .set("Authorization", `Bearer ${token}`)
                                .field("title", data.title)
                                .attach("data", data.data, "component_file.txt")
                                .end((err, res) => {
                                    should.exist(err);
                                    res.status.should.eql(400);
                                    done();
                                });
                        });
                });
            });
        });
    });

    describe("PATCH /v1/pages/{pageId}/components/{componentUUID}/files/{fileUUID}", () => {
        it("should update component file", function(done) {
            const { Component, ComponentFile, PageVersion } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp => {
                getTmpImage("component_file.png")
                    .then(({ filePath }) =>
                        ComponentFile.create({
                            componentId: comp.get("id"),
                            title: "Component file",
                            data: filePath
                        })
                    )
                    .then(compFile => {
                        request()
                            .post(
                                `/api/v1/pages/${pageVerModel.get("id")}/lock`
                            )
                            .set("Authorization", `Bearer ${token}`)
                            .send()
                            .end(() => {
                                request()
                                    .patch(
                                        `/api/v1/pages/${pageVerModel.get(
                                            "id"
                                        )}/components/${comp.get(
                                            "uuid"
                                        )}/files/${compFile.get("uuid")}`
                                    )
                                    .set("Authorization", `Bearer ${token}`)
                                    .field("title", "Component file new title")
                                    .field(
                                        "description",
                                        "Component file description"
                                    )
                                    .end((err, res) => {
                                        should.not.exist(err);
                                        res.status.should.eql(200);
                                        res.body.should.have.property("title");
                                        res.body.title.should.eql(
                                            "Component file new title"
                                        );
                                        res.body.should.have.property(
                                            "description"
                                        );
                                        res.body.description.should.eql(
                                            "Component file description"
                                        );
                                        done();
                                    });
                            });
                    });
            });
        });
    });

    describe("DELETE /v1/pages/{pageId}/components/{componentUUID}/files/{fileUUID}", () => {
        it("should delete component file", function(done) {
            const { Component, ComponentFile, PageVersion } = clout.models;

            Component.create({
                pageVersion: pageVerModel.get("activeVersionId"),
                typeId: "HEADER_H1"
            }).then(comp => {
                getTmpImage("component_file.png")
                    .then(({ filePath }) =>
                        ComponentFile.create({
                            componentId: comp.get("id"),
                            title: "Component file",
                            data: filePath
                        })
                    )
                    .then(compFile => {
                        request()
                            .post(
                                `/api/v1/pages/${pageVerModel.get("id")}/lock`
                            )
                            .set("Authorization", `Bearer ${token}`)
                            .send()
                            .end(() => {
                                request()
                                    .delete(
                                        `/api/v1/pages/${pageVerModel.get(
                                            "id"
                                        )}/components/${comp.get(
                                            "uuid"
                                        )}/files/${compFile.get("uuid")}`
                                    )
                                    .set("Authorization", `Bearer ${token}`)
                                    .end((err, res) => {
                                        should.not.exist(err);
                                        res.status.should.eql(200);
                                        done();
                                    });
                            });
                    });
            });
        });
    });
});
