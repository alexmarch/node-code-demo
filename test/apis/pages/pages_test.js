const jwt = require('jsonwebtoken');
const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();
const { getTmpImage, removeFile } = require('../testLib');

chai.use(chaiHttp);

describe('Pages API', function () {
    let clout, userUser, user2Model, profileModel, profile2Model, orgModel, token, tokenUser;

    const request = () => chai.request(clout.server['http']);

    before(() => clout = testLib.cloutInstance);

    before(done => {
    	const { Page, AuthUser, Organisation, AccountProfile } = clout.models;
    	const secret = clout.config.jwt.secret;

    	AuthUser.create({
    		'username': 'john.doe',
            'email': 'john.doe@test.com',
            'password': 'test1234',
            'isSuperUser': false,
            'dateJoined': Date.now(),
            'firstName': 'John',
            'lastName': 'Doe',
            'isStaff': false,
            'isActive': true,
    	})
    	.then(user => Organisation.create({ name: 'Test organisation' })
    	.then(organisation => Page.create({ userId: user.get('id'), title: 'Developer', organisationId: organisation.get('id') })
    	.then(() => AuthUser.create({
    		'username': 'john.doe2',
            'email': 'john.doe2@test.com',
            'password': 'test12345',
            'isSuperUser': false,
            'dateJoined': Date.now(),
            'firstName': 'John',
            'lastName': 'Doe 2',
            'isStaff': false,
            'isActive': true,
    	})
    	.then(user2 => AccountProfile.create({
    		userId: user.get('id'),
            title: 'Developer',
            organisationId: organisation.get('id'),
            role: AccountProfile.Roles.admin
    	})
        .then(profile => {
            profileModel = profile;

            return AccountProfile.create({
                userId: user2.get('id'),
                title: 'Test user',
                organisationId: organisation.get('id'),
                role: AccountProfile.Roles.user
            });
        })
    	.then(profile2 => {
    		userModel = user;
    		user2Model = user2;
            profile2Model = profile2;
    		orgModel = organisation;
    		token = jwt.sign(userModel.get({ plain: true }), secret);
            tokenUser = jwt.sign(user2Model.get({ plain: true }), secret);
    		done();
    	})))));
    });

    after(done => {
    	Promise.all([
    		profileModel.destroy().then(() => userModel.destroy()),
            profile2Model.destroy().then(()=> user2Model.destroy()).then(() =>  orgModel.destroy()),
    	]).then(() => done());
    });

    describe('GET /api/v1/pages', () => {
    	it('should not get pages for noauthorized user', done => {
    		request().get('/api/v1/pages')
    			.end((err, res) => {
    				should.exist(err);
    				res.status.should.eql(401);
    				done();
    			});
    	});

    	it('should get pages for authorized user', done => {
    		clout.models.Page.createPage(userModel.get('id'), { isPublished: true })
    			.then(() => {
    				request().get('/api/v1/pages')
	    				.set('Authorization', `Bearer ${token}`)
	    				.end((err, res) => {
	    					should.not.exist(err);
	    					res.status.should.eql(200);
	    					res.body.should.not.be.null;
	    					res.body.should.have.property('results');
	    					res.body.should.have.property('count');
	    					res.body.results.should.have.lengthOf(res.body.count);
	    					done();
	    				});
    			});
    	});
    });

    describe('POST /api/v1/pages', () => {
        it('should create page for none admin user', done => {
            let data = {
                'title': 'Area',
                'owner': user2Model.get('id'),
                'parent': null
            };

            request().post('/api/v1/pages')
                .set('Authorization', `Bearer ${tokenUser}`)
                .send(data)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    res.body.should.not.be.null;
                    res.body.should.have.property('id');
                    done();
                });
        });

        it('should have permissions', done => {
             clout.models.Page.createPage(user2Model.get('id'), { isPublished: true })
                .then(page => {
                    Promise.all([
                        clout.models.UserGuard.assignUserPermission(user2Model.get('id'), 'change_page', clout.models.UserGuard.Types.Page, page),
                        clout.models.UserGuard.assignUserPermission(user2Model.get('id'), 'moderate_page', clout.models.UserGuard.Types.Page, page)
                        ])
                        .then(() => {
                            clout.models.UserGuard.hasPermission(user2Model, ['change_page', 'moderate_page'], clout.models.UserGuard.Types.Page, page)
                                .then(perms => {
                                    should.not.equal(perms, null);
                                    should.equal(perms.length, 2);
                                    done();
                                });
                        });
                });
        });

        it('should create child page for user', done => {
            let data = {
                'title': 'Area',
                'owner': user2Model.get('id'),
                'parent': null,
                'is_published': false
            };

            clout.models.Page.createPage(user2Model.get('id'), { isPublished: true })
                .then(page => {
                    Promise.all([
                        clout.models.UserGuard.assignUserPermission(user2Model.get('id'), 'change_page', clout.models.UserGuard.Types.Page, page),
                        clout.models.UserGuard.assignUserPermission(user2Model.get('id'), 'moderate_page', clout.models.UserGuard.Types.Page, page)
                        ])
                        .then(() => {
                            data.parent = page.get('id');
                            request().post('/api/v1/pages')
                                .set('Authorization', `Bearer ${tokenUser}`)
                                .send(data)
                                .end((err, res) => {
                                    should.not.exist(err);
                                    res.status.should.eql(200);
                                    res.body.should.not.be.null;
                                    res.body.should.have.property('id');
                                    done();
                        });
                    });
                });
        });

    	it('should create page for user', done => {
    		let data = {
    			'title': 'Area',
            	'owner': user2Model.get('id'),
            	'parent': null
    		};

    		request().post('/api/v1/pages')
    			.set('Authorization', `Bearer ${token}`)
    			.send(data)
    			.end((err, res) => {
    				should.not.exist(err);
	    			res.status.should.eql(200);
	    			res.body.should.not.be.null;
	    			res.body.should.have.property('id');
	    			done();
    			});
    	});

    	it('should create page with parent pages', function(done) {
    		this.timeout(3000);
    		let data = {
    			'title': 'Area',
            	'owner': user2Model.get('id'),
            	'parent': null
    		};

    		request().post('/api/v1/pages')
    			.set('Authorization', `Bearer ${token}`)
    			.send(data)
    			.end((err, res) => {
    				let data2 = {
    					'title': 'Section',
		            	'owner': user2Model.get('id'),
		            	'parent': res.body.id
    				};

    				request().post('/api/v1/pages')
		    			.set('Authorization', `Bearer ${token}`)
		    			.send(data2)
		    			.end((err, res) => {
		    				let data3 = {
		    					'title': 'Content page',
				            	'owner': user2Model.get('id'),
				            	'parent': res.body.id
    						}
    						request().post('/api/v1/pages')
				    			.set('Authorization', `Bearer ${token}`)
				    			.send(data3)
				    			.end((err, res) => {
		    						should.not.exist(err);
					    			res.status.should.eql(200);
					    			res.body.should.not.be.null;
					    			res.body.should.have.property('parent');
					    			res.body.parent.should.eql(data3.parent);
					    			done();
			    				});
		    			});
    			});
    	});

        it('should create page with isPublished status', done => {
            let data = {
                'title': 'Area',
                'owner': user2Model.get('id'),
                'parent': null
            };

            request().post('/api/v1/pages')
                .set('Authorization', `Bearer ${token}`)
                .send(data)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    res.body.should.not.be.null;
                    res.body.should.have.property('isPublished');
                    res.body.isPublished.should.eql(true);
                    done();
                });
        });

        it('should create test image file', done => {
            getTmpImage('cover_image.png').then(img => {
                should.not.equal(img, null);
                removeFile(img.filePath);
                done();
            });
        });

        it('should create page with cover image', done => {
            let data = {
                'title': 'Area2',
                'owner': user2Model.get('id'),
                'parent': null,
                'cover_image': null
            };

            getTmpImage('cover_image.png').then(img => {
                data.cover_image = img.data;

                request().post('/api/v1/pages')
                    .set('Authorization', `Bearer ${token}`)
                    .attach("cover_image", data.cover_image, img.name)
                    .type('form')
                    .field('title', data.title)
                    .field('owner', data.owner)
                    .end((err, res) => {
                        should.not.exist(err);
                        res.status.should.eql(200);
                        res.body.should.not.be.null;
                        res.body.should.have.property('coverImage');
                        should.not.equal(res.body.coverImage, null);
                        removeFile(img.filePath);
                        done();
                    });
            });
        });
    });

    describe('DELETE /api/v1/pages/{id}', () => {
    	it('should not delete page by id', done => {
    		clout.models.Page.createPage(userModel.get('id'), { isPublished: true })
    			.then(page => {
    				request().delete(`/api/v1/pages/${page.get('id')}`)
				   		.set('Authorization', `Bearer ${token}`)
				    	.end((err, res) => {
				    		should.exist(err);
					    	res.status.should.eql(400);
					    	done();
				    	});
    			});
    	});

    	it('should delete child page by id', done => {
            clout.models.Page.createPage(userModel.get('id'), { isPublished: true })
    			.then(page1 => {
                    request().post(`/api/v1/pages/${page1.get('id')}/lock`)
                        .set('Authorization', `Bearer ${token}`)
                        .send()
                        .end((err, res)=> {
                            clout.models.Page.createPage(userModel.get('id'), { isPublished: true, parent: page1.get('id') })
                            .then(page2 => {
                                 request().post(`/api/v1/pages/${page2.get('id')}/lock`)
                                    .set('Authorization', `Bearer ${token}`)
                                    .send()
                                    .end((err, res)=> {
                                        should.not.exist(err);
                                        request().delete(`/api/v1/pages/${page2.get('id')}`)
                                            .set('Authorization', `Bearer ${token}`)
                                            .end((err, res) => {
                                                should.not.exist(err);
                                                res.status.should.eql(200);
                                                done();
                                            });
                                });
                            });
                        });
    			});
    	});
    });

    describe('PATCH, /api/v1/pages/{id}', () => {
    	it('should update page with title', function(done) {
    		this.timeout(3000);
    		clout.models.Page.createPage(userModel.get('id'), { isPublished: true })
    			.then(page => {
		    		let data = {
		    			title: 'Renamed Area'
		    		};

                    request().post(`/api/v1/pages/${page.get('id')}/lock`)
                        .set('Authorization', `Bearer ${token}`)
                        .send()
                        .end((err, res)=> {
        		    		request().patch(`/api/v1/pages/${page.get('id')}`)
        		    			.set('Authorization', `Bearer ${token}`)
        		    			.send(data)
        		    			.end((err, res) => {
        		    				should.not.exist(err);
        		    				res.body.should.not.be.null;
        					    	res.body.should.have.property('title');
        					    	res.body.title.should.eql(data.title);
        		    				res.status.should.eql(200);
        		    				done();
        		    			});
                        });
		    	});
    	});
        // @TODO - test_can_set_image_for_area
        // @TODO - test_cannot_set_image_for_other_pages
        // @TODO - test_page_is_duplicated_correctly_when_adding_component ( After Components APIs )
        // @TODO - test_can_lock_page
        // @TODO - test_can_unlock_page
        // @TODO - test_different_user_cannot_unlock_page
        // @TODO - test_version_is_published_when_page_is_unlocked ( After Components APIs )
        // @TODO - test_shadow_version_is_deleted_on_discard_changes ( After Components APIs )
        // @TODO - test_page_cover_image_is_reverted
        // @TODO - test_page_lock_can_be_obtained_after_it_expires
        // @TODO - test_page_lock_is_refreshed_when_locking
        // @TODO - test_can_revert_page_version ( After Components APIs )
        // @TODO - test_recently_edited_pages
        // @TODO - test_page_lock_is_reset_when_creating_a_component ( After Components APIs )
        // @TODO - test_page_lock_is_reset_when_deleting_a_component ( After Components APIs )
        // @TODO - test_page_lock_is_reset_when_duplicating_a_component ( After Components APIs )
        // @TODO - test_page_lock_is_reset_when_changing_a_component ( After Components APIs )
        // @TODO - test_page_lock_is_reset_when_adding_component_files ( After Components APIs )
        // @TODO - test_page_lock_is_reset_when_removing_component_files ( After Components APIs )
        // @TODO - test_page_lock_is_reset_when_changing_component_files ( After Components APIs )
        // @TODO - test_page_has_default_title_and_description
        // @TODO - test_only_admins_can_lock_navigation
    });
});
