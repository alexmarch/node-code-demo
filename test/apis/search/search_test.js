const jwt = require("jsonwebtoken");
const chai = require("chai");
const chaiHttp = require("chai-http");
const testLib = require("clout-js/test/lib");
const should = chai.should();
const SphinxMock = require("./sphinx_mock");
const { createPage, getTmpImage } = require("../testLib");
const fs = require('fs');

chai.use(chaiHttp);

const TEST_PORT = 8081;

describe("Search API", function() {
    let clout;

    const request = () => chai.request(clout.server["http"]);

    before(() => (clout = testLib.cloutInstance));

    before(function(done) {
        const { Page, AuthUser, Organisation, AccountProfile } = clout.models;
        const secret = clout.config.jwt.secret;

        this.timeout(3000);

        AuthUser.create({
            username: "john.doe",
            email: "john.doe@test.com",
            password: "test1234",
            isSuperUser: false,
            dateJoined: Date.now(),
            firstName: "John",
            lastName: "Doe",
            isStaff: false,
            isActive: true
        }).then(user =>
            Organisation.create({ name: "Test organisation" }).then(
                organisation =>
                    Page.create({
                        userId: user.get("id"),
                        title: "Developer",
                        organisationId: organisation.get("id")
                    }).then(() =>
                        AuthUser.create({
                            username: "john.doe2",
                            email: "john.doe2@test.com",
                            password: "test12345",
                            isSuperUser: false,
                            dateJoined: Date.now(),
                            firstName: "John",
                            lastName: "Doe 2",
                            isStaff: false,
                            isActive: true
                        }).then(user2 =>
                            AccountProfile.create({
                                userId: user.get("id"),
                                title: "Developer",
                                organisationId: organisation.get("id"),
                                role: AccountProfile.Roles.admin
                            })
                                .then(profile => {
                                    profileModel = profile;

                                    return AccountProfile.create({
                                        userId: user2.get("id"),
                                        title: "Test user",
                                        organisationId: organisation.get("id"),
                                        role: AccountProfile.Roles.user
                                    });
                                })
                                .then(profile2 => {
                                    userModel = user;
                                    user2Model = user2;
                                    profile2Model = profile2;
                                    orgModel = organisation;
                                    token = jwt.sign(
                                        userModel.get({ plain: true }),
                                        secret
                                    );
                                    tokenUser = jwt.sign(
                                        user2Model.get({ plain: true }),
                                        secret
                                    );
                                    createPage(clout, userModel.get("id")).then(
                                        pageVer => {
                                            pageVerModel = pageVer;
                                            done();
                                        }
                                    );
                                })
                        )
                    )
            )
        );
    });

    after(done => {
        Promise.all([
            profileModel.destroy().then(() => userModel.destroy()),
            profile2Model
                .destroy()
                .then(() => user2Model.destroy())
                .then(() => orgModel.destroy())
        ]).then(() => done());
    });

    /**
    	In this test we have 2 options
    **/
    describe("GET /api/v1/search", () => {
        it("should not get query result for noauthorized user", function(done) {
            request()
                .get('/api/v1/search')
                .query({ q: 'test', limit: 10 })
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(401);
                    done();
                });
        });

        it("should get sphinx query result", function(done) {
            this.timeout(4000);

            let data = {
                'title': 'Area',
                'description': 'MyTestPage1',
                'owner': userModel.get('id'),
                'parent': null
            };

            request().post('/api/v1/pages')
                .set('Authorization', `Bearer ${token}`)
                .send(data)
                .end((err, res) => {
                     should.not.exist(err);
                     res.status.should.eql(200);
                     res.body.should.have.property('active_version_id');

                     const pageId = res.body.id;

                      request()
                        .post(`/api/v1/pages/${pageId}/lock`)
                        .set("Authorization", `Bearer ${token}`)
                        .send()
                        .end(() => {

                            const data = {
                                page_version: null,
                                type_id: "H1_HEADER_TEST",
                                data: '{"rand":true}',
                                position: null
                            };

                            request().post(`/api/v1/pages/${pageId}/components`)
                                .set("Authorization", `Bearer ${token}`)
                                .send(data)
                                .end((err, res) => {
                                    should.not.exist(err);
                                    res.status.should.eql(200);
                                    res.body.should.not.be.null;
                                    res.body.should.have.property("typeId");
                                    res.body.typeId.should.eql("H1_HEADER_TEST");
                                    res.body.should.have.property("position");
                                    res.body.position.should.eql(0);

                                    const componentUUID = res.body.uuid;

                                    getTmpImage("component_file.png").then(({ filePath }) => {
                                        let data = {
                                            data: fs.readFileSync(filePath),
                                            title: "Component_file_test"
                                        };

                                        request()
                                            .post(`/api/v1/pages/${pageId}/lock`)
                                            .set("Authorization", `Bearer ${token}`)
                                            .send()
                                            .end(() => {
                                                request()
                                                    .post(`/api/v1/pages/${pageId}/components/${componentUUID}/files`)
                                                    .set("Authorization", `Bearer ${token}`)
                                                    .field("title", data.title)
                                                    .attach("data", data.data, "component_file.png")
                                                    .end((err, res) => {
                                                        should.not.exist(err);
                                                        res.status.should.eql(200);
                                                        res.body.should.have.property("title");
                                                        res.body.title.should.eql("Component_file_test");
                                                        Promise.resolve(fs.unlinkSync(res.body.data));
                                                        const sphinx = new SphinxMock(clout);
                                                        sphinx
                                                            .connect()
                                                            .query("H1_HEADER_TEST", 0, 100, [ pageId ])
                                                            .then(result => {
                                                                should.exist(result);
                                                                result.should.have.lengthOf(1);
                                                                done();
                                                            });
                                                    });
                                            });
                                        });
                                });
                        });
                });

        });
    });
});
