const sphinxMock = function(clout) {
    this.clout = clout;

	this.connect = function() {
		return this;
	};
	this.query = function(q, offset=0, limits=500, permissionIds) {
    	const { Page, Component, ComponentFile, PageVersion } = this.clout.models;
    	let pageIds = [], componentIds = [], componentFileIds = [];

        return new Promise((resolve, reject) => {
        	Promise.all([
        		Page.findAll({ where: { description: { $like: `%${q}%` }}}),
        		Component.findAll({ where: { typeId: { $like: `%${q}%`}}}),
        		ComponentFile.findAll({ where: { title: { $like: `%${q}%`}}})
        	])
        	.then(results => {
        		let pages = results[0].map(p => ({
        			attrs: {
        				index_type: 'page',
        				id: p.get('id')
        			}
        		}));
        		let components = results[1].map(c => ({
        			attrs: {
        				index_type: 'component',
        				id: c.get('id')
        			}
        		}));
        		let componentFiles = results[2].map(cf => ({
        			attrs: {
        				index_type: 'componentfile',
        				id: cf.get('id')
        			}
        		}));
        		return Promise.resolve([...pages, ...components, ...componentFiles])
        	})
        	.then(result => {
                    result.forEach(obj => {
                        switch(obj.attrs.index_type) {
                            case 'page':
                                if (permissionIds.indexOf(obj.attrs.id)) {
                                    pageIds.push(obj.attrs.id);
                                }
                                break;
                            case 'component':
                                componentIds.push(obj.attrs.id);
                                break;
                            case 'componentfile':
                                componentFileIds.push(obj.attrs.id);
                                break;
                        };
                    });

                    return Promise.all([
                            Page.findAll({ where: { id: { $in: pageIds } }, include: [{ model: PageVersion, as: 'pageVersion' }]}),
                            Component.findAll({
                                        where: { id: { $in: componentIds } },
                                        include: [{ model: PageVersion, as: 'page_version', where: { pageId: { $in: permissionIds }} }]}),
                            ComponentFile.findAll({
                                        where: { id: { $in: componentFileIds } },
                                        include: [{
                                            model: Component, as: 'component',
                                            include: [{ model: PageVersion, as: 'page_version', where: { pageId: { $in: permissionIds } } }]
                                        }]})
                            ])
                            .then(results => {
                                let result = results
                                    .reduce((prev, next) => prev.concat(next))
                                    .map(itemModel => itemModel.serializer());

                               return resolve(result);
                            })
                            .catch(err => reject(err));

        	});
        });
	}
}

module.exports = sphinxMock;
