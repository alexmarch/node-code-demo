const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();
const CURRENT_APPLICATION_PATH = require('path').join(__dirname, '../../');
const { loadSettings } = require('../../libs/sso');

chai.use(chaiHttp);

describe('SSO API', () => {
    const request = () => chai.request(clout.server['http']);
    let clout, userModel, organisationModel, profileModel;
    let userData = {
        username: 'john.doe_new_123456',
        email: 'john.done_new_123456@test.com',
        password: 'test12345'
    };
    let notExistEmail = 'john.doe_new_test@test.com';
    const loginUser = () => request().post('/api/v1/auth/login').send(userData);

    before(() => clout = testLib.cloutInstance);

    before((done) => {
        const { AuthUser, Organisation, AccountProfile } = clout.models;
        const user = {
            email: userData.email,
            username: userData.username,
            password: userData.password,
            isSuperUser: false,
            dateJoined: Date.now(),
            firstName: 'John',
            lastName: 'User',
            isStaff: false,
            isActive: true,
        }
        Organisation.create({
            'name': 'Test Company'
        }).then(organisation => {
            organisationModel = organisation;
            AuthUser.create(user)
                .then(_user => {
                    userModel = _user;
                    AccountProfile.create({
                        userId: _user.get('id'),
                        title: 'Developer',
                        organisationId: organisationModel.get('id'),
                        message: '',
                        tokenCreatedAt: null,
                        avatar: null,
                        avatarThumbnail: null,
                        ssoLoginCompleted: false,
                        role: AccountProfile.Role.user,
                        hasSeenOnboarding: false
                    }).then(profile => {
                        profileModel = profile;
                        done();
                    });
                });
        });
    });

    after((done) => {
            profileModel.destroy().then(() => {
                organisationModel.destroy()
                    .then(() => userModel.destroy()
                    .then(() => clout.models.AuthUser.findOne({ where: { email: notExistEmail }})
                    .then(user => clout.models.AccountProfile.findOne({ where: { userId: user.get('id') } })
                    .then(profile => {
                        profile.destroy();
                        user.destroy();
                        clout.stop().then(() => done());
                    })
                    .catch(err => { throw Error(err) })
                    .catch(err => {
                        clout.stop().then(() => done());
                        throw err;
                    }))));
                });
    });

    describe('GET /api/v1/auth/sso/metadata', () => {
        it('should response metadata with status 200 application/xml', done => {
            request().get('/api/v1/auth/sso/metadata')
            .end((err, res) => {
                should.not.exist(err);
                res.status.should.eql(200);
                res.type.should.eql('application/xml');
                done();
            });
        })
    });

    describe('GET /api/v1/auth/sso', () => {
        it('should response with redirect URL', done => {
            loginUser().end((err, res) => {
                    const token = `Bearer ${res.body.token}`;
                    const { AuthUser } = clout.models;
                    AuthUser.count().then(countBefore => {
                            request().get('/api/v1/auth/sso')
                                .set('Authorization', token)
                                .query({ samlNameId: userData.email })
                                .end((err, res) => {
                                    should.exist(err);
                                    chai.should(res).redirect;
                                    done();
                                });
                        });
                });
        });
        it('should create user and profile successfuly', done => {
            loginUser().end((err, res) => {
                    const token = `Bearer ${res.body.token}`;
                    const { AuthUser } = clout.models;
                    AuthUser.count().then(countBefore => {
                            request().get('/api/v1/auth/sso')
                                .set('Authorization', token)
                                .query({ samlNameId: notExistEmail })
                                .end((err, res) => {
                                    should.exist(err);
                                    chai.should(res).redirect;
                                    AuthUser.count().then(countAfter => {
                                        countAfter.should.equal(countBefore + 1);
                                        done();
                                    });
                                });
                    });
                });
        });
        it('should complete user profile', done => {
            userModel.update({ firstName: 'Unknown', lastName: 'Unknown' })
                .then(() => profileModel.update({ ssoLoginCompleted: false, title: 'Unknown' })
                .then(() => new Promise((resolve, reject) => {
                    request().get('/api/v1/auth/sso')
                    .query({ samlNameId: userModel.get('email') })
                    .end((err, res) => {
                        should.exist(err);
                        chai.should(res).redirect;
                        resolve();
                    })
                })
                .then(() =>  clout.models.AccountProfile.findOne({ where: { userId: userModel.get('id') }})
                .then(profile => {
                    const data = {
                        token: profile.get('token'),
                        firstName: 'John',
                        lastName: 'Doe',
                        organisationId: 1,
                        title: 'Tester',
                    }
                    request().post('/api/v1/auth/sso/complete-profile')
                        .send(data)
                        .end((err, res) => {
                            res.body.should.have.property('token');
                            done();
                        });
                }))));
        });
        it('should allow user login', done => {
            userModel.update({ firstName: 'Unknown', lastName: 'Unknown', isActive: true, isApproved: true })
            .then(() => profileModel.update({ ssoLoginCompleted: false, title: 'Unknown' })
            .then(() => new Promise((resolve, reject) => {
                    request().get('/api/v1/auth/sso')
                        .query({ samlNameId: userModel.get('email') })
                        .end((err, res) => {
                            should.exist(err);
                            chai.should(res).redirect;
                            resolve();
                        });
            })
            .then(() => clout.models.AccountProfile.findOne({ where: { userId: userModel.get('id') } })
            .then(profile => {
                    request().post('/api/v1/auth/sso/login')
                        .send({ token: profile.get('token') })
                        .end((err, res) => {
                            res.body.should.have.property('token');
                            done();
                         });
            }))));
        });
        it('should not allow user create without nameId', done => {
            request().get('/api/v1/auth/sso')
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(400);
                    done();
                });
        });
        it('should update sso name', done => {
            const samlUserdata = {
                firstName: 'John Tester',
                lastName: 'Doe Tester'
            };
            request().get('/api/v1/auth/sso')
            .query({ samlNameId: userModel.get('email'), samlUserdata: JSON.stringify(samlUserdata) })
            .end((err, res) => {
                should.exist(err);
                chai.should(res).redirect;
                clout.models.AuthUser.findById(userModel.get('id')).then(user => {
                    const plainUser = user.get({ plain: true });
                    plainUser.firstName.should.eql(samlUserdata.firstName);
                    plainUser.lastName.should.eql(samlUserdata.lastName);
                    done();
                });
            });
        });
        it('should redirect user when firstName or lastName is missing', done => {
            request().get('/api/v1/auth/sso')
                .query({ samlNameId: notExistEmail })
                .end((err, res) => {
                    should.exist(err);
                    chai.should(res).redirect;
                    done();
                });
        });
        it('should not allow inactive user to login', done => {
            userModel.update({ firstName: 'Unknown', lastName: 'Unknown', isActive: false, isApproved: false })
                .then(() => profileModel.update({ ssoLoginCompleted: false, title: 'Unknown' })
                .then(() => new Promise(( resolve, reject ) => {
                        request().get('/api/v1/auth/sso')
                            .query({ samlNameId: userModel.get('email') })
                            .end((err, res) => {
                                should.exist(err);
                                chai.should(res).redirect;
                                //return done();
                                resolve();
                            });
                })
                .then(() => clout.models.AccountProfile.findOne({ where: { userId: userModel.get('id') } })
                .then(profile => {
                    request().post('/api/v1/auth/sso/login')
                            .send({ token: profile.get('token') })
                            .end((err, res) => {
                                should.exist(err);
                                res.status.should.eql(401);
                                done();
                            })
                }))));
        });
        it('should merged SAML settings files', done => {
            const settings = loadSettings({ hostname: 'localhost '});
            settings.should.have.property('contactPerson');
            settings.should.have.property('organization');
            done();
        });
    });
});

