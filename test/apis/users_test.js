const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const { setupUserTest, destroyUserTest, destroyEachUsersGetTest } = require('./setupUserTest');
const fs = require("fs");

const should = chai.should();

chai.use(chaiHttp);

describe('User API', function () {
    let clout;

    const request = () => chai.request(clout.server['http']);

    before(() => { clout = testLib.cloutInstance });

    before(function(done) { this.timeout(3000); setupUserTest(clout).then((newClout) => { clout = newClout; done() }) });

    after(function(done) { destroyUserTest(clout).then(() => done()) });

    describe('Setup test clout properties', () => {
        it('should have admin and moderator tokens', done => {
            clout.should.have.property('adminAuthToken');
            clout.should.have.property('moderatorAuthToken');
            done();
        });
    });

    describe('GET /api/v1/users', () => {


        afterEach((done) => {
            this.timeout(100);
            destroyEachUsersGetTest(clout).then(() => done())
        });

        it('should response with status 200 for user admin', done => {
            request().get('/api/v1/users')
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    done();
                });
        });
        it('should response with status 200 for user moderator', done => {
            request().get('/api/v1/users')
                .set('Authorization', `Bearer ${clout.moderatorAuthToken}`)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    done();
                });
        });
        it('should response list of users', done => {
            request().get('/api/v1/users')
                .set('Authorization', `Bearer ${clout.moderatorAuthToken}`)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    res.body.should.not.be.null;
                    res.body.should.have.property('results');
                    res.body.should.have.property('count');
                    res.body.results.should.have.lengthOf(res.body.count);
                    done();
                });
        });
        it('should not allow user to get list of users', done => {
            request().get('/api/v1/users')
                .set('Authorization', `Bearer ${clout.userAuthToken}`)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(401);
                    done();
                });
        });

        it('should can filter query', done => {
            request().get('/api/v1/users')
                .query({'q': 'jane'})
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.should.have.property('results');
                    res.body.should.have.property('count');
                    res.body.results.should.have.lengthOf(res.body.count);
                    res.body.results[0].first_name.should.eql('Jane');
                    done();
                });
        });
        it('should can filter organisation', done => {
            request().get('/api/v1/users')
                .query({'organisations[]': clout.superCompany.get('id')})
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.should.have.property('results');
                    res.body.should.have.property('count');
                    res.body.count.should.eql(3);
                    res.body.results.should.have.lengthOf(res.body.count);
                    res.body.results[0].first_name.should.eql('Bob');
                    res.body.results[1].first_name.should.eql('John');
                    res.body.results[2].first_name.should.eql('Jane');
                    done();
                });
        });
        it('should can filter title', done => {
            request().get('/api/v1/users')
                .query({'titles[]': ['Man', 'ag']})
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.should.have.property('results');
                    res.body.should.have.property('count');
                    res.body.results.should.have.lengthOf(res.body.count);
                    res.body.results[0].first_name.should.eql('Jane');
                    done();
                });
        });
        it('should can filter read permissions', done => {
            const { UserGuard, Page } = clout.models;
            Page.createPage(1)
                .then(page => {
                    UserGuard.assignUserPermission(clout.user.get('id'), UserGuard.Permissions.Page.view, UserGuard.Types.Page, page)
                        .then(() => {
                            request().get('/api/v1/users')
                                .query({'read-area-ids[]': [page.id]})
                                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                                .end((err, res) => {
                                    res.status.should.eql(200);
                                    res.body.should.have.property('results');
                                    res.body.should.have.property('count');
                                    res.body.count.should.eql(1);
                                    res.body.results.should.have.lengthOf(res.body.count);
                                    res.body.results[0].last_name.should.eql('Roe');
                                    done();
                                });
                        });
                });
        });
        it('should can filter write permissions', done => {
            const {UserGuard, Page} = clout.models;
            Page.createPage(1)
                .then(page => {
                    UserGuard.assignUserPermission(clout.moderatorUser.get('id'), UserGuard.Permissions.Page.view, UserGuard.Types.Page, page)
                        .then(() => {
                            UserGuard.assignUserPermission(clout.moderatorUser.get('id'), UserGuard.Permissions.Page.change, UserGuard.Types.Page, page)
                        })
                        .then(() => {
                            request().get('/api/v1/users')
                                .query({'write-area-ids[]': [page.id]})
                                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                                .end((err, res) => {
                                    res.status.should.eql(200);
                                    res.body.should.have.property('results');
                                    res.body.should.have.property('count');
                                    res.body.count.should.eql(1);
                                    res.body.results.should.have.lengthOf(res.body.count);
                                    res.body.results[0].first_name.should.eql('Jane');
                                    done();
                                });
                        });
                });
        });
    });

    describe('GET /api/v1/users/{id}/permissions', () => {
        it('should response object with user permissions', done => {
            const { UserGuard, Page } = clout.models;
            Page.createPage(1)
                .then(page => {
                    UserGuard.assignUserPermission(clout.moderatorUser.get('id'), UserGuard.Permissions.Page.view, UserGuard.Types.Page, page)
                        .then(guards => {
                            request().get(`/api/v1/users/${clout.moderatorUser.get('id')}/permissions`)
                                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                                .end((err, res) => {
                                    should.not.exist(err);
                                    res.status.should.eql(200);
                                    res.body.should.not.be.null;
                                    res.body.should.have.property('read');
                                    res.body.should.have.property('write');
                                    res.body.should.have.property('id');
                                    res.body.id.should.eql(clout.moderatorUser.get('id'));
                                    res.body.read.should.not.have.lengthOf(0);
                                    res.body.write.should.have.lengthOf(0);
                                    res.body.read[0].should.eql(guards[0].get('id'))
                                    done();
                                });
                        });
                });
        });
    });

    describe('PUT /api/v1/users/{id}/permissions', () => {

        it('should set user permission', done => {
            const { UserGuard, Page } = clout.models;
            Page.createPage(1)
                .then(page => {
                    UserGuard.assignUserPermission(clout.moderatorUser.id, UserGuard.Permissions.Page.view, UserGuard.Types.Page, page)
                        .then(guards => {
                            request().put(`/api/v1/users/${clout.moderatorUser.get('id')}/permissions`)
                                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                                .send({ write: [page.get('id')], read: [] })
                                .end((err, res) => {
                                    should.not.exist(err);
                                    res.status.should.eql(200);
                                    res.body.should.not.be.null;
                                    res.body.should.have.property('read');
                                    res.body.should.have.property('write');
                                    res.body.should.have.property('id');
                                    res.body.id.should.eql(clout.moderatorUser.get('id'));
                                    res.body.write.should.have.lengthOf(1);
                                    res.body.write[0].should.eql(page.get('id'))
                                    done();
                                });
                        });
                });
        });

        it('should revoke user permission', function(done) {
            const { UserGuard, Page } = clout.models;
            this.timeout(3000);
            Page.createPage(1)
                .then(page => {
                    UserGuard.assignUserPermission(clout.moderatorUser.id, UserGuard.Permissions.Page.view, UserGuard.Types.Page, page)
                        .then(guards => {
                            request().put(`/api/v1/users/${clout.moderatorUser.get('id')}/permissions`)
                                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                                .send({ write: [page.get('id')], read: [page.get('id')] })
                                .end((err, res) => request().put(`/api/v1/users/${clout.moderatorUser.get('id')}/permissions`)
                                        .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                                        .send({ write: [], read: [] })
                                        .end((err, res) => {
                                            should.not.exist(err);
                                            res.status.should.eql(200);
                                            res.body.should.not.be.null;
                                            res.body.should.have.property('read');
                                            res.body.should.have.property('write');
                                            res.body.should.have.property('id');
                                            res.body.id.should.eql(clout.moderatorUser.get('id'));
                                            res.body.write.should.have.lengthOf(0);
                                            res.body.read.should.have.lengthOf(0);
                                            done();
                                        }));
                        });
                });
        });


        it('should response status 403', done => {
            const { Page } = clout.models;
            Page.createPage(1)
                .then(page => {
                    request().put(`/api/v1/users/${clout.adminUser.get('id')}/permissions`)
                        .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                        .send({ write: [ page.get('id') ], read: [] })
                        .end((err, res) => {
                            should.exist(err);
                            res.status.should.eql(403);
                            done();
                        });
                });
        });

        it.skip('should create bulk permissions', done => { done() });
    });

    describe('GET /api/v1/users/roles', () => {
        it('should response data with roles', done => {
            request().get(`/api/v1/users/roles`)
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    res.body.should.have.property('admin');
                    res.body.should.have.property('user');
                    res.body.should.have.property('moderator');
                    done();
                });
        });
    });

    describe('POST /api/v1/users/{id}/approve', () => {
        it.skip('should approve user by id', function(done) {
            this.timeout(3000);
            request().post(`/api/v1/users/${clout.user.get('id')}/approve`)
            .set('Authorization', `Bearer ${clout.adminAuthToken}`)
            .end((err, res) => {
                should.not.exist(err);
                res.status.should.eql(200);
                res.body.should.have.property('isApproved');
                res.body.isApproved.should.eql(true);
                done();
            });
        });
    });

    describe('POST /api/v1/users/{id}/reject', () => {
        it.skip('should reject user by id', function(done) {
            this.timeout(3000);
            request().post(`/api/v1/users/${clout.user.get('id')}/reject`)
            .set('Authorization', `Bearer ${clout.adminAuthToken}`)
            .end((err, res) => {
                should.not.exist(err);
                res.status.should.eql(200);
                res.body.should.have.property('isApproved');
                res.body.isApproved.should.eql(false);
                done();
            });
        });
    });


    describe('PATCH /api/v1/users/{id}', () => {
        it('should update user details', done => {
            request().patch(`/api/v1/users/${clout.user.get('id')}`)
            .set('Authorization', `Bearer ${clout.adminAuthToken}`)
            .send({ firstName: 'NewFirstName', organisationId: clout.organisation.get("id") })
            .end((err, res) => {
                should.not.exist(err);
                res.status.should.eql(200);
                res.body.should.have.property('user');
                res.body.user.firstName.should.eql('NewFirstName');
                done();
            });
        });
    });

    describe('PATCH /api/v1/users/{id}', () => {
        it('should update user avatar', done => {
            let image = fs.readFileSync(__dirname + '/../assets/avatar.png');
            request().patch(`/api/v1/users/${clout.user.get('id')}`)
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .attach("avatar", image, "avatar.png")
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.should.not.be.null;
                    res.body.avatar.should.not.be.null;
                    res.body.avatarThumbnail.should.not.be.null;
                    fs.unlinkSync(__dirname + '/../../public' + res.body.avatar);
                    fs.unlinkSync(__dirname + '/../../public' + res.body.avatarThumbnail);
                    done();
                });
        });
    });

    describe('POST /api/v1/users/{id}/set-password', () => {
        it('should update user details', done => {
            request().post(`/api/v1/users/${clout.user.get('id')}/set-password`)
            .set('Authorization', `Bearer ${clout.adminAuthToken}`)
            .send({ password: 'newuserpassword' })
            .end((err, res) => {
                should.not.exist(err);
                res.status.should.eql(200);
                done();
            });
        });
    });

    describe('POST /api/v1/users/invite', () => {
        it.skip('should invite user', done => {});
    });
});
