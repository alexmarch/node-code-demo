const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();
const { setupUserTest, destroyUserTest } = require('../setupUserTest');

chai.use(chaiHttp);

describe('Account API Create', function () {
    let clout;

    const request = () => chai.request(clout.server['http']);

    before(() => { clout = testLib.cloutInstance });

    before(function(done) { this.timeout(3000); setupUserTest(clout).then((newClout) => { clout = newClout; done() }) });

    after(function(done) { destroyUserTest(clout).then(() => done()) });

    describe('POST /api/v1/account', () => {
        it('should create account for unauthenticated user', done => {
            const user = {
                firstName: 'John',
                lastName: 'Doe',
                email: 'john.doe@test.com',
                title: 'Senior digital designer',
                organisation: 1,
                message: 'Please grant access to a unknown person!11one',
            };
            request().post('/api/v1/account')
                .send(user)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    done();
                });
        });

        it('should not create admin account unauthenticated user', done => {
            const user = {
                firstName: 'Hacker',
                lastName: 'Doe',
                email: 'hacker.doe@test.com',
                title: 'Senior Hacker',
                organisation: 1,
                message: 'Please grant access to a 1337 person!11one',
                role: 'admin'
            }
            request().post('/api/v1/account')
                .send(user)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(403);
                    done();
                });
        });

        it('should create admin account user', done => {
            const user = {
                firstName: 'NewAdmin',
                lastName: 'NewAdmin',
                email: 'admin1.doe@test.com',
                title: 'Administarator2',
                organisation: 1,
                message: 'Admin message',
                role: 'admin'
            }
            request().post('/api/v1/account')
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .send(user)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    done();
                });
        });
    });
});
