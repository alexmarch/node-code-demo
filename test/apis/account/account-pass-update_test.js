const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();
const { setupUserTest, destroyUserTest } = require('../setupUserTest');

chai.use(chaiHttp);

describe('Account API Update password', function () {
    let clout;

    const request = () => chai.request(clout.server['http']);

    before(() => { clout = testLib.cloutInstance });

    before(function(done) { this.timeout(3000); setupUserTest(clout).then((newClout) => { clout = newClout; done() }) });

    after(function(done) { destroyUserTest(clout).then(() => done()) });

    describe('POST /api/v1/update-password', () => {
        it('should update password with valid password', done => {
            const data = {
                currentPassword: 'test1234',
                password: 'newpassword'
            };
            request().post('/api/v1/update-password')
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .send(data)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    done();
                });
        });

        it('should not update password with wrong old pass', done => {
            const data = {
                currentPassword: 'wrong_old_pass',
                password: 'newpassword'
            };
            request().post('/api/v1/update-password')
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .send(data)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(400);
                    done();
                });
        });
    });
});
