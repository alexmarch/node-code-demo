const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();
const { setupUserTest, destroyUserTest } = require('../setupUserTest');

chai.use(chaiHttp);

describe('Account API reset password', function () {
    let clout;

    const request = () => chai.request(clout.server['http']);

    before(() => { clout = testLib.cloutInstance });

    before(function(done) { this.timeout(3000); setupUserTest(clout).then((newClout) => { clout = newClout; done() }) });

    after(function(done) { destroyUserTest(clout).then(() => done()) });

    describe('POST /api/v1/reset-password', () => {
        it('should send reset link with status 200', function(done) {
            this.timeout(3000);
            const data = {
                email: clout.moderatorUser.get('email')
            };
            request().post('/api/v1/reset-password')
                .send(data)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    done();
                });
        });

        it('should get user details by token', done => {
            let { AccountProfile } = clout.models;
            AccountProfile.findByUserId(clout.moderatorUser.get('id'))
                .then(profile => {
                    const data = {
                        token: profile.get('token'),
                    };
                    request().post('/api/v1/validate-token')
                        .send(data)
                        .end((err, res) => {
                            should.not.exist(err);
                            res.body.should.have.property('firstName');
                            res.body.should.have.property('lastName');
                            res.status.should.eql(200);
                            done();
                        });
                });
        });

        it('should set new password', done => {
            let { AccountProfile } = clout.models;
            AccountProfile.findByUserId(clout.moderatorUser.get('id'))
                .then(profile => {
                    const data = {
                        token: profile.get('token'),
                        password: 'newpassword_1234'
                    };
                    request().post('/api/v1/set-password')
                        .send(data)
                        .end((err, res) => {
                            should.not.exist(err);
                            res.status.should.eql(200);
                            done();
                        });
                });
        });


        it('should not reset password if not active user', done => {
            let { AuthUser } = clout.models;
            this.timeout(3000);
            AuthUser.create({
                username: 'jane.doe',
                email: 'jane.doe1234456@test.com',
                password: 'doe1234456',
                isSuperUser: false,
                dateJoined: Date.now(),
                firstName: 'Jane',
                lastName: 'Doe',
                isStaff: false,
                isActive: false
            })
                .then(user => {
                    let userData = user.get({ plain: true });
                    const data = {
                        email: userData.email
                    };
                    request().post('/api/v1/reset-password')
                        .send(data)
                        .end((err, res) => {
                            should.exist(err);
                            res.status.should.eql(404);
                            user.destroy().then(() => done())
                        });
                });
        });
    });
});
