const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();
const { setupUserTest, destroyUserTest } = require('../setupUserTest');

chai.use(chaiHttp);

describe('Account API Update', function () {
    let clout;

    const request = () => chai.request(clout.server['http']);

    before(() => { clout = testLib.cloutInstance });

    before(function(done) { this.timeout(3000); setupUserTest(clout).then((newClout) => { clout = newClout; done() }) });

    after(function(done) { destroyUserTest(clout).then(() => done()) });

    describe('PATCH /api/v1/account/{id}', () => {
        it('should update user profile', done => {
            const user = {
                firstName: 'John',
                lastName: 'Doe',
                email: 'john.doe@test.com',
                title: 'Senior digital designer',
                organisation: 1,
            };
            request().patch(`/api/v1/account/${clout.user.get('id')}`)
                .set('Authorization', `Bearer ${clout.userAuthToken}`)
                .send(user)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    res.body.should.have.property('user');
                    res.body.user.should.have.property('firstName');
                    res.body.user.should.have.property('lastName');
                    res.body.user.firstName.should.eql(user.firstName);
                    res.body.user.lastName.should.eql(user.lastName);
                    done();
                });
        });

        it('should not update other account by Id', done => {
            const user = {
                firstName: 'Hacker',
                lastName: 'Doe',
                email: 'hacker.doe@test.com',
                title: 'Senior Hacker',
                organisation: 1,
                message: 'Please grant access to a 1337 person!11one',
                role: 'admin'
            }
            request().patch(`/api/v1/account/${clout.moderatorUser.get('id')}`)
                .set('Authorization', `Bearer ${clout.userAuthToken}`)
                .send(user)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(403);
                    done();
                });
        });

        it.skip('should set avatar for user', done => {
            done();
        });

        it('should not update role to admin', done => {
            const user = {
                firstName: 'NewAdmin',
                lastName: 'NewAdmin',
                email: 'admin1.doe@test.com',
                title: 'Administarator2',
                organisation: 1,
                message: 'Admin message',
                role: 'admin'
            }
            request().patch(`/api/v1/account/${clout.user.get('id')}`)
                .set('Authorization', `Bearer ${clout.userAuthToken}`)
                .send(user)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(403);
                    done();
                });
        });

        it('should not update role to admin for moderator', done => {
            const user = {
                firstName: 'NewAdmin',
                lastName: 'NewAdmin',
                email: 'admin1.doe@test.com',
                title: 'Administarator2',
                organisation: 1,
                message: 'Admin message',
                role: 'admin'
            }
            request().patch(`/api/v1/account/${clout.moderatorUser.get('id')}`)
                .set('Authorization', `Bearer ${clout.moderatorAuthToken}`)
                .send(user)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(403);
                    done();
                });
        });

        it('should update role to admin for admin user', done => {
            const user = {
                firstName: 'NewAdmin',
                lastName: 'NewAdmin',
                email: 'admin1.doe@test.com',
                title: 'Administarator2',
                organisation: 1,
                message: 'Admin message',
                role: 'admin'
            }
            request().patch(`/api/v1/account/${clout.user.get('id')}`)
                .set('Authorization', `Bearer ${clout.adminAuthToken}`)
                .send(user)
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    done();
                });
        });
    });
});
