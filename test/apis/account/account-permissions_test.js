const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();
const { setupUserTest, destroyUserTest } = require('../setupUserTest');

chai.use(chaiHttp);

describe('Account API Permissions', function () {
    let clout;

    const request = () => chai.request(clout.server['http']);

    before(() => { clout = testLib.cloutInstance });

    before(function(done) { this.timeout(3000); setupUserTest(clout).then((newClout) => { clout = newClout; done() }) });

    after(function(done) { destroyUserTest(clout).then(() => done()) });

    describe('GET /api/v1/account/{id}', () => {
        it('should not get profile details unauthorized use', done => {
            request().get(`/api/v1/account/${clout.user.get('id')}`)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(401);
                    done();
                });
        });

        it('should not get profile with wrong user Id', done => {
            request().get(`/api/v1/account/${clout.adminUser.get('id')}`)
                .set('Authorization', `Bearer ${clout.userAuthToken}`)
                .end((err, res) => {
                    should.exist(err);
                    res.status.should.eql(400);
                    done();
                });
        });

        it('should get profile details for user', done => {
            request().get(`/api/v1/account/${clout.user.get('id')}`)
            .set('Authorization', `Bearer ${clout.userAuthToken}`)
            .end((err, res) => {
                should.not.exist(err);
                res.status.should.eql(200);
                done();
            });
        });

        it('should get profile details for any user with admin role', done => {
            request().get(`/api/v1/account/${clout.user.get('id')}`)
            .set('Authorization', `Bearer ${clout.adminAuthToken}`)
            .end((err, res) => {
                should.not.exist(err);
                res.status.should.eql(200);
                done();
            });
        });
    });
});
