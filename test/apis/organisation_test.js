const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const should = chai.should();

chai.use(chaiHttp);

describe('Organisation API', () => {
    const request = () => chai.request(clout.server['http']);

    before(() => clout = testLib.cloutInstance);

    before(function(done) { done() });

    describe('Organisation API', () => {
        it('should get organisations for unauthorized user', done => {
            request()
                .get('/api/v1/organisations')
                .end((err, res) => {
                    should.not.exist(err);
                    res.status.should.eql(200);
                    done();
                });
        });
        it('should get organisations by Id', done => {
            const { Organisation } = clout.models;
            Organisation.all()
                .then(organisations => {
                    request()
                        .get(`/api/v1/organisations/${organisations[0].get('id')}`)
                        .end((err, res) => {
                            should.not.exist(err);
                            res.status.should.eql(200);
                            done();
                        });
                });
        });
    });
});
