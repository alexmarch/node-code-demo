const chai = require('chai');
const chaiHttp = require('chai-http');
const testLib = require('clout-js/test/lib');
const fs = require("fs");
const should = chai.should();
const { server } = require('./server-mock');
const jwt = require('jsonwebtoken');
const TEST_PORT=3434;

chai.use(chaiHttp);

describe('Embeds API', function () {
    let clout, mockServer, token;

    const request = () => chai.request(clout.server['http']);

    before(() => { clout = testLib.cloutInstance });

    before(function(done) {
        const { AuthUser, AccountProfile, Organisation } = clout.models;

        AuthUser.create({
            username: 'jane.doe',
            email: 'jane_doe@example.com',
            password: 'jane_doe123456',
            isSuperUser: false,
            dateJoined: Date.now(),
            firstName: 'Jane',
            lastName: 'Doe',
            isStaff: false,
            isActive: true
        }).then(user => {
            Organisation.create({ name: 'Super Company' })
                .then(org => {
                    const secret = clout.config.jwt.secret;
                    AccountProfile.create({
                        userId: user.get('id'),
                        title: 'User',
                        organisationId: org.get('id'),
                        role: AccountProfile.Roles.moderator
                    });
                    token = jwt.sign(user.get({ plain: true }), secret);
                    done();
                });
        });
    });

    after(function(done) { done(); });

    describe('GET /api/v1/embeds/extract', () => {
        it('should success retrive preview with status 200', done => {
            let mock = server((req, res) => {
                res.write(`<html><head><meta property="og:image" content="https://testserver/image.jpg" /><meta property="og:title" content="Awesome title" /><meta property="og:description" content="Super description" /></head>`);
            }).listen(TEST_PORT, () => {
                request()
                    .get('/api/v1/embeds/extract')
                    .query({ url: `http://localhost:${TEST_PORT}` })
                    .set('Authorization', `Bearer ${token}`)
                    .end((err, res) => {
                        should.not.exist(err);
                        res.status.should.eql(200);
                        res.type.should.eql('application/json');
                        res.body.should.have.property('title');
                        res.body.should.have.property('image');
                        res.body.should.have.property('description');
                        res.body.title.should.eql('Awesome title');
                        res.body.image.should.eql('https://testserver/image.jpg');
                        res.body.description.should.eql('Super description');
                        mock.close(() => done());
                    });
            });
        });

        it('should response without image', done => {
            let mock = server((req, res) => {
                res.write(`<html><head><meta property="og:title" content="Awesome title" /><meta property="og:description" content="Super description" /></head>`);
            }).listen(TEST_PORT, () => {
                request()
                    .get('/api/v1/embeds/extract')
                    .query({ url: `http://localhost:${TEST_PORT}` })
                    .set('Authorization', `Bearer ${token}`)
                    .end((err, res) => {
                        should.not.exist(err);
                        res.status.should.eql(200);
                        res.type.should.eql('application/json');
                        res.body.should.have.property('title');
                        res.body.should.have.property('image');
                        res.body.should.have.property('description');
                        res.body.title.should.eql('Awesome title');
                        res.body.description.should.eql('Super description')
                        mock.close(() => done());
                    });
            });
        });

        it('should response without image and title', done => {
            let mock = server((req, res) => {
                res.write(`<html><head><meta property="og:description" content="Super description" /></head>`);
            }).listen(TEST_PORT, () => {
                request()
                    .get('/api/v1/embeds/extract')
                    .query({ url: `http://localhost:${TEST_PORT}` })
                    .set('Authorization', `Bearer ${token}`)
                    .end((err, res) => {
                        should.not.exist(err);
                        res.status.should.eql(200);
                        res.type.should.eql('application/json');
                        res.body.should.have.property('title');
                        res.body.should.have.property('image');
                        res.body.should.have.property('description');
                        res.body.description.should.eql('Super description')
                        mock.close(() => done());
                    });
            });
        });

        it('empty meta tags', done => {
            let mock = server((req, res) => {
                res.write(`<html><head></head>`);
            }).listen(TEST_PORT, () => {
                request()
                    .get('/api/v1/embeds/extract')
                    .query({ url: `http://localhost:${TEST_PORT}` })
                    .set('Authorization', `Bearer ${token}`)
                    .end((err, res) => {
                        should.not.exist(err);
                        res.status.should.eql(200);
                        res.type.should.eql('application/json');
                        res.body.should.have.property('title');
                        res.body.should.have.property('image');
                        res.body.should.have.property('description');
                        mock.close(() => done());
                    });
            });
        });
    });
});
