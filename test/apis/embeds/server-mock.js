const http = require('http');

module.exports = {
    server: function (cb, port) {
        this.down = function(cb) {
            this.server.close(cb)
        };

        this.listen = function(cb, port) {
            this.server.listen(port, cb);
        };

        this.server = http.createServer((req, res) => {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            cb(req, res);
            res.end();
        }, port);

        return this.server;
    }
};
