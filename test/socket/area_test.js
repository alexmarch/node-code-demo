/**
 * JWT socket test
 */
const testLib = require('clout-js/test/lib');
const {USER_1} = require('../fixed/userSessionJwt');
const {join} = require('path');
const should = require('should');
const io = require('socket.io-client');
const jwt = require('jwt-simple');
const {merge} = require('lodash');

const DEFAULT_SOCKET_OPTIONS = {
    forceNew: true,
    reconnection: false
};

describe('module: socket', () => {
    let clout;

    before(() => clout = testLib.cloutInstance);

    describe('Area Management test', () => {
        let socket;

        before((done) => {
            let validJwtToken = jwt.encode(USER_1, clout.config.jwt.secret);
            socket = io.connect(testLib.config.serverAddress + '/areas', merge({
                query: `auth_token=${validJwtToken}`
            }, DEFAULT_SOCKET_OPTIONS));

            socket.on('connect', () => {
                done();
            });
        });

        after(() => socket.close());

        it('should not be be able to subscribe to a invalid area', (done) => {
            socket.emit('subscribe', {}, (err, response) => {
                should(err).be.deepEqual({ code: 404, message: 'Please define an areaId' });
                done();
            });
        });

        it('should be able to subscribe to an area', (done) => {
            socket.emit('subscribe', {
                areaId: 1
            }, (err, response) => {
                should(err).be.equal(null);
                should(response).be.deepEqual({
                    code: 200,
                    message: 'successfully subscribed'
                });
                done();
            });
        });

        it('should recive updates to a page moved in area 1', (done) => {
            let mockResponse = {
                areaId: 1,
                pageId: 1,
                position: 1
            };

            socket.on('pageMoved', (data) => {
                should(data).be.deepEqual(mockResponse);
                done();
            });
            socket.emit('pageMoved', mockResponse, () => {});
        });

        it('should recive updates to a page delete in subscribed area 1', (done) => {
            let mockResponse = {
                areaId: 1,
                pageId: 1
            };

            socket.on('pageDeleted', (data) => {
                should(data).be.deepEqual(mockResponse);
                done();
            });
            socket.emit('pageDeleted', mockResponse, () => {});
        });

        it('should recive updates to a page renamed in subscribed area 1', (done) => {
            let mockResponse = {
                areaId: 1,
                pageId: 1,
                title: 'new title'
            };

            socket.on('pageRenamed', (data) => {
                should(data).be.deepEqual(mockResponse);
                done();
            });
            socket.emit('pageRenamed', mockResponse, () => {});
        });

        it('should not be able to emit to unsubscribed area', (done) => {
            let mockResponse = {
                areaId: 4,
                pageId: 20,
                position: 1
            };

            socket.emit('pageMoved', mockResponse, (err) => {
                should(err).not.equal(null);
                done();
            });
        });

        it('should be able to unsubscribe to an area', (done) => {
            socket.emit('unsubscribe', {
                areaId: 1
            }, (err, response) => {
                should(err).be.equal(null);
                should(response).be.deepEqual({
                    code: 200,
                    message: 'successfully unsubscribed'
                });
                done();
            });
        });
    });
});
