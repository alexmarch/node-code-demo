/**
 * JWT socket test
 */
const testLib = require('clout-js/test/lib');
const {join} = require('path');
const should = require('should');
const io = require('socket.io-client');
const jwt = require('jwt-simple');
const {merge} = require('lodash');

const DEFAULT_SOCKET_OPTIONS = {
    forceNew: true,
    reconnection: false
};
const {USER_1} = require('../fixed/userSessionJwt');

describe('module: socket', () => {
    let clout;

    before(() => clout = testLib.cloutInstance);

    describe('JWT test', () => {
        it('should reject invalid JWT request', (done) => {
            let invalidJwtToken = jwt.encode(USER_1, 'notavalidjwtsecret');
            let socket = io.connect(testLib.config.serverAddress, merge({
                query: `auth_token=${invalidJwtToken}`
            }, DEFAULT_SOCKET_OPTIONS));

            socket.on('error', () => {
                socket.close();
                done();
            });
        });

        it('should reject expired JWT request', (done) => {
            let expiryTimeS = Date.now() / 1000 - 100;
            let expiredJwtToken = jwt.encode(merge({}, USER_1, {exp: expiryTimeS}), clout.config.jwt.secret);
            let socket = io.connect(testLib.config.serverAddress, merge({
                query: `auth_token=${expiredJwtToken}`
            }, DEFAULT_SOCKET_OPTIONS));

            socket.on('error', () => {
                socket.close();
                done();
            });
        });

        it('should accept valid JWT request', (done) => {
            let validJwtToken = jwt.encode(USER_1, clout.config.jwt.secret);
            let socket = io.connect(testLib.config.serverAddress, merge({
                query: `auth_token=${validJwtToken}`
            }, DEFAULT_SOCKET_OPTIONS));

            socket.on('connect', () =>{
                socket.close();
                done();
            });
        });
    });
});
