/**
 * test/fixed/userSessionJwt
 */

module.exports = {
    USER_1: {
        exp: Date.now() / 1000 + 100,
        username: 'ob6j-0z9s-12qe-1ufz',
        email: "test@test.com",
        user_id: 2,
        organisation: 1,
        first_name: 'Test',
        last_name: 'Name',
        is_editor: true,
        is_external: false,
        orig_iat: 1519329444,
        role: 'admin',
        title: 'Developer',
        avatar: '',
        avatar_thumbnail: '',
        has_seen_onboarding: true
    }
};
