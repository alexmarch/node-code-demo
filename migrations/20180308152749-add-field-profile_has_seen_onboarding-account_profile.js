'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('account_profile', 'has_seen_onboarding', {
        type: Sequelize.BOOLEAN,
        default: false
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('account_profile', 'has_seen_onboarding');
  }
};
