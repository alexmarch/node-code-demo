'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('pages_page', 'position', {
            type: Sequelize.INTEGER,
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('pages_page', 'position');
    }
};
