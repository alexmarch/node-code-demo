'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('pages_pageversion', 'createdAt', {
            type: Sequelize.DATE,
            defaultValue: Date.now()
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('pages_pageversion', 'createdAt');
    }
};
