'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('managed_users', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                type: Sequelize.INTEGER,
            },
            profile_id: {
                type: Sequelize.INTEGER,
                references: null
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('managed_users');
    }
};
