'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('user_guard', {
          id: {
              type: Sequelize.INTEGER,
              primaryKey: true,
              autoIncrement: true,
          },
          permission: {
              type: Sequelize.STRING(255),
              allowNull: false
          },
          objectType: {
              type: Sequelize.ENUM('Page', 'Component'),
              allowNull: false
          },
          userId: {
              type: Sequelize.INTEGER,
              allowNull: false
          },
          objectId: {
              type: Sequelize.INTEGER,
              allowNull: false
          }
      });
  },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('user_guard');
    }
};
