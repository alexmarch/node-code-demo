'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('auth_user_groups', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      userId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'auth_user',
          key: 'id',
        },
        field: 'user_id'
      },
      groupId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'auth_group',
          key: 'id'
        },
        field: 'group_id'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('auth_user_groups', {
      force: true,
      cascade: false
    });
  }
};
