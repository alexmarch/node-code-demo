'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('organisation_organisation', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: true
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.dropTable('organisation_organisation', {
      force: true,
      cascade: false,
    });
  }
};
