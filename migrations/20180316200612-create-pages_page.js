'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('pages_page', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            lft: {
                type: Sequelize.INTEGER(10),
                allowNull: true
            },
            right: {
                type: Sequelize.INTEGER(10),
                allowNull: true
            },
            treeId: {
                type: Sequelize.INTEGER(10),
                field: 'tree_id',
                allowNull: true
            },
            level: {
                type: Sequelize.INTEGER(10),
                field: 'level',
                allowNull: true
            },
            parentId: {
                type: Sequelize.INTEGER,
                field: 'parent_id',
                allowNull: true
            },
            isPublished: {
                type: Sequelize.BOOLEAN,
                field: 'is_published',
                default: true
            },
            description: {
                type: Sequelize.STRING(255),
                field: 'description'
            },
            activeVersionId: {
                type: Sequelize.INTEGER(11),
                field: 'active_version_id',
                allowNull: true
            },
            isActive: {
                type: Sequelize.BOOLEAN,
                field: 'is_active',
                default: true
            },
            lockedAt: {
                type: Sequelize.DATE,
                field: 'locked_at',
                allowNull: true
            },
            lockedById: {
                type: Sequelize.INTEGER,
                field: 'locked_by_id',
                allowNull: true
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('pages_page');
    }
};
