'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('auth_user', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      password: {
        type: Sequelize.STRING(128),
        allowNull: false,
      },
      lastLogin: {
        type: Sequelize.DATE,
        field: 'last_login'
      },
      isSuperUser: {
        type: Sequelize.BOOLEAN,
        field: 'is_superuser',
        default: false
      },
      username: {
        type: Sequelize.STRING(30),
        field: 'username'
      },
      firstName: {
        type: Sequelize.STRING(30),
        field: 'first_name'
      },
      lastName: {
        type: Sequelize.STRING(30),
        field: 'last_name'
      },
      email: {
        type: Sequelize.STRING(254),
        field: 'email'
      },
      isStaff: {
        type: Sequelize.BOOLEAN,
        field: 'is_staff',
        default: false
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        field: 'is_active',
        default: false
      },
      dateJoined: {
        type: Sequelize.DATE,
        field: 'date_joined',
        default: Date.now()
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('auth_user', { force: true });
  }
};
