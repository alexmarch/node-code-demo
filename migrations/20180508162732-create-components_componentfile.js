"use strict";

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable("components_componentfile", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            title: {
                type: Sequelize.STRING(250),
                allowNull: false,
                field: "title"
            },
            description: {
                type: Sequelize.STRING(250),
                allowNull: true,
                field: "description"
            },
            originalFileName: {
                type: Sequelize.STRING(250),
                allowNull: true,
                field: "original_file_name"
            },
            position: {
                type: Sequelize.INTEGER,
                allowNull: true,
                field: "position"
            },
            componentId: {
                type: Sequelize.INTEGER,
                field: "component_id"
            },
            originComponentId: {
                type: Sequelize.INTEGER,
                allowNull: true,
                field: "origin_component_id"
            },
            uuid: {
                type: Sequelize.UUIDV4,
                allowNull: false,
                field: "uuid"
            },
            data: {
                type: Sequelize.STRING(100),
                allowNull: true
            },
            preview: {
                type: Sequelize.STRING(100),
                allowNull: true
            },
            createdAt: {
                type: Sequelize.DATE,
                defaultValue: Date.now()
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable("components_componentfile");
    }
};
