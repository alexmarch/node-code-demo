'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('account_profile', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      title: {
        type: Sequelize.STRING(255),
        allowNull: false,
      },
      message: {
        type: Sequelize.STRING(255),
      },
      token: {
        type: Sequelize.STRING(40),
      },
      organisationId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'organisation_organisation',
          key: 'id',
        },
        field: 'organisation_id'
      },
      userId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'auth_user',
          key: 'id',
        },
        field: 'user_id'
      },
      tokenCreatedAt: {
        type: Sequelize.DATE,
        field: 'token_created_at'
      },
      isApproved: {
        type: Sequelize.BOOLEAN,
        field: 'is_approved',
        default: false
      },
      avatar: {
        type: Sequelize.STRING(100),
      },
      avatarThumbnail: {
        type: Sequelize.STRING(100),
        field: 'avatar_thumbnail'
      },
      ssoLoginCompleted: {
        type: Sequelize.BOOLEAN,
        field: 'sso_login_completed',
        default: false
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('account_profile', {
      force: true,
      cascade: false
    });
  }
};
