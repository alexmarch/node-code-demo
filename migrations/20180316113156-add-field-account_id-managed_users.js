'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('managed_users', 'account_id', {
            type: Sequelize.INTEGER,
            references: null
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('managed_users', 'account_id');
    }
};
