const JwtStrategy = require('passport-jwt').Strategy;

module.exports = function (conf) {
  var self = this; // clout context
  return new JwtStrategy(conf, (payload, done) => {
    const expiredAt = payload.expiredAt || payload.exp;
    if (expiredAt < Date.now() / 1000) {
      return done(new Error("The token has expired."), null);
    }
    self.models.AuthUser
      .findById(payload.id)
      .then((user) => {
        if (user) {
          self.models.AccountProfile.findOne({ where: { userId: user.get('id') }})
            .then(profile => {
              let userData =  user.userSerializer();
              userData.profile = profile.get({ plain: true });
              return done(null, userData);
            })
        } else {
          return done(null, null);
        }
      })
      .catch(err => done(err, null))
  });
};
