const clout = require('clout-js');
const { Sequelize, sequelize } = clout;
const { COMPONENT_TYPES, componentFileThumbPath } = clout.config;
const Component = require('./Component');
const uuidv4 = require('uuid/v4');
const { toSnakeCase, makeThumbnail } = require('../libs/utils');
const { MAGIC_MIME_TYPE, Magic } = require('mmmagic');
const magic = new Magic(MAGIC_MIME_TYPE);
const path = require('path');
const destPath = path.resolve(__dirname, '../public/components/file_thumbnail');

/**
 * ComponentFile table model
 */
const ComponentFile = sequelize.define(
    'ComponentFile',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: Sequelize.STRING(250),
            allowNull: false,
            field: 'title'
        },
        description: {
            type: Sequelize.STRING(250),
            allowNull: true,
            field: 'description'
        },
        originalFileName: {
            type: Sequelize.STRING(250),
            allowNull: true,
            field: 'original_file_name'
        },
        position: {
            type: Sequelize.INTEGER,
            allowNull: true,
            field: 'position'
        },
        componentId: {
            type: Sequelize.INTEGER,
            references: {
                model: Component,
                key: 'id'
            },
            field: 'component_id'
        },
        originComponentId: {
            type: Sequelize.INTEGER,
            references: {
                model: Component,
                key: 'id'
            },
            allowNull: true,
            field: 'origin_component_id'
        },
        uuid: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            field: 'uuid'
        },
        data: {
            type: Sequelize.STRING(100),
            allowNull: true
        },
        preview: {
            type: Sequelize.STRING(100),
            allowNull: true
        }
    },
    {
        updatedAt: false,
        tableName: 'components_componentfile'
    }
);

ComponentFile.beforeValidate((component, options) => {
    if (!component.position) {
        component.position = 0;
    }
    if (!component.uuid) {
        component.uuid = uuidv4();
    }
    return sequelize.Promise.resolve(component);
});

ComponentFile.beforeCreate((component, options) => {
    component.uuid = uuidv4();
    return sequelize.Promise.resolve(component);
});

ComponentFile.createComponentFile = (data, component) => {
    data.componentId = component.get('id');

    return ComponentFile.create(data).then(fileComponent => {
        return ComponentFile.makeThumbnail(data, fileComponent.get('uuid'))
            .then(() => ComponentFile.findById(fileComponent.get('id')));
    });
};

ComponentFile.updateComponentFile = (data, component, uuid) => {
    let updatePromise;

    if (data.partial) {
        updatePromise = ComponentFile.updateAttributes(data, {
            where: { uuid }
        });
    } else {
        updatePromise = ComponentFile.update(data, { where: { uuid } });
    }

    return updatePromise.then(() => {
        return ComponentFile.makeThumbnail(data, uuid)
            .then(() => ComponentFile.findOne({ where: { uuid } }))
    });
};

ComponentFile.makeThumbnail = (data, uuid) => {
    let isNeedThumbnail = COMPONENT_TYPES.indexOf(data.typeId) > -1;
    if (isNeedThumbnail) {
        return makeThumbnail(data.data, destPath)
            .then(() => ComponentFile.update({ preview: `${destPath}/${path.basename(data.data)}` }, { where: { uuid }}));
    }
    return Promise.resolve(data);
};

ComponentFile.Instance.prototype.serializer = function() {
    let componentFile = this.get({ plain: true });
    return toSnakeCase(componentFile);
};

ComponentFile.isImage = path => {
    const mimeTypes = [
        'image/gif',
        'image/png',
        'image/jpeg',
        'image/bmp',
        'image/webp'
    ];
    return new Promise((resolve, reject) => {
        magic.detectFile(path, (err, mimeType) => {
            if (err) return reject(err);
            resolve(mimeTypes.indexOf(mimeType) > -1);
        });
    });
};
ComponentFile.belongsTo(Component, {
    as: 'component',
    foreignKey: 'componentId'
});
module.exports = ComponentFile;
