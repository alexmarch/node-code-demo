const clout = require('clout-js');
const { Sequelize, sequelize } = clout;
const { hashPassword } = require('../libs/crypto');
const UserGuard = require('./UserGuard');
const Page = require('./Page');
const AccoutProfile = require('./AccountProfile');
const _ = require('lodash');

/**
 * AuthUser table model
 */
const AuthUser = sequelize.define(
    'AuthUser',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        password: {
            type: Sequelize.STRING(128),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        lastLogin: {
            type: Sequelize.DATE,
            field: 'last_login'
        },
        isSuperUser: {
            type: Sequelize.BOOLEAN,
            field: 'is_superuser',
            default: false
        },
        username: {
            type: Sequelize.STRING(30),
            field: 'username',
            validate: {
                notEmpty: true
            }
        },
        firstName: {
            type: Sequelize.STRING(30),
            field: 'first_name',
            validate: {
                notEmpty: true
            }
        },
        lastName: {
            type: Sequelize.STRING(30),
            field: 'last_name',
            validate: {
                notEmpty: true
            }
        },
        email: {
            type: Sequelize.STRING(254),
            field: 'email',
            validate: {
                isEmail: true,
                notEmpty: true
            }
        },
        isStaff: {
            type: Sequelize.BOOLEAN,
            field: 'is_staff',
            default: false
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            field: 'is_active',
            default: false
        },
        dateJoined: {
            type: Sequelize.DATE,
            field: 'date_joined',
            default: Date.now()
        }
    },
    {
        createdAt: false,
        updatedAt: false,
        tableName: 'auth_user'
    }
);

AuthUser.beforeCreate((user, options) => {
    user.email = user.email.toLowerCase();
    return hashPassword(user.password).then(
        hashPassword => (user.password = hashPassword)
    );
});

AuthUser.serializeUser = () => {
    return (user, done) => {
        const id = user ? user.id : null;
        done(null, id);
    };
};

AuthUser.deserializeUser = () => {
    return (id, done) => {
        AuthUser.find(id)
            .then(user =>
                AccountProfile.findOne({ where: { userId: id } }).then(
                    profile => {
                        let userData = user.get({ plain: true });
                        let profileData = profile.get({ plain: true });
                        userData.profile = profileData;
                        done(null, userData);
                    }
                )
            )
            .catch(err => done(err, null));
    };
};

AuthUser.getUsersByQueryParams = (user, query) => {
    let replacements = {};
    replacements.userId = user.id;
    let q = '';
    let organisations = '';
    let id = '';
    let role = '';
    let roles = '';
    let isActive = '';
    let isApproved = '';
    let titles = '';
    let writeAreaIds = '';
    let readAreaIds = '';

    if (query.q) {
        replacements.q = `%${query.q}%`;
        q = `and (auth_user.email LIKE :q or auth_user.first_name LIKE :q or auth_user.last_name LIKE :q)`;
    }

    if (query.id && query.id.length !== 0) {
        replacements.id = query.id;
        id = `and auth_user.id in (:id)`;
    }

    if (query.role) {
        replacements.role = query.role;
        role = `and account_profile.role = :role`;
    }

    if (query.roles && query.roles.length !== 0) {
        replacements.roles = query.roles;
        roles = `and account_profile.role in (:roles)`;
    }

    if (query.organisations && query.organisations.length !== 0) {
        replacements.organisations = query.organisations;
        organisations = `and account_profile.organisation_id in (:organisations)`;
    }

    if (query.is_active) {
        if (query.is_active == 'true' || query.is_active == 1) {
            isActive = `and auth_user.is_active = 1`;
        } else {
            isActive = `and (auth_user.is_active = 0 or auth_user.is_active is NULL)`;
        }
    }

    if (query.is_approved) {
        if (query.is_approved == 'true' || query.is_approved == 1) {
            isApproved = `and auth_user.is_approved = 1`;
        } else {
            isApproved = `and (auth_user.is_approved = 0 or auth_user.is_approved is NULL)`;
        }
    }

    if (query.titles) {
        if (Array.isArray(query.titles)) {
            titles = `and ( account_profile.title LIKE ('%${
                query.titles[0]
            }%') `;
            for (let i = 1; i < query.titles.length; i++) {
                titles += `or account_profile.title LIKE ('%${
                    query.titles[i]
                }%') `;
            }
            titles += ' )';
        } else {
            titles = `and account_profile.title LIKE ('%${query.titles}%') `;
        }
    }

    if (query['write-area-ids']) {
        if (Array.isArray(query['write-area-ids'])) {
            writeAreaIds = ` and ( (user_guard.permission = '${
                UserGuard.Permissions.Page.change
            }' and user_guard.objectId = ${query['write-area-ids'][0]}) `;

            for (let i = 1; i < query['write-area-ids'].length; i++) {
                writeAreaIds += ` or (user_guard.permission = '${
                    UserGuard.Permissions.Page.change
                }' and user_guard.objectId = ${query['write-area-ids'][i]})`;
            }

            writeAreaIds += ')';
        } else {
            writeAreaIds += ` and (user_guard.permission = '${
                UserGuard.Permissions.Page.change
            }' and user_guard.objectId = ${query['write-area-ids']})`;
        }
    }

    if (query['read-area-ids']) {
        if (Array.isArray(query['read-area-ids'])) {
            readAreaIds = ` and ( (user_guard.permission = '${
                UserGuard.Permissions.Page.view
            }' and user_guard.objectId = ${query['read-area-ids'][0]}) `;

            for (let i = 1; i < query['read-area-ids'].length; i++) {
                readAreaIds += ` or (user_guard.permission = '${
                    UserGuard.Permissions.Page.view
                }' and user_guard.objectId = ${query['read-area-ids'][i]})`;
            }

            readAreaIds += ' )';
        } else {
            readAreaIds += ` and (user_guard.permission = '${
                UserGuard.Permissions.Page.view
            }' and user_guard.objectId = ${query['read-area-ids']})`;
        }
    }

    return sequelize.query(
        `
                SELECT
                    auth_user.id as id,
                    auth_user.last_name as last_name,
                    auth_user.first_name as first_name,
                    auth_user.is_superuser as is_admin,
                    auth_user.is_active as is_active,
                    auth_user.email as email,
                    auth_user.date_joined as date_joined,
                    auth_user.is_staff,
                    auth_user.username,
                    account_profile.avatar as avatart,
                    account_profile.avatar_thumbnail as avatar_thumbnail,
                    account_profile.title as title,
                    account_profile.organisation_id as organisation,
                    account_profile.role as role
                FROM auth_user
                    LEFT JOIN account_profile ON auth_user.id = account_profile.user_id
                    LEFT JOIN organisation_organisation ON account_profile.organisation_id = organisation_organisation.id
                    LEFT JOIN user_guard ON auth_user.id = user_guard.userId
                WHERE
                    auth_user.id <> :userId
                    ${q}
                    ${id}
                    ${organisations}
                    ${role}
                    ${roles}
                    ${isActive}
                    ${isApproved}
                    ${titles}
                    ${writeAreaIds}
                    ${readAreaIds}
                ORDER BY auth_user.id DESC;
            `,
        {
            raw: true,
            type: 'SELECT',
            replacements: replacements
        }
    );
};

AuthUser.getControlledPagesForUser = (user, permissions=[UserGuard.Permissions.Page.moderate]) => {
    return new Promise((resolve, reject) => {
        Page.findAll({ order: 'id DESC' }).then(pages => {
            const isAdmin = user.isSuperUser || user.get('isSuperUser');
            if (user.isSuperUser) {
                resolve(_.map(pages, page => page.get({ plain: true })));
            } else {
                UserGuard.getGuardObjectsForUser(
                    user,
                    permissions,
                    UserGuard.Types.Page,
                    pages
                ).then(objects =>
                    Page.findAll({
                        where: {
                            id: { $in: _.map(objects, obj => obj.objectId) }
                        }
                    }).then(pages =>
                        resolve(_.map(pages, page => page.get({ plain: true })))
                    )
                );
            }
        });
    });
};

AuthUser.isAdmin = user => {
    return user.isSuperAdmin || user.profile.role === 'admin';
};

AuthUser.Instance.prototype.userSerializer = function() {
    let user = Object.assign({}, this.get({ plain: true }));
    user.password = undefined;
    return user;
};

AuthUser.Instance.prototype.setPassword = function(password) {
    return hashPassword(password).then(hashPassword => {
        return AuthUser.update(
            { password: hashPassword },
            { where: { id: this.id } }
        );
    });
};

AuthUser.setPassword = (id, pwd) => {
    return AuthUser.findById(id).then(user =>
        hashPassword(pwd).then(hashedPwd =>
            user
                .update({ password: hashedPwd })
                .then(() => Promise.resolve(user))
        )
    );
};

AuthUser.Instance.prototype.serializer = function() {
    let user = this.get({ plain: true });
    return user;
};

module.exports = AuthUser;
