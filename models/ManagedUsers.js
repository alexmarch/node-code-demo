const clout = require('clout-js');
const { Sequelize, sequelize } = clout;

const ManagedUsers = sequelize.define('ManagedUsers', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.INTEGER,
    },
    profile_id: {
        type: Sequelize.INTEGER,
        references: null
    },
    account_id: {
        type: Sequelize.INTEGER,
        references: null
    }
}, {
        createdAt: false,
        updatedAt: false,
        tableName: 'managed_users'
    });

module.exports = ManagedUsers;
