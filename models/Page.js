const clout = require('clout-js');
const { Sequelize, sequelize } = clout;
const PageVersion = require('./PageVersion');
const { thumb } = require('node-thumbnail');
const { resolve } = require('path');
const { imagesPath, imagesThumbPath } = clout.config;
const imgPath = resolve(__dirname, `../public/${imagesPath}`);
const imgThumbPath = resolve(__dirname, `../public/${imagesThumbPath}`);
const { toSnakeCase } = require('../libs/utils');

/**
 * Page table model
 */
const Page = sequelize.define(
    'Page',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        lft: {
            type: Sequelize.INTEGER(10),
            allowNull: true
        },
        right: {
            type: Sequelize.INTEGER(10),
            allowNull: true
        },
        treeId: {
            type: Sequelize.INTEGER(10),
            field: 'tree_id',
            allowNull: true
        },
        level: {
            type: Sequelize.INTEGER(10),
            field: 'level',
            allowNull: true
        },
        parent: {
            type: Sequelize.INTEGER,
            field: 'parent_id',
            allowNull: true
        },
        isPublished: {
            type: Sequelize.BOOLEAN,
            field: 'is_published',
            default: true
        },
        description: {
            type: Sequelize.STRING(255),
            field: 'description'
        },
        activeVersionId: {
            type: Sequelize.INTEGER(11),
            field: 'active_version_id',
            allowNull: true
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            field: 'is_active',
            default: true
        },
        lockedAt: {
            type: Sequelize.DATE,
            field: 'locked_at',
            allowNull: true
        },
        lockedById: {
            type: Sequelize.INTEGER,
            field: 'locked_by_id',
            allowNull: true
        },
        position: {
            type: Sequelize.INTEGER,
            field: 'position',
            allowNull: true
        }
    },
    {
        updatedAt: false,
        tableName: 'pages_page'
    }
);

Page.belongsTo(PageVersion, {
    as: 'pageVersion',
    foreignKey: 'activeVersionId'
});

PageVersion.belongsTo(Page, { as: 'page', foreignKey: 'pageId' });

Page.Instance.prototype.isLocked = function() {
    return this.get('lockedById');
};

Page.Instance.prototype.unlock = function() {
    this.lockedById = null;
    this.lockedAt = null;
    return this.save();
};

Page.Instance.prototype.lock = function(user) {
    this.lockedById = user.id;
    this.lockedAt = Date.now();
    return this.save();
};

Page.Instance.prototype.assertPageIsLockedBy = function(user) {
    if (!this.isLocked()) {
        return Promise.reject(
            'Page must be locked before attempting to change any of the components.'
        );
    }
    if (!this.isLockedBy(user)) {
        return Promise.reject('Not authorized to change this object.');
    }
    return Promise.resolve(this);
};

Page.Instance.prototype.unlockFor = function(user) {
    return new Promise((resolve, reject) => {
        if (!this.isLocked()) {
            return resolve(this.serializer());
        }
        if (user.isStaff) {
            return this.unlock().then(() => resolve(this.serializer()));
        } else if (this.lockedById !== user.id) {
            return reject({ message: 'Unlock page permission denied.' });
        }
    });
};

Page.Instance.prototype.lockFor = function(user) {
    return new Promise((resolve, reject) => {
        if (this.isLocked() && !this.isLockedBy(user)) {
            return reject({ message: 'Page already locked.' });
        }
        return this.lock(user)
            .then(() => resolve(this))
            .catch(err => reject(err));
    });
};

Page.createPage = (ownerId, params) => {
    const body = Object.assign(
        {},
        { isPublished: true, parentId: params.parent },
        { ownerId, version: 1 },
        params
    );

    return Page.create(body).then(page =>
        PageVersion.create(
            Object.assign({}, { pageId: page.get('id') }, body)
        ).then(ver =>
            page
                .update({ activeVersionId: ver.get('id') })
                .then(() => Promise.resolve(page))
        )
    );
};

Page.Instance.prototype.isLockedBy = function(user) {
    return this.get('lockedById') === user.id;
};

Page.Instance.prototype.pageIsBeingReordered = function() {
    // @TODO: - Implement page_is_being_restored
    return false;
};

// Get root page of pages SQL tree
Page.Instance.prototype.getRoot = function() {
    const getParrent = parentId => {
        return Page.findOne({ where: { parentId: parentId } }).then(page => {
            if (page.get('parentId')) {
                return getParrent(page.get('parentId'));
            }
            Promise.resolve(page);
        });
    };
    return getParrent(this.parentId);
};

// Get all children pages
Page.Instance.prototype.getChildren = function(ownerId) {
    let childrenPages = [];

    const createActiveVersion = (pageId, title, version) => {
        return PageVersion.create({ pageId, ownerId, title, version })
            .then(pv => Promise.resolve(pv.get('id')));
    };

    const getChildrenFn = parentId => {
        return Page.findOne({ where: { parentId } })
            .then(page => {
                return createActiveVersion(page.get('id'), page.get('pageTitle'), 1)
                    .then(activeVersionId => {
                            page.activeVersionId = activeVersionId;
                            return Promise.resolve(childrenPages);
                    });

            })
            .catch(err => Promise.resolve(childrenPages));
    };

    return getChildrenFn(this.id);
};

Page.Instance.prototype.cloneTree = function(ownerId, isRecursive = false) {
    const cloneTreeFn = page => {
        return page.getChildren(ownerId).then(children => {
            let clonedPage = Object.assign({}, page.get({ plain: true }));
            clonedPage.id == null;
            clonedPage.lockedAt = null;
            clonedPage.lockedById = null;
            clonedPage.lft = null;
            clonedPage.right = null;

            return Page.create(clonedPage).then(newPage => {
                // @TODO - cloned_active_version = clone_page_versions(page, active_version) (After components)
                if (isRecursive) {
                    if (!children.length) {
                        return Promise.resolve(newPage);
                    }

                    let promises = children.map(child =>
                        Promise.resolve(child.cloneTree(ownerId, isRecursive))
                    );
                    return Promise.all(promises);
                }

                Promise.resolve(newPage);
            });
        });
    };

    return cloneTree(this);
};

// Delete page
Page.Instance.prototype.deletePage = function(user) {
    return this.getChildren()
        .then(children => {
            if (children) {
                children.forEach(child => {
                    if (
                        child.get('isLockedAt') &&
                        child.lockedById === user.id
                    ) {
                        return Promise.reject(
                            'Page cannot be deleted, a child page is being edited.'
                        );
                    }
                });
            }
            return this.destroy().then(() => Promise.resolve(children));
        })
        .catch(err => Promise.reject(err));
};

Page.Instance.prototype.serializer = function() {
    if (!this.get('activeVersionId')) {
        return Promise.resolve(
            Object.assign({}, toSnakeCase(this.get({ plain: true })))
        );
    }

    return PageVersion.findById(this.get('activeVersionId')).then(pageVer => {
        let ver = pageVer.get({ plain: true });
        let page = this.get({ plain: true });
        let result = toSnakeCase({
            coverImage: ver.coverImage,
            version: ver.version,
            title: ver.title,
            treeId: ver.treeId
        });
        return Promise.resolve(Object.assign({}, page, result));
    });
};

Page.beforeCreate = function(page, options) {
    return PageVersion.findById(this.get('activeVersionId')).then(pageVer =>
        Page.makeThumbnail(pageVer)
    );
};

Page.beforeUpdate = function(page, options) {
    return PageVersion.findById(this.get('activeVersionId')).then(pageVer =>
        Page.makeThumbnail(pageVer)
    );
};

Page.makeThumbnail = function(pageVersion) {
    const { coverImage } = pageVersion.get({ plain: true });
    if (coverImage) {
        return thumb({
            prefix: '',
            suffix: '',
            width: 800,
            source: `${imgPath}/${coverImage}`,
            destination: imgThumbPath
        }).then(() => pageVersion.update({ coverImageThumbnail: coverImage }));
    }
};

Page.Instance.prototype.type = function() {
    let level = this.get('level');

    switch (level) {
        case 0:
            return Page.TYPES.AREA;
        case 1:
            return Page.TYPES.SECTION;
        default:
            return Page.TYPES.CONTENT;
    }
};

Page.Instance.prototype.duplicate = function(version, owner) {

    const createNewVersion = (currentVersion = null, version) => {
        let ownerId = owner && owner.id ? owner.id : currentVersion.get('ownerId');
        let parentId = this.get('pageVersion') ? this.get('pageVersion').get('id') : null ;
        let title = currentVersion.get('title');
        let pageId = this.get('id');

        return PageVersion.create({ pageId, ownerId, version, title })
            .then(newVersion => {
                // @TODO: Page.copyComponent(currentVersion, newVersion);
                return Promise.resolve(newVersion);
            })
            .catch(err => Promise.resolve(err));
    };

    if (!owner) {
        throw new Error('Page owner not defined.');
    }
    //

    return PageVersion.max('version', { where: { pageId: this.get('id') }})
        .then(ver => {
            let nextVersion = this.get('pageVersion').get('version') + 1;

            if (ver) {
               nextVersion = ver + 2;
            }

            if (!version) {
                return createNewVersion(this.get('pageVersion'), nextVersion);
            }

            return createNewVersion(version, nextVersion);
    });
};

Page.Instance.prototype.shadowVersion = function(fromVersion, owner) {
    const page = this;

    const selectVersion = (version = null, owner = null) => {
        return new Promise((resolve, reject) => {
            return PageVersion.findOne({
                where: {
                    pageId: page.get('id'),
                    version: page.get('pageVersion').get('version') + 1
                }
            })
                .then(pageVersion => {
                    if (!pageVersion) {
                        return Promise.reject();
                    }
                })
                .catch(() => {
                    return page
                        .duplicate(version, owner)
                        .then(pageVersion => resolve(pageVersion));
                });
        });
    };

    if (fromVersion) {
        return PageVersion.findOne({
            where: {
                pageId: page.get('id'),
                version: page.get('pageVersion').get('version') + 1,
                parentId: { $not: fromVersion }
            }
        })
            .then(pageVersion => pageVersion.destroy())
            .then(() => selectVersion(fromVersion, owner))
            .catch(() => selectVersion(fromVersion, owner));
    }

    return selectVersion(null, owner);
};

Page.copyComponent = function(currentVersion, newVersion) {
    /* @TODO:
    for component in current_version.components.all():
        original_component_pk = component.pk
        component_files = list(component.files.all())

        # create a new component
        component.pk = None
        component.page_version = new_version
        component.save()

        for file in component_files:
            file.pk = None
            file.original_component_id = original_component_pk
            file.component = component
            file.save()
    */
};

Page.TYPES = {
    AREA: 'area',
    SECTION: 'section',
    CONTENT: 'content'
};

module.exports = Page;
