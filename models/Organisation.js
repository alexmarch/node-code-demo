const clout = require('clout-js');
const { Sequelize, sequelize } = clout;

/**
 * Organisation table model
 */
const Organisation = sequelize.define(
    'Organisation',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING(255),
            allowNull: true
        }
    },
    {
        createdAt: false,
        updatedAt: false,
        tableName: 'organisation_organisation'
    }
);

Organisation.Instance.prototype.serializer = function() {
    let organisation = this.get({ plain: true });
    return toSnakeCase(organisation);
};
module.exports = Organisation;
