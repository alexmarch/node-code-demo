const clout = require('clout-js');
const { Sequelize, sequelize } = clout;
const _ = require('lodash');
/**
 * Page table model
 */
const UserGuard = sequelize.define('UserGuard', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    permission: {
        type: Sequelize.STRING(255),
        allowNull: false
    },
    objectType: {
        type: Sequelize.ENUM('Page', 'Component'),
        allowNull: false
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    objectId: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
}, {
        createdAt: false,
        updatedAt: false,
        tableName: 'user_guard'
    });

UserGuard.Types = {
    Page: 'Page',
    Component: 'Component'
};

UserGuard.Permissions = {
    Page: {
        view: 'view_page',
        change: 'change_page',
        moderate: 'moderate_page'
    }
};

UserGuard.hasPermission = (user, perm, objectType, obj) => {
    let permList = []

    if (Array.isArray(perm)) {
        permList = perm;
    } else {
        permList.push(perm);
    }

    return UserGuard.findAll({
        where: {
            userId: user.id || user.get('id'),
            permission: { $in: permList },
            objectType,
            objectId: obj.get('id')
        }})
        .then(guards => {
            if (guards.length >= permList.length) {
               return Promise.resolve(permList);
            }
            return Promise.reject('Not have permission to change this object.');
        })
        .catch(err => Promise.reject('Not have permission to change this object.'));
};

/**
 *
 * @param {Object} user - User object Model/JSON
 * @param {Array|Object} obj - Array of object model or model
 * @param {String} objectType - Object type from UserGuard.Types
 */
UserGuard.getGuardObjectsForUser = (user, perm, objectType, obj) => {
    let objList = [];
    let permList = [];

    return new Promise((resolve, reject) => {
        if (Array.isArray(obj)) {
            obj.forEach(item => objList.push(item.id || item.get('id')));
        } else {
            objList.push(obj.id || obj.get('id'));
        }

        if (Array.isArray(perm)) {
            permList = perm;
        } else {
            permList.push(perm);
        }

        UserGuard.findAll({
            where: {
                userId: user.id || user.get('id'),
                permission: { $in: permList },
                objectType,
                objectId: { $in: objList }
            },
            order: 'id DESC'
        })
        .then(guards => resolve(_.map(guards, guard => guard.get({ plain: true }))))
        .catch(err => reject(err));
    });
};

/**
 *
 * @param {Object} user - User object Model/JSON
 * @param {String} permission - User permission for object
 * @param {String} objectType - Object type from UserGuard.Types
 * @param {Array|Object} obj - Array of object model or model
 */
UserGuard.removeUserPermission = (user, permission, objectType, obj) => {
    let objList = [];
    if (Array.isArray(obj)) {
        obj.forEach(item => objList.push(item.id || item.get('id')));
    } else {
        objList.push(obj.id || obj.get('id'));
    }
    return UserGuard.findAll({
        where: {
            userId: user.id || user.get('id'),
            permission,
            objectType,
            objectId: { $in: objList }
        }
    })
    .then(guards => Promise.all(_.map(guards, (guard) => guard.destroy())));
};

/**
 *
 * @param {String} permission - User permission for object
 * @param {Object} user - User object Model/JSON
 * @param {String} objectType - Object type from UserGuard.Types
 * @param {Array|Object} obj - Array of object model or model
 */
UserGuard.assignUserPermission = (userId, permission, objectType, obj) => {
    let objList = [];

    if (Array.isArray(obj)) {
        obj.forEach(item => {
            objList.push({ permission, objectType, userId, objectId: item.id || item.get('id') })
        });
    } else {
        objList.push({ permission, objectType, userId, objectId: obj.id || obj.get('id') })
    }
  
    return UserGuard.bulkCreate(objList).then(() => UserGuard.findAll({ where: { userId } }));
};

module.exports = UserGuard;

