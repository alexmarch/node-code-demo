const clout = require('clout-js');
const { Sequelize, sequelize } = clout;
const { hashPassword } = require('../libs/crypto');
const AuthUser = require('./AuthUser');
const Organisation = require('./Organisation');
const ManagedUsers = require('./ManagedUsers');
const PageVersion = require('./PageVersion');
const Page = require('./Page');
const { approveUser, rejectUser } = require('../libs/user');
const { toSnakeCase } = require('../libs/utils');

/**
 * AccountProfile table model
 */
const AccountProfile = sequelize.define(
    'AccountProfile',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        message: {
            type: Sequelize.STRING(255),
            allowNull: true,
            default: ''
        },
        token: {
            type: Sequelize.STRING(40)
        },
        organisationId: {
            type: Sequelize.INTEGER,
            references: {
                model: Organisation,
                key: 'id'
            },
            field: 'organisation_id'
        },
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: AuthUser,
                key: 'id'
            },
            field: 'user_id'
        },
        tokenCreatedAt: {
            type: Sequelize.DATE,
            field: 'token_created_at',
            allowNull: true
        },
        isApproved: {
            type: Sequelize.BOOLEAN,
            field: 'is_approved',
            default: false
        },
        avatar: {
            type: Sequelize.STRING(100),
            allowNull: true
        },
        avatarThumbnail: {
            type: Sequelize.STRING(100),
            field: 'avatar_thumbnail',
            allowNull: true
        },
        ssoLoginCompleted: {
            type: Sequelize.BOOLEAN,
            field: 'sso_login_completed',
            allowNull: true
        },
        role: {
            type: Sequelize.ENUM,
            field: 'role',
            values: ['admin', 'moderator', 'user'],
            defaultValue: 'user',
            allowNull: false
        },
        hasSeenOnboarding: {
            type: Sequelize.BOOLEAN,
            field: 'has_seen_onboarding',
            default: false
        }
    },
    {
        createdAt: false,
        updatedAt: false,
        tableName: 'account_profile'
    }
);

const DATE_OFFSET = 7 * (24 * 60 * 1000 * 2);

/**
 * @description Create default profile
 * @param {String} email  Account email address
 * @param {Object} data  SSO session data
 * @param {Object} req Express router request object
 */
AccountProfile.createProfile = (email, data, req, isDefault) => {
    const { AccountProfile, Organisation, AuthUser } = req.models;
    return new Promise((resolve, reject) =>
        Organisation.all().then(organisations => {
            let organisation;
            const firstName =
                data['FirstName'] || data['First Name'] || 'Unknown';
            const lastName = data['LastName'] || data['Last Name'] || 'Unknown';
            const password = data['password'] || `Unknown_${Date.now()}`;
            const isSuperUser = data['isSuperUser'] || false;
            const isStaff = data['isStaff'] || false;
            const isActive = data['isActive'] || false;
            const title = data['title'] || 'Unknown';
            const message = data['message'] || '';
            const organisationId =
                data['organisation'] || data['organisationId'];

            if (!organisations.length && !data.organisation) {
                return reject('Organisations not found.');
            }

            const userData = {
                email,
                password,
                username: `${firstName}.${lastName}_${Date.now()}`,
                firstName,
                lastName,
                isSuperUser,
                dateJoined: Date.now(),
                isStaff,
                isActive
            };
            return AuthUser.create(userData)
                .then(user => {
                    const profileData = {
                        userId: user.get('id'),
                        organisationId: data.organisation || organisation.id,
                        title: 'Unknown',
                        message: '',
                        tokenCreatedAt: null,
                        avatar: null,
                        avatarThumbnail: null,
                        ssoLoginCompleted: isDefault ? null : false,
                        role: AccountProfile.Role.user,
                        hasSeenOnboarding: false
                    };
                    return AccountProfile.create(profileData)
                        .then(profile => {
                            Organisation.findOne({
                                where: { id: profileData.organisationId }
                            }).then(organisation => {
                                let profileData = profile.get({ plain: true });
                                profileData.user = user.get({ plain: true });
                                profileData.organisation = organisation.get({
                                    plain: true
                                });
                                resolve(profileData);
                            });
                        })
                        .catch(err => {
                            reject(
                                `Can\`t create account profile ${err} ${JSON.stringify(
                                    profileData
                                )}`
                            );
                        });
                })
                .catch(err => reject(err));
        })
    ).catch(err => {
        reject(`Can\`t create user ${err} ${JSON.stringify(userData)}`);
    });
};
AccountProfile.Roles = {
    admin: 'admin',
    moderator: 'moderator',
    user: 'user'
};
AccountProfile.belongsTo(AuthUser, { as: 'user' });
AccountProfile.belongsTo(Organisation, { as: 'organisation' });

AccountProfile.belongsToMany(AuthUser, {
    as: 'Moderator',
    through: { model: ManagedUsers },
    foreignKey: 'account_id',
    constraints: false
});
AuthUser.belongsToMany(AccountProfile, {
    through: { model: ManagedUsers },
    foreignKey: 'user_id',
    constraints: false
});

PageVersion.hasOne(Page, { as: 'page', foreignKey: 'activeVersionId' });

AccountProfile.bulkUpdate = (body, partial) => {
    return Promise((resolve, reject) => {
        let updates = [];
        for (let profileData of body) {
            updates.push(
                AccountProfile.update(profileData, {
                    where: { userId: profileData.userId }
                })
            );
        }
        Promise.all(updates).then(...result => resolve(result));
    });
};

AccountProfile.findByUserId = userId => {
    return AccountProfile.findOne({
        where: { userId },
        include: [{ model: AuthUser, as: 'user' }]
    });
};

AccountProfile.Instance.prototype.isTokenExpired = () =>
    this.tokenCreatedAt < Date.now() - DATE_OFFSET;
AccountProfile.Instance.prototype.isAdmin = () =>
    this.user && this.user.isSuperUser;
AccountProfile.Instance.prototype.isModerator = () =>
    this.role === AccountProfile.Role.moderator;
AccountProfile.Instance.prototype.isUser = () =>
    this.role === AccountProfile.Role.user;
AccountProfile.Instance.prototype.approve = (user, req) =>
    approveUser(user, this, req);
AccountProfile.Instance.prototype.reject = (user, req) =>
    rejectUser(user, this, req);
AccountProfile.Instance.prototype.serializer = () => {
    const accountProfile = this.get({ plain: true });
    return toSnakeCase(accountProfile);
};

AccountProfile.Role = {
    admin: 'admin',
    moderator: 'moderator',
    user: 'user'
};

module.exports = AccountProfile;
