const clout = require('clout-js');
const { Sequelize, sequelize } = clout;
const AuthUser = require('./AuthUser');
const AuthGroup = require('./AuthGroup');

/**
 * AuthUserGroups table model
*/
const AuthUserGroups = sequelize.define('AuthUserGroups', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  userId: {
    type: Sequelize.INTEGER,
    references: {
      model: AuthUser,
      key: 'id',
    },
    field: 'user_id'
  },
  groupId: {
    type: Sequelize.INTEGER,
    references: {
      model: AuthGroup,
      key: 'id'
    },
    field: 'group_id'
  }
}, {
    createdAt: false,
    updatedAt: false,
    tableName: 'auth_user_groups'
  });

AuthUserGroups.belongsTo(AuthUser, { as: 'user' });
AuthUserGroups.belongsTo(AuthGroup, { as: 'group' });

module.exports = AuthUserGroups;
