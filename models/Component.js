const clout = require('clout-js');
const { Sequelize, sequelize } = clout;
const { COMPONENTS_DATA_MAX_BYTES } = clout.config;
const PageVersion = require('./PageVersion');
const uuidv4 = require('uuid/v4');
const { toSnakeCase } = require('../libs/utils');

/**
 * Component table model
 */
const Component = sequelize.define(
    'Component',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        position: {
            type: Sequelize.INTEGER,
            allowNull: true,
            field: 'position',
            defaultValue: 0
        },
        pageVersion: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: PageVersion,
                key: 'id'
            },
            field: 'page_version_id'
        },
        typeId: {
            type: Sequelize.STRING(64),
            allowNull: false,
            field: 'type_id'
        },
        uuid: {
            type: Sequelize.UUIDV4,
            allowNull: false,
            field: 'uuid'
        },
        isVisiable: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            field: 'is_visiable',
            defaultValue: false
        },
        data: {
            type: Sequelize.JSON,
            allowNull: true
        }
    },
    {
        validate: {
            bytesSizeValidator: function() {
                if (this.data) {
                    const valueBytes = Buffer.from(this.data, 'utf8').length;
                    if (valueBytes > COMPONENTS_DATA_MAX_BYTES) {
                        throw new Error(
                            `Maximum field size of ${COMPONENTS_DATA_MAX_BYTES} bytes exceeded (You entered ${valueBytes} bytes).`
                        );
                    }
                }
            }
        },
        updatedAt: false,
        tableName: 'components_component'
    }
);

Component.belongsTo(PageVersion, {
    as: 'page_version',
    foreignKey: 'pageVersion'
});

Component.TYPE = {
    IMAGE_GRID: 'imagegrid',
    LINK: 'links',
    HEADER: 'heading'
};

Component.beforeValidate((component, options) => {
    if (!component.position) {
        component.position = 0; // @TODO: Need check this later in python - 'Should this really be moved to enterprise_drg.utils package?'
    }
    if (!component.uuid) {
        component.uuid = uuidv4();
    }
    return sequelize.Promise.resolve(component);
});

Component.beforeCreate((component, options) => {
    component.uuid = uuidv4();
    return sequelize.Promise.resolve(component);
});

Component.Instance.prototype.serializer = function() {
    let component = this.get({ plain: true });
    return toSnakeCase(component);
};

module.exports = Component;
