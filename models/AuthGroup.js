const clout = require('clout-js');
const { Sequelize, sequelize } = clout;

/**
 * AuthGroup table model
*/
const AuthGroup = sequelize.define('AuthGroup', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: Sequelize.STRING(80),
    allowNull: true
  }
}, {
    createdAt: false,
    updatedAt: false,
    tableName: 'auth_group'
  });

module.exports = AuthGroup;
